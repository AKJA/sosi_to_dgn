﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using MicroStationDGN;
using System.Diagnostics;

using System.Runtime.InteropServices;
using System.Data.OleDb;



namespace SOSI_Converter
{
    public partial class Form1 : Form
    {
        string[] SelectedFilenames = null;
        private string ProjectPath = string.Empty;

        int SOSIFileNameID = 1969;


        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SelectedFilenames = null;
            openFileDialog1.Multiselect = true;
            openFileDialog1.Filter = "SOSI files (*.sos)|*.sos|All files (*.*)|*.*";
            DialogResult di = openFileDialog1.ShowDialog();
            if (di == DialogResult.OK)
            {
                textBox1.Text = openFileDialog1.FileName;
                SelectedFilenames = openFileDialog1.FileNames;
                foreach (string Fname in SelectedFilenames)
                {
                    richTextBox1.Text = richTextBox1.Text + Fname + "\n";
                }
                progressBar2.Minimum = 0;
                if (SelectedFilenames.Length > 0) { progressBar2.Value = 0; progressBar2.Maximum = SelectedFilenames.Length; }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            folderBrowserDialog1.SelectedPath = "C:\\Temp\\1";
            DialogResult di = folderBrowserDialog1.ShowDialog();
            if (di == DialogResult.OK)
            {
                textBox2.Text = folderBrowserDialog1.SelectedPath;    
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //try
            {
                label4.Text = "";
                if (textBox1.Text == "" ) { MessageBox.Show("Input SOSI file not selected"); return; }
                if (textBox2.Text == "") { MessageBox.Show("Output path not selected.."); return; }
                if (comboBox_UTF_Type.Text == "") { MessageBox.Show("Encoding type not selected.."); return; }
                //if (textBox3.Text == "" ) { MessageBox.Show("Input Attribute sosi file Missing...."); }
                if (File.Exists(textBox1.Text) == false)  { MessageBox.Show("Input SOSI file not selected"); return; }
                if (SelectedFilenames == null)
                {
                    Array.Resize(ref SelectedFilenames, 1);
                    SelectedFilenames[0] = textBox1.Text;
                }

                List<string> PointAtt = new List<string>();
                List<string> LineAtt = new List<string>();
                List<string> polyAtt = new List<string>();
                PointAtt.Clear();
                LineAtt.Clear();
                polyAtt.Clear();
                int linecnt = 0, pointcnt = 0, polycnt = 0;
                int Centroid_pointcnt = 0;
                //======================================================================================
                string[] sosifilename1 = textBox1.Text.Split('\\');
                string sosifilename = sosifilename1[sosifilename1.Length - 1];
                sosifilename = sosifilename.Substring(0, sosifilename.Length - 4);

                if (SelectedFilenames.Length > 1) { sosifilename = sosifilename + "_Merge"; }

                string app_path = System.Windows.Forms.Application.StartupPath;
                string GDNseed = System.Windows.Forms.Application.StartupPath + "\\Norway_SEED.dgn";
                if (File.Exists(GDNseed) == false) { MessageBox.Show("Norway_SEED.dgn not found"); return; }

                string OpDGN = textBox2.Text + "\\" + sosifilename + ".dgn";
               // if (SelectedFilenames.Length > 1) { OpDGN = textBox2.Text + "\\" + sosifilename + "_Merge.dgn"; }

                File.Copy(GDNseed, OpDGN, true);
                string DBfile = System.Windows.Forms.Application.StartupPath + "\\Norway_SOSI_DB.mdb";
                if (File.Exists(DBfile) == false) { MessageBox.Show("Norway_SOSI_DB.mdb not found"); return; }
                string OpDB = textBox2.Text + "\\" + sosifilename + ".mdb";
                File.Copy(DBfile, OpDB, true);

                string celllibraryPath = System.Windows.Forms.Application.StartupPath + "\\tcsosi.cel";
                //======================================================================================
                MicroStationDGN.Application micro = new MicroStationDGN.Application();
                micro.Visible = true;
                micro.OpenDesignFile(OpDGN, false, MsdV7Action.msdV7ActionUpgradeToV8);
                this.BringToFront();
                MicroStationDGN.Application mApp = (MicroStationDGN.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("MicroStationDGN.Application");
                //------------------cell library attach----------------------
                mApp.AttachCellLibrary(celllibraryPath);
                if (mApp.ActiveDesignFile.Levels.Find("DELETED", null) == null)
                {
                    mApp.ActiveDesignFile.AddNewLevel("DELETED");
                    mApp.ActiveDesignFile.Levels.Rewrite();
                }
                //------------------------------------
                int filecount = 0;
                foreach (string Fname in SelectedFilenames)
                {
                    //richTextBox1.Text = richTextBox1.Text + Fname + "\n";
                    string[] SOSIfilename1 = Fname.Split('\\');
                    string SOSIfilename=SOSIfilename1[SOSIfilename1.Length-1];
                    filecount++;
                    string filestaus = "";
                    filestaus = filecount.ToString() + " \\ " + SelectedFilenames.Length.ToString();
                    //int MslinkId = 1000;
                    //StreamReader file = new System.IO.StreamReader(textBox1.Text, System.Text.Encoding.UTF7);
                    //StreamReader file = new System.IO.StreamReader(Fname, System.Text.Encoding.UTF8);
                    StreamReader file=null;
                    if (comboBox_UTF_Type.Text.Trim() == "UTF8")
                    {
                        file = new System.IO.StreamReader(Fname, System.Text.Encoding.UTF8);
                    }
                    else if (comboBox_UTF_Type.Text.Trim() == "UTF7")
                    {
                        file = new System.IO.StreamReader(Fname, System.Text.Encoding.UTF7);
                    }
                    else if (comboBox_UTF_Type.Text.Trim() == "Unicode")
                    {
                        file = new System.IO.StreamReader(Fname, System.Text.Encoding.Unicode);
                    }
                    else
                    {
                        file = new System.IO.StreamReader(Fname, System.Text.Encoding.UTF7);
                    }
                    

                    string Mline;
                    List<string> Featurelist = new List<string>();
                    string FeatureAttributes = "";
                    while ((Mline = file.ReadLine()) != null)
                    {

                        Mline = Mline.Replace(";", ":");// 20160422
                        label4.Text = filestaus + "Reading sosi file:" + SOSIfilename;
                        if (Mline.Trim().Length > 0)
                        {
                            string[] splittedline = Mline.Trim().Split();
                            string Firstchar = Mline.Trim().Substring(0, 1);
                            if (Firstchar != "!")
                            {
                                
                                //string Secchar = Mline.Trim().Substring(1, 1);
                                string Secchar = "";
                                if (Mline.Trim().Length > 1) Secchar = Mline.Trim().Substring(1, 1);
                                if (Firstchar == "." && Secchar != ".")
                                {
                                    if (FeatureAttributes != "") Featurelist.Add(FeatureAttributes);
                                    FeatureAttributes = "";
                                    FeatureAttributes = Mline.Trim();
                                }
                                else
                                {
                                    FeatureAttributes = FeatureAttributes + ";" + Mline.Trim();
                                }
                                
                            }

                        }

                    }
                    file.Close();
                    if (Featurelist.Count > 1)
                    {
                        //-----------------------------------------------------
                        double fileunit = 1;
                        string[] tempsplitedfline = Featurelist[0].Split(';');
                        for (int t = 0; t < tempsplitedfline.Length; t++)
                        {
                            if (tempsplitedfline[t].Length > 8)
                            {
                                if (tempsplitedfline[t].Trim().Substring(3, 5) == "ENHET")
                                {
                                    string[] linesplited = tempsplitedfline[t].Split(' ');
                                    fileunit = Convert.ToDouble(linesplited[1]);
                                }
                            }
                        }
                        //---------------------------------------------------------------------------------------
                        OleDbConnection conn = new OleDbConnection("Provider = Microsoft.Jet.OLEDB.4.0; Data Source = " + OpDB);//OpDB
                        conn.Open();
                        var DataTableLayerList = new DataTable();
                        var query = "SELECT * from feature_Layer_Ref";
                        var adapter = new OleDbDataAdapter(query, conn);
                        OleDbCommandBuilder oleDbCommandBuilder = new OleDbCommandBuilder(adapter);
                        adapter.Fill(DataTableLayerList);
                        //---------------------------------------------------------------------------------------
                        var DataTableFeature = new DataTable();
                        var query1 = "SELECT * from feature";
                        var adapter1 = new OleDbDataAdapter(query1, conn);
                        OleDbCommandBuilder oleDbCommandBuilder1 = new OleDbCommandBuilder(adapter1);
                        adapter1.Fill(DataTableFeature);
                        //---------------------------------------------------------------------------------------
                        progressBar1.Minimum = 0;
                        if (Featurelist.Count > 1) { progressBar1.Maximum = Featurelist.Count; }

                        //--------------------20150512------------------------
                        string DOF_NewFeature = ""; 
                        if (checkBox1.Checked)
                        {
                            DOF_NewFeature = textBox5.Text.Trim();
                        }



                        //----------------------------------------------------------


                        for (int i = 1; i < Featurelist.Count; i++)
                        {
                            label4.Text = filestaus + "Writing from SOSI:IP:" + SOSIfilename;
                            Point3d[] Vlist = null;
                            bool D3 = false;
                            string[] splitedfline = Featurelist[i].Split(';');
                            string FLayername = "Default";
                            string[] tempver1 = splitedfline[1].Split();
                            if (tempver1.Length > 1)
                            {
                                FLayername = splitedfline[1].Split()[1].Trim();

                            }
                            //double Zval2 = 0;
                            double Zval1 = 0;
                            bool XYZfound = false;
                            bool XYfound = false;
                            bool HODE_found = false;
                            bool MediumFound = false;


                            for (int j = 2; j < splitedfline.Length; j++)
                            {
                                string[] Fattributes = splitedfline[j].Split();
                                if (Fattributes[0].Trim() == "..HØYDE") 
                                { 
                                    Zval1 = Convert.ToDouble(Fattributes[Fattributes.Length - 1].Trim()); HODE_found = true;
                                }
                                //if (splitedfline[j] == "..NØH") { XYZfound = true; XYfound = false; }
                                //if (splitedfline[j] == "..NØ") { XYZfound = false; XYfound = true; }
                                string Firstch = splitedfline[j].Trim().Substring(0, 1);
                                if (Firstch != "." && Firstch != ":" && Firstch != "(" && Firstch != ")" && Firstch != "!") ///problem area
                                {
                                    string[] Vliststr = splitedfline[j].Trim().Split(' ');
                                    Point3d pt = mApp.Point3dZero();
                                    pt.X = Convert.ToDouble(Vliststr[1]) * fileunit;
                                    pt.Y = Convert.ToDouble(Vliststr[0]) * fileunit;
                                    if (Vliststr.Length > 2)
                                    {
                                        double zval = 0;
                                        if (double.TryParse(Vliststr[2], out zval))
                                        {
                                            pt.Z = Convert.ToDouble(Vliststr[2]) * fileunit;
                                        }
                                        else
                                        {
                                            pt.Z = Zval1;
                                        }

                                    }
                                    else
                                    {
                                        pt.Z = Zval1;    //Jan 15 2015 for height issue
                                    }
                                    //if (splitedfline[j - 1] == "..NØH") { Zval2 = Convert.ToDouble(Vliststr[2]) * fileunit; }
                                    //if ((splitedfline[j - 1].Trim().Length == 5) || D3) { pt.Z = Convert.ToDouble(Vliststr[2]) * fileunit; D3 = true; }
                                    if (Vlist == null)
                                    {
                                        Array.Resize(ref Vlist, 1);
                                    }
                                    else
                                    {
                                        Array.Resize(ref Vlist, Vlist.Length + 1);
                                    }
                                    Vlist[Vlist.Length - 1] = pt;
                                }
                            }
                            //-----------------------Attributes part--------------------------------------------
                            string Datagroup = splitedfline[0].Split()[0].Trim();
                            Datagroup = Datagroup.Substring(1, Datagroup.Length - 1);
                            string DataId = splitedfline[0].Split()[1].Trim();
                            DataId = DataId.Substring(0, DataId.Length - 1);
                            int Mlink = Convert.ToInt32(DataId.Trim());
                            DataId = splitedfline[0].Substring(1);

                            string Objtype = "";
                            string Val_TRE_D_NIVA = "";
                            string Val_KVALITET = "";
                            string Val_DATAFANGSTDATO = "";
                            string Val_REGISTRERINGSVERSJON = "";

                            string STATUS_val = "";
                            string KOPIDATA_avl = "";
                            string OMRADEID_val = "";
                            string ORIGINALDATAVERT_val = "";
                            string KOPIDATO_val = "";
                            string OPPDATERINGSDATO_val = "";
                            string VERIFISERINGSDATO_val = "";
                            string PROSESS_HISTORIE_val = "";

                            string ppKOMM = "";
                            string ppBYGGNR = "";
                            string ppBYGGSTAT = "";
                            string ppINFORMASJON = "";
                            string ppREF = "";

                            string MALEMETODE = "";
                            string NOYAKTIGHET = "";
                            string SYNBARHET = "";
                            string HMALEMETODE = "";
                            string HNOYAKTIGHET = "";
                            string SEFRAK_ID = "";
                            string SEFRAKKOMMUNE = "";
                            string REGISTRERINGKRETSNR = "";
                            string HUSLØPENR = "";
                            string BYGGSTAT = "";
                            string MEDIUM = "";
                            string HREF = "";
                            string BYGGTYP_NBR = "";
                            string MATRIKKEL_ID = "";
                            string ppREF1 = "";
                            string ppREF2 = "";
                            string ppREF3 = "";
                            //---------------------------------------------------------------------------------
                            string ppREF4 = "";
                            string ppREF5 = "";
                            string ppREF6 = "";
                            string ppREF7 = "";
                            string ppREF8 = "";
                            string ppREF9 = "";
                            string ppREF10 = "";
                            string ppREF11 = "";
                            string ppREF12 = "";
                            string ppREF13 = "";
                            string ppREF14 = "";
                            string ppREF15 = "";
                            string ppREF16 = "";
                            string ppREF17 = "";
                            string ppREF18 = "";
                            string ppREF19 = "";
                            string ppREF20 = "";
                            string ppREF21 = "";
                            string ppREF22 = "";
                            string ppREF23 = "";
                            string ppREF24 = "";
                            string ppREF25 = "";
                            string ppREF26 = "";
                            string ppREF27 = "";
                            string ppREF28 = "";
                            string ppREF29 = "";
                            string ppREF30 = "";
                            string ppREF31 = "";
                            string ppREF32 = "";
                            string ppREF33 = "";
                            string ppREF34 = "";
                            string ppREF35 = "";

                            string ppREF36 = "";
                            string ppREF37 = "";
                            string ppREF38 = "";
                            string ppREF39 = "";
                            string ppREF40 = "";
                            string ppREF41 = "";
                            string ppREF42 = "";
                            string ppREF43 = "";
                            string ppREF44 = "";
                            string ppREF45 = "";
                            string ppREF46 = "";
                            string ppREF47 = "";
                            string ppREF48 = "";
                            string ppREF49 = "";
                            string ppREF50 = "";
                            //-----------------------------------------------
                            string VANNBR_val = "";
                            string LHLYSRETN_val = "";
                            string LHLYSFARGE_val = "";
                            string LHLYS_OPPHØYD_NEDFELT_val = "";
                            string LHLYSTYPE_val = "";
                            string HØYDE_val = "";
                            string KYSTREF_val = "";
                            string KYSTKONSTRUKSJONSTYPE_val = "";
                            string TRE_TYP_val = "";
                            string BRUTRAFIKKTYPE_val = "";
                            string BRUOVERBRU_val = "";
                            string SKJERMINGFUNK_val = "";
                            string HOB_val = "";
                            string IDENTIFIKASJON_val = "";
                            string LOKALID_val = "";
                            string NAVNEROM_val = "";
                            string VERSJONID_val = "";
                            string LEDN_EIER_val = "";
                            string LEDN_EIERTYPE_val = "";
                            string LEDN_EIERNAVN_val = "";
                            string LEDN_EIERANDEL_val = "";
                            string DRIFTSMERKING_val = "";
                            string LEDN_HØYDEREFERANSE_val = "";
                            string EL_STASJONSTYPE_val = "";
                            string MASTEFUNKSJON_val = "";
                            string MASTEKONSTRUKSJON_val = "";
                            string VNR_val = "";
                            string MAST_LUFTFARTSHINDERMERKING_val = "";
                            string LEDNINGNETTVERKSTYPE_val = "";
                            string FELLESFØRING_val = "";
                            string LEDNINGSNETTVERKSTYPE_val = "";
                            string LEDN_LEIETAKER_val = "";
                            string LINJEBREDDE_val = "";
                            string VEGREKKVERKTYPE_val = "";
                            string NEDSENKETKANTSTEIN_val = "";
                            string VEGKATEGORI_val = "";
                            string VEGSTATUS_val = "";
                            string VEGNUMMER_val = "";
                            string VEGOVERVEG_val = "";
                            string JERNBANETYPE_val = "";
                            string JERNBANEEIER_val = "";
                            string LHINST_TYPE_val = "";
                            string RESTR_OMR_val = "";
                            string RETN_val = "";
                            string TWYMERK_val = "";
                            string LHAREAL_val = "";
                            string ANNENLUFTHAVN_val = "";
                            string PLFMERK_val = "";
                            string LHSKILTTYPE_val = "";
                            string LHSKILTKATEGORI_val = "";
                            string BYGGNR_val = "";
                            string BYGGTYP_NBR_val = "";
                            string BYGGSTAT_val = "";
                            string SEFRAK_ID_val = "";
                            string SEFRAKKOMMUNE_val = "";
                            string HUSLØPENR_val = "";
                            string KOMM_val = "";
                            string STED_VERIF_val = "";
                            string TAKSKJEGG_val = "";

                            string VEGSPERRINGTYPE_val = "";
                            string VPA_val = "";
                            string VKJORFLT_val = "";
                            string VFRADATO_val = "";
                            string LBVKLASSE_val = "";
                            string KOMM_2_val = "";

                            //----------------------DEC 23 ----------------
                            string 	SLUSETYP_val="";
                            string 	ENDRINGSFLAGG_val="";
                            string 	ENDRET_TYPE_val="";
                            string 	ENDRET_TID_val="";
                            string 	NETTVERKSTASJONTYPE_val="";
                            string 	KONSTRUKSJONSHØYDE_val="";
                            string 	BELYSNINGSBRUK_val="";
                            string 	NEDSENKET_val="";
                            string 	KANTSTEIN_val="";
                            string 	VTILDATO_val="";

                            string 	METER_TIL_val="";
                            string 	METER_FRA_val="";

                            string 	HOVEDPARSELL_val="";
                            string 	VLENKEID_val="";
                            string 	GATENR_val="";
                            string 	GATENAVN_val="";
                            string 	BRUKSKLASSE_val="";
                            string 	BRUKSKLASSEHELÅR_val="";
                            string 	BRUKSKLASSEVINTER_val="";
                            string 	BRUKSKLASSETELE_val="";
                            string 	MAKSVOGNTOGLENGDE_val="";
                            string 	MAKSTOTALVEKT_val="";
                            string 	MAKSTOTALVEKTSKILTET_val="";
                            string 	LINEÆRREFERANSE_val="";
                            string 	LINEÆRREFERANSETYPE_val="";
                            string 	REFERANSEFRA_val="";
                            string 	REFERANSETIL_val="";
                            string 	KJØREFELT_val="";
                            string 	FARTSGRENSE_val="";
                            string 	FARTSGRENSEVERDI_val="";
                            string 	SVINGEFORBUDREFID_val="";
                            string 	FORBUDRETNING_val="";
                            string 	SKILTAHØYDE_val="";
                            string 	LHFDET_val="";
                            string 	LHELEV_val="";
                            string 	LHSKILTLYS_val="";
                            string 	OPAREALAVGRTYPE_val="";
                            string 	RWYMERK_val="";
                            string 	TYPEVEG_val="";
                            string 	KONNEKTERINGSLENKE_val="";
                            string 	NVDB_KLASSELANDBRUKSVEG_val="";
                            string 	VEGLENKEADRESSE_val="";
                            string 	ADRESSEKODE_val="";
                            string 	ADRESSENAVN_val="";
                            string 	SIDEVEG_val="";
                            string 	BARMARKSLØYPE_val="";
                            string 	BELYSNING_val="";
                            string 	RUTEMERKING_val="";

                            //---------Jan 07--------
                            string REGISTRERINGKRETSNR_val = "";
                            //----------
                            string 	OPPHAV_val="";
                            string 	NETTVERKSSTASJONADKOMSTTYPE_val="";
                            string 	BELYSNINGSPLASSERING_val="";
                            string 	KUMFUNKSJON_val="";
                            string 	TRASEBREDDE_val="";
                            string 	MAX_AVVIK_val="";
                            string 	PRODUKTSPEK_val="";
                            string 	KORTNAVN_val="";
                            string 	OPPDATERT_val="";
                            string 	HRV_val="";
                            string 	LRV_val="";
                            string 	PRODUKTNAVN_val="";
                            string 	BYGN_ENDR_LØPENR_val="";
                            string 	BYGN_ENDR_KODE_val="";
                            string 	SKAL_AVGR_BYGN_val="";
                            int KOMM_ValCnt = 0;
                            //----------------------------------------------------------------------------------
                            for (int j = 1; j < splitedfline.Length; j++)
                            {
                                //Line
                                string firstpart = splitedfline[j].Split()[0].Trim();
                                string aatname = splitedfline[j].Split()[0].Trim();
                                if (aatname.Substring(0, 1) == ".")
                                {
                                    if (Datagroup == "KURVE")
                                    {
                                        if (LineAtt.Contains(aatname) == false)
                                        {
                                            LineAtt.Add(aatname);
                                        }
                                    }
                                    if (Datagroup == "PUNKT")
                                    {
                                        if (PointAtt.Contains(aatname) == false)
                                        {
                                            PointAtt.Add(aatname);
                                        }
                                    }
                                    if (Datagroup == "FLATE")
                                    {
                                        if (polyAtt.Contains(aatname) == false)
                                        {
                                            polyAtt.Add(aatname);
                                        }
                                    }
                                }

                                string Attval = "";
                                if (splitedfline[j].Length > firstpart.Length) { Attval = splitedfline[j].Substring(firstpart.Length + 1); Attval = Attval.Trim(); }
                                //---------------------------------------------------------------------
                                //if (GATENAVN_val.Contains("'"))
                                //{
                                //    GATENAVN_val = GATENAVN_val.Replace("'", "''");
                                //}
                                if (Attval.Contains("'"))
                                {
                                    Attval = GATENAVN_val.Replace("'", "''");
                                }
                                string AttributeNAME = "";
                                string attributeName1 = splitedfline[j].Split()[0];
                                string[] attributeName2 = attributeName1.Split('.');
                                if (attributeName2.Length > 0) { AttributeNAME = attributeName2[attributeName2.Length - 1]; }
                                if (AttributeNAME == "VANNBR") { VANNBR_val = Attval; }
                                if (AttributeNAME == "LHLYSRETN") { LHLYSRETN_val = Attval; }
                                if (AttributeNAME == "LHLYSFARGE") { LHLYSFARGE_val = Attval; }
                                if (AttributeNAME == "LHLYS_OPPHØYD_NEDFELT") { LHLYS_OPPHØYD_NEDFELT_val = Attval; }
                                if (AttributeNAME == "LHLYSTYPE") { LHLYSTYPE_val = Attval; }
                                if (AttributeNAME == "HØYDE") { HØYDE_val = Attval; }
                                if (AttributeNAME == "KYSTREF") { KYSTREF_val = Attval; }
                                if (AttributeNAME == "KYSTKONSTRUKSJONSTYPE") { KYSTKONSTRUKSJONSTYPE_val = Attval; }
                                if (AttributeNAME == "TRE_TYP") { TRE_TYP_val = Attval; }
                                if (AttributeNAME == "BRUTRAFIKKTYPE") { BRUTRAFIKKTYPE_val = Attval; }
                                if (AttributeNAME == "SKJERMINGFUNK") { SKJERMINGFUNK_val = Attval; }
                                if (AttributeNAME == "HOB") { HOB_val = Attval; }
                                if (AttributeNAME == "IDENTIFIKASJON") { IDENTIFIKASJON_val = Attval; }
                                if (AttributeNAME == "LOKALID") { LOKALID_val = Attval; }
                                if (AttributeNAME == "NAVNEROM") { NAVNEROM_val = Attval; }
                                if (AttributeNAME == "VERSJONID") { VERSJONID_val = Attval; }
                                if (AttributeNAME == "LEDN_EIER") { LEDN_EIER_val = Attval; }
                                if (AttributeNAME == "LEDN_EIERTYPE") { LEDN_EIERTYPE_val = Attval; }
                                if (AttributeNAME == "LEDN_EIERNAVN") { LEDN_EIERNAVN_val = Attval; }
                                if (AttributeNAME == "LEDN_EIERANDEL") { LEDN_EIERANDEL_val = Attval; }
                                if (AttributeNAME == "DRIFTSMERKING") { DRIFTSMERKING_val = Attval; }
                                if (AttributeNAME == "LEDN_HØYDEREFERANSE") { LEDN_HØYDEREFERANSE_val = Attval; }
                                if (AttributeNAME == "EL_STASJONSTYPE") { EL_STASJONSTYPE_val = Attval; }
                                if (AttributeNAME == "MASTEFUNKSJON") { MASTEFUNKSJON_val = Attval; }

                                if (AttributeNAME == "MASTEKONSTRUKSJON") { MASTEKONSTRUKSJON_val = Attval; } //missed

                                if (AttributeNAME == "VNR") { VNR_val = Attval; }
                                if (AttributeNAME == "MAST_LUFTFARTSHINDERMERKING") { MAST_LUFTFARTSHINDERMERKING_val = Attval; }
                                if (AttributeNAME == "LEDNINGNETTVERKSTYPE") { LEDNINGNETTVERKSTYPE_val = Attval; }
                                if (AttributeNAME == "FELLESFØRING") { FELLESFØRING_val = Attval; }
                                if (AttributeNAME == "LEDNINGSNETTVERKSTYPE") { LEDNINGSNETTVERKSTYPE_val = Attval; }
                                if (AttributeNAME == "LEDN_LEIETAKER") { LEDN_LEIETAKER_val = Attval; }
                                if (AttributeNAME == "LINJEBREDDE") { LINJEBREDDE_val = Attval; }
                                if (AttributeNAME == "VEGREKKVERKTYPE") { VEGREKKVERKTYPE_val = Attval; }
                                if (AttributeNAME == "NEDSENKETKANTSTEIN") { NEDSENKETKANTSTEIN_val = Attval; }
                                if (AttributeNAME == "VEGKATEGORI") { VEGKATEGORI_val = Attval; }
                                if (AttributeNAME == "VEGSTATUS") { VEGSTATUS_val = Attval; }
                                if (AttributeNAME == "VEGNUMMER") { VEGNUMMER_val = Attval; }
                                if (AttributeNAME == "VEGOVERVEG") { VEGOVERVEG_val = Attval; }
                                if (AttributeNAME == "JERNBANETYPE") { JERNBANETYPE_val = Attval; }
                                if (AttributeNAME == "JERNBANEEIER") { JERNBANEEIER_val = Attval; }
                                if (AttributeNAME == "LHINST_TYPE") { LHINST_TYPE_val = Attval; }
                                if (AttributeNAME == "RESTR_OMR") { RESTR_OMR_val = Attval; }
                                if (AttributeNAME == "RETN") { RETN_val = Attval; }
                                if (AttributeNAME == "TWYMERK") { TWYMERK_val = Attval; }
                                if (AttributeNAME == "LHAREAL") { LHAREAL_val = Attval; }
                                if (AttributeNAME == "ANNENLUFTHAVN") { ANNENLUFTHAVN_val = Attval; }
                                if (AttributeNAME == "PLFMERK") { PLFMERK_val = Attval; }
                                if (AttributeNAME == "LHSKILTTYPE") { LHSKILTTYPE_val = Attval; }
                                if (AttributeNAME == "LHSKILTKATEGORI") { LHSKILTKATEGORI_val = Attval; }
                                if (AttributeNAME == "BYGGNR") { BYGGNR_val = Attval; }
                                if (AttributeNAME == "BYGGTYP_NBR") { BYGGTYP_NBR_val = Attval; }
                                if (AttributeNAME == "BYGGSTAT") { BYGGSTAT_val = Attval; }
                                if (AttributeNAME == "SEFRAK_ID") { SEFRAK_ID_val = Attval; }
                                if (AttributeNAME == "SEFRAKKOMMUNE") { SEFRAKKOMMUNE_val = Attval; }
                                if (AttributeNAME == "HUSLØPENR") { HUSLØPENR_val = Attval; }
                                if (AttributeNAME == "KOMM")
                                {
                                    if (KOMM_ValCnt == 0) { KOMM_val = Attval; KOMM_ValCnt++; }
                                    else { KOMM_2_val = Attval; }
                                }
                                if (AttributeNAME == "STED_VERIF") { STED_VERIF_val = Attval; }
                                if (AttributeNAME == "TAKSKJEGG") { TAKSKJEGG_val = Attval; }

                                if (AttributeNAME == "VEGSPERRINGTYPE") { VEGSPERRINGTYPE_val = Attval; }
                                if (AttributeNAME == "VPA") { VPA_val = Attval; }
                                if (AttributeNAME == "VKJORFLT") { VKJORFLT_val = Attval; }
                                if (AttributeNAME == "VFRADATO") { VFRADATO_val = Attval; }
                                if (AttributeNAME == "LBVKLASSE") { LBVKLASSE_val = Attval; }
                                //if (AttributeNAME == "") { _val = Attval; }
                                //if (AttributeNAME == "") { _val = Attval; }
                                //if (AttributeNAME == "") { _val = Attval; }
                                //if (AttributeNAME == "") { _val = Attval; }

                                //----------DEc23 dec--------------------------------------
                                if (AttributeNAME == "SLUSETYP") { SLUSETYP_val = Attval; }
                                if (AttributeNAME == "ENDRINGSFLAGG") { ENDRINGSFLAGG_val = Attval; }
                                if (AttributeNAME == "ENDRET_TYPE") { ENDRET_TYPE_val = Attval; }
                                if (AttributeNAME == "ENDRET_TID") { ENDRET_TID_val = Attval; }
                                if (AttributeNAME == "NETTVERKSTASJONTYPE") { NETTVERKSTASJONTYPE_val = Attval; }
                                if (AttributeNAME == "KONSTRUKSJONSHØYDE") { KONSTRUKSJONSHØYDE_val = Attval; }
                                if (AttributeNAME == "BELYSNINGSBRUK") { BELYSNINGSBRUK_val = Attval; }
                                if (AttributeNAME == "NEDSENKET") { NEDSENKET_val = Attval; }
                                if (AttributeNAME == "KANTSTEIN") { KANTSTEIN_val = Attval; }
                                if (AttributeNAME == "VTILDATO") { VTILDATO_val = Attval; }
                                if (AttributeNAME == "METER-TIL") { METER_TIL_val = Attval; }
                                if (AttributeNAME == "METER-FRA") { METER_FRA_val = Attval; }
                                if (AttributeNAME == "HOVEDPARSELL") { HOVEDPARSELL_val = Attval; }
                                if (AttributeNAME == "VLENKEID") { VLENKEID_val = Attval; }
                                if (AttributeNAME == "GATENR") { GATENR_val = Attval; }
                                if (AttributeNAME == "GATENAVN") { GATENAVN_val = Attval; }
                                if (AttributeNAME == "BRUKSKLASSE") { BRUKSKLASSE_val = Attval; }
                                if (AttributeNAME == "BRUKSKLASSEHELÅR") { BRUKSKLASSEHELÅR_val = Attval; }
                                if (AttributeNAME == "BRUKSKLASSEVINTER") { BRUKSKLASSEVINTER_val = Attval; }
                                if (AttributeNAME == "BRUKSKLASSETELE") { BRUKSKLASSETELE_val = Attval; }
                                if (AttributeNAME == "MAKSVOGNTOGLENGDE") { MAKSVOGNTOGLENGDE_val = Attval; }
                                if (AttributeNAME == "MAKSTOTALVEKT") { MAKSTOTALVEKT_val = Attval; }
                                if (AttributeNAME == "MAKSTOTALVEKTSKILTET") { MAKSTOTALVEKTSKILTET_val = Attval; }
                                if (AttributeNAME == "LINEÆRREFERANSE") { LINEÆRREFERANSE_val = Attval; }
                                if (AttributeNAME == "LINEÆRREFERANSETYPE") { LINEÆRREFERANSETYPE_val = Attval; }
                                if (AttributeNAME == "REFERANSEFRA") { REFERANSEFRA_val = Attval; }
                                if (AttributeNAME == "REFERANSETIL") { REFERANSETIL_val = Attval; }
                                if (AttributeNAME == "KJØREFELT") { KJØREFELT_val = Attval; }
                                if (AttributeNAME == "FARTSGRENSE") { FARTSGRENSE_val = Attval; }
                                if (AttributeNAME == "FARTSGRENSEVERDI") { FARTSGRENSEVERDI_val = Attval; }
                                if (AttributeNAME == "SVINGEFORBUDREFID") { SVINGEFORBUDREFID_val = Attval; }
                                if (AttributeNAME == "FORBUDRETNING") { FORBUDRETNING_val = Attval; }
                                if (AttributeNAME == "SKILTAHØYDE") { SKILTAHØYDE_val = Attval; }
                                if (AttributeNAME == "LHFDET") { LHFDET_val = Attval; }
                                if (AttributeNAME == "LHELEV") { LHELEV_val = Attval; }
                                if (AttributeNAME == "LHSKILTLYS") { LHSKILTLYS_val = Attval; }
                                if (AttributeNAME == "OPAREALAVGRTYPE") { OPAREALAVGRTYPE_val = Attval; }
                                if (AttributeNAME == "RWYMERK") { RWYMERK_val = Attval; }
                                if (AttributeNAME == "TYPEVEG") { TYPEVEG_val = Attval; }
                                if (AttributeNAME == "KONNEKTERINGSLENKE") { KONNEKTERINGSLENKE_val = Attval; }
                                if (AttributeNAME == "NVDB_KLASSELANDBRUKSVEG") { NVDB_KLASSELANDBRUKSVEG_val = Attval; }
                                if (AttributeNAME == "VEGLENKEADRESSE") { VEGLENKEADRESSE_val = Attval; }
                                if (AttributeNAME == "ADRESSEKODE") { ADRESSEKODE_val = Attval; }
                                if (AttributeNAME == "ADRESSENAVN") { ADRESSENAVN_val = Attval; }
                                if (AttributeNAME == "SIDEVEG") { SIDEVEG_val = Attval; }
                                if (AttributeNAME == "BARMARKSLØYPE") { BARMARKSLØYPE_val = Attval; }
                                if (AttributeNAME == "BELYSNING") { BELYSNING_val = Attval; }
                                if (AttributeNAME == "RUTEMERKING") { RUTEMERKING_val = Attval; }
                                //---------------jan 7--------------------------------------------
                                if (AttributeNAME == "OPPHAV") { OPPHAV_val = Attval; }
                                if (AttributeNAME == "NETTVERKSSTASJONADKOMSTTYPE") { NETTVERKSSTASJONADKOMSTTYPE_val = Attval; }
                                if (AttributeNAME == "BELYSNINGSPLASSERING") { BELYSNINGSPLASSERING_val = Attval; }
                                if (AttributeNAME == "KUMFUNKSJON") { KUMFUNKSJON_val = Attval; }
                                if (AttributeNAME == "TRASEBREDDE") { TRASEBREDDE_val = Attval; }
                                if (AttributeNAME == "MAX-AVVIK") { MAX_AVVIK_val = Attval; }
                                if (AttributeNAME == "PRODUKTSPEK") { PRODUKTSPEK_val = Attval; }
                                if (AttributeNAME == "KORTNAVN") { KORTNAVN_val = Attval; }
                                if (AttributeNAME == "OPPDATERT") { OPPDATERT_val = Attval; }
                                if (AttributeNAME == "HRV") { HRV_val = Attval; }
                                if (AttributeNAME == "LRV") { LRV_val = Attval; }
                                if (AttributeNAME == "PRODUKTNAVN") { PRODUKTNAVN_val = Attval; }
                                if (AttributeNAME == "BYGN_ENDR_LØPENR") { BYGN_ENDR_LØPENR_val = Attval; }
                                if (AttributeNAME == "BYGN_ENDR_KODE") { BYGN_ENDR_KODE_val = Attval; }
                                if (AttributeNAME == "SKAL_AVGR_BYGN") { SKAL_AVGR_BYGN_val = Attval; }
                                //----------------------------------------------------------------------------------
                                if (splitedfline[j].Split()[0] == "..OBJTYPE") { Objtype = Attval; }
                                else if (splitedfline[j].Split()[0] == "..TRE_D_NIVÅ") { Val_TRE_D_NIVA = Attval; }
                                else if (splitedfline[j].Split()[0] == "..KVALITET") { Val_KVALITET = Attval; }

                                else if (splitedfline[j].Split()[0] == "..DATAFANGSTDATO") { Val_DATAFANGSTDATO = Attval; }
                                else if (splitedfline[j].Split()[0] == "..REGISTRERINGSVERSJON") { Val_REGISTRERINGSVERSJON = Attval; }
                                else if (splitedfline[j].Split()[0] == "..MEDIUM") { MEDIUM = Attval; }
                                else if (splitedfline[j].Split()[0] == "..HREF") { HREF = Attval; }

                                else if (splitedfline[j].Split()[0] == "..STATUS") { STATUS_val = Attval; }
                                else if (splitedfline[j].Split()[0] == "..KOPIDATA") { KOPIDATA_avl = Attval; }
                                else if (splitedfline[j].Split()[0] == "...OMRÅDEID") { OMRADEID_val = Attval; }
                                else if (splitedfline[j].Split()[0] == "...ORIGINALDATAVERT") { ORIGINALDATAVERT_val = Attval; }
                                else if (splitedfline[j].Split()[0] == "...KOPIDATO") { KOPIDATO_val = Attval; }
                                else if (splitedfline[j].Split()[0] == "..OPPDATERINGSDATO") { OPPDATERINGSDATO_val = Attval; }
                                else if (splitedfline[j].Split()[0] == "..VERIFISERINGSDATO") { VERIFISERINGSDATO_val = Attval; }
                                else if (splitedfline[j].Split()[0] == "..INFORMASJON") { ppINFORMASJON = Attval; }
                                else if (splitedfline[j].Split()[0] == "..PROSESS_HISTORIE") { PROSESS_HISTORIE_val = Attval; }



                                else if (splitedfline[j].Split()[0] == "..KOMM") { ppKOMM = Attval; }
                                else if (splitedfline[j].Split()[0] == "..BYGGNR") { ppBYGGNR = Attval; }
                                else if (splitedfline[j].Split()[0] == "..BYGGSTAT") { ppBYGGSTAT = Attval; }
                                else if (splitedfline[j].Split()[0] == "..INFORMASJON") { ppINFORMASJON = Attval; }
                                    

                                //-----------------FLAT--------------------------

                                else if (splitedfline[j].Split()[0] == "...MÅLEMETODE") { MALEMETODE = Attval; }
                                else if (splitedfline[j].Split()[0] == "...NØYAKTIGHET") { NOYAKTIGHET = Attval; }
                                else if (splitedfline[j].Split()[0] == "...SYNBARHET") { SYNBARHET = Attval; }
                                else if (splitedfline[j].Split()[0] == "...H-MÅLEMETODE") { HMALEMETODE = Attval; }
                                else if (splitedfline[j].Split()[0] == "...H-NØYAKTIGHET") { HNOYAKTIGHET = Attval; }

                                else if (splitedfline[j].Split()[0] == "..SEFRAK_ID") { SEFRAK_ID = Attval; }
                                else if (splitedfline[j].Split()[0] == "...SEFRAKKOMMUNE") { SEFRAKKOMMUNE = Attval; }
                                else if (splitedfline[j].Split()[0] == "...REGISTRERINGKRETSNR") { REGISTRERINGKRETSNR = Attval; REGISTRERINGKRETSNR_val = Attval; }
                                else if (splitedfline[j].Split()[0] == "...HUSLØPENR") { HUSLØPENR = Attval; }

                                else if (splitedfline[j].Split()[0] == "..BYGGTYP_NBR") { BYGGTYP_NBR = Attval; }
                                else if (splitedfline[j].Split()[0] == "..MATRIKKEL-ID") { MATRIKKEL_ID = Attval; }


                                else if (splitedfline[j].Split()[0] == "..REF")
                                {
                                    ppREF = Attval;
                                    if (Attval.Length > 250)
                                    {
                                        ppREF = Attval.Substring(0, 250);
                                        if (Attval.Length < 500) { ppREF1 = Attval.Substring(251); }
                                        else if (Attval.Length < 750) { ppREF1 = Attval.Substring(251, 250); ppREF1 = Attval.Substring(501); }
                                        else if (Attval.Length < 1000) { ppREF1 = Attval.Substring(251, 250); ppREF2 = Attval.Substring(501, 250); ppREF3 = Attval.Substring(751); }
                                        else { ppREF1 = Attval.Substring(251, 250); ppREF2 = Attval.Substring(501, 250); ppREF3 = Attval.Substring(751, 250); }

                                    }
                                    for (int m = j + 1; m < splitedfline.Length - 1; m++)
                                    {
                                        string firstpart1 = splitedfline[m].Split()[0].Trim();
                                        string aatname1 = splitedfline[m].Split()[0].Trim();
                                        if (aatname1.Trim().Substring(0, 1) != ".")
                                        {
                                            if (m == j + 1) { ppREF1 = splitedfline[m]; }
                                            if (m == j + 2) { ppREF2 = splitedfline[m]; }
                                            if (m == j + 3) { ppREF3 = splitedfline[m]; }

                                            if (m == j + 4) { ppREF4 = splitedfline[m]; }
                                            if (m == j + 5) { ppREF5 = splitedfline[m]; }
                                            if (m == j + 6) { ppREF6 = splitedfline[m]; }
                                            if (m == j + 7) { ppREF7 = splitedfline[m]; }
                                            if (m == j + 8) { ppREF8 = splitedfline[m]; }
                                            if (m == j + 9) { ppREF9 = splitedfline[m]; }
                                            if (m == j + 10) { ppREF10 = splitedfline[m]; }
                                            if (m == j + 11) { ppREF11 = splitedfline[m]; }
                                            if (m == j + 12) { ppREF12 = splitedfline[m]; }
                                            if (m == j + 13) { ppREF13 = splitedfline[m]; }
                                            if (m == j + 14) { ppREF14 = splitedfline[m]; }
                                            if (m == j + 15) { ppREF15 = splitedfline[m]; }
                                            if (m == j + 16) { ppREF16 = splitedfline[m]; }
                                            if (m == j + 17) { ppREF17 = splitedfline[m]; }
                                            if (m == j + 18) { ppREF18 = splitedfline[m]; }
                                            if (m == j + 19) { ppREF19 = splitedfline[m]; }
                                            if (m == j + 20) { ppREF20 = splitedfline[m]; }
                                            if (m == j + 21) { ppREF21 = splitedfline[m]; }
                                            if (m == j + 22) { ppREF22 = splitedfline[m]; }
                                            if (m == j + 23) { ppREF23 = splitedfline[m]; }
                                            if (m == j + 24) { ppREF24 = splitedfline[m]; }
                                            if (m == j + 25) { ppREF25 = splitedfline[m]; }
                                            if (m == j + 26) { ppREF26 = splitedfline[m]; }
                                            if (m == j + 27) { ppREF27 = splitedfline[m]; }
                                            if (m == j + 28) { ppREF28 = splitedfline[m]; }
                                            if (m == j + 29) { ppREF29 = splitedfline[m]; }
                                            if (m == j + 30) { ppREF30 = splitedfline[m]; }

                                            if (m == j + 31) { ppREF31 = splitedfline[m]; }
                                            if (m == j + 32) { ppREF32 = splitedfline[m]; }
                                            if (m == j + 33) { ppREF33 = splitedfline[m]; }
                                            if (m == j + 34) { ppREF34 = splitedfline[m]; }
                                            if (m == j + 35) { ppREF35 = splitedfline[m]; }

                                            if (m == j + 36) { ppREF36 = splitedfline[m]; }
                                            if (m == j + 37) { ppREF37 = splitedfline[m]; }
                                            if (m == j + 38) { ppREF38 = splitedfline[m]; }
                                            if (m == j + 39) { ppREF39 = splitedfline[m]; }
                                            if (m == j + 40) { ppREF40 = splitedfline[m]; }
                                            if (m == j + 41) { ppREF41 = splitedfline[m]; }
                                            if (m == j + 42) { ppREF42 = splitedfline[m]; }
                                            if (m == j + 43) { ppREF43 = splitedfline[m]; }
                                            if (m == j + 44) { ppREF44 = splitedfline[m]; }
                                            if (m == j + 45) { ppREF45 = splitedfline[m]; }
                                            if (m == j + 46) { ppREF46 = splitedfline[m]; }
                                            if (m == j + 47) { ppREF47 = splitedfline[m]; }
                                            if (m == j + 48) { ppREF48 = splitedfline[m]; }
                                            if (m == j + 49) { ppREF49 = splitedfline[m]; }
                                            if (m == j + 50) { ppREF50 = splitedfline[m]; }

                                          
                                        }
                                    }

                                }
                                //---------------------------------------[KOPIDATA],[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO]
                            }
                            //----------Place feature-------------------------------

                            //------------------------------Get Layer------------------------------------------------------------
                            string DGNlay = Objtype;
                            // DGNlay = "Bygning";
                            DataRow[] oRow1 = DataTableLayerList.Select("fname_no='" + Objtype.Trim() + "'"); //Bygning
                            if (Datagroup == "KURVE")
                            {

                                oRow1 = DataTableLayerList.Select("fname_no='" + Objtype.Trim() + "'" + " AND Geometri_type=" + "'Line'");
                            }
                            else if (Datagroup == "FLATE")
                            {
                                oRow1 = DataTableLayerList.Select("fname_no='" + Objtype.Trim() + "'" + " AND Geometri_type=" + "'Point'");
                                //if (oRow1.Length == 0)
                                //{
                                //    oRow1 = DataTableLayerList.Select("fname_no='" + Objtype.Trim() + "'");
                                //}
                            }
                            //else if (Datagroup == "PUNKT") //20150512
                            //{
                            //    oRow1 = DataTableLayerList.Select("fname_no='" + Objtype.Trim() + "'" + " AND Geometri_type=" + "'Point'");
                            //    if (oRow1.Length == 0)
                            //    {
                            //        oRow1 = DataTableLayerList.Select("fname_no='" + Objtype.Trim() + "'");
                            //    }
                            //}
                            

                            //DataRow[] oRow1 = DataTableLayerList.Select("fname_no ='Bygning'"); //Bygning
                            string Cellname = "";
                            string CellScale = "";
                            if (oRow1.Length == 1)
                            {
                                DGNlay = oRow1[0]["fname"].ToString();
                                //Colorcoad = oRow1[0]["color"].ToString();
                                Cellname = oRow1[0]["fcellname"].ToString();
                                CellScale = oRow1[0]["CellScale"].ToString();
                            }
                            if (oRow1.Length > 1)
                            {
                                DGNlay = oRow1[0]["fname"].ToString();
                                Cellname = oRow1[0]["fcellname"].ToString();
                                CellScale = oRow1[0]["CellScale"].ToString();
                                for (int w = 0; w < oRow1.Length; w++)
                                {

                                    

                                    if (oRow1[w]["TRE_D_NIVÅ"].ToString().Trim() == Val_TRE_D_NIVA.Trim() && oRow1[w]["MEDIUM"].ToString().Trim() == MEDIUM.Trim() && oRow1[w]["HREF"].ToString().Trim() == HREF.Trim() && oRow1[w]["VANNBR"].ToString().Trim() == VANNBR_val.Trim() && oRow1[w]["LHLYSRETN"].ToString().Trim() == LHLYSRETN_val.Trim() && oRow1[w]["LHLYSFARGE"].ToString().Trim() == LHLYSFARGE_val.Trim() && oRow1[w]["LHLYS_OPPHØYD_NEDFELT"].ToString().Trim() == LHLYS_OPPHØYD_NEDFELT_val.Trim() && oRow1[w]["LHLYSTYPE"].ToString().Trim() == LHLYSTYPE_val.Trim() && oRow1[w]["KYSTKONSTRUKSJONSTYPE"].ToString().Trim() == KYSTKONSTRUKSJONSTYPE_val.Trim() && oRow1[w]["TRE_TYP"].ToString().Trim() == TRE_TYP_val.Trim() && oRow1[w]["SKJERMINGFUNK"].ToString().Trim() == SKJERMINGFUNK_val.Trim() && oRow1[w]["LHINST_TYPE"].ToString().Trim() == LHINST_TYPE_val.Trim())
                                    {
                                        DGNlay = oRow1[w]["fname"].ToString();
                                        Cellname = oRow1[w]["fcellname"].ToString();
                                        CellScale = oRow1[w]["CellScale"].ToString();
                                    }

                                }
                                if (Objtype.Trim() == "ElvBekk" || Objtype.Trim() == "ElvBekkKant" || Objtype.Trim() == "KanalGrøft" || Objtype.Trim() == "KanalGrøftKant")//ElvBekk ElvBekkKant KanalGrøft KanalGrøftKant
                                {
                                    for (int w = 0; w < oRow1.Length; w++)
                                    {
                                        if (oRow1[w]["VANNBR"].ToString().Trim() == VANNBR_val.Trim())
                                        {
                                            DGNlay = oRow1[w]["fname"].ToString();
                                        }
                                    }
                                }
                                else if (Objtype.Trim() == "Veglenke")
                                {
                                    for (int w = 0; w < oRow1.Length; w++)
                                    {
                                        if (oRow1[w]["TYPEVEG"].ToString().Trim() == TYPEVEG_val.Trim())
                                        {
                                            DGNlay = oRow1[w]["fname"].ToString();
                                        }
                                    }
                                }
                                else if (Objtype.Trim() == "Voll" || Objtype.Trim() == "Skjerm")
                                {
                                    for (int w = 0; w < oRow1.Length; w++)
                                    {
                                        if (oRow1[w]["SKJERMINGFUNK"].ToString().Trim() == SKJERMINGFUNK_val.Trim())
                                        {
                                            DGNlay = oRow1[w]["fname"].ToString();
                                        }
                                    }
                                }
                                else if (Objtype.Trim() == "Bygningslinje" || Objtype.Trim() == "Hjelpelinje3D" || Objtype.Trim() == "Hjelpepunkt3D" || Objtype.Trim() == "Mønelinje" || Objtype.Trim() == "Takplatå" || Objtype.Trim() == "Taksprang" || Objtype.Trim() == "TaksprangBunn" || Objtype.Trim() == "TakplatåTopp")
                                {
                                    for (int w = 0; w < oRow1.Length; w++)
                                    {
                                        if (oRow1[w]["TRE_D_NIVÅ"].ToString().Trim() == Val_TRE_D_NIVA.Trim())
                                        {
                                            DGNlay = oRow1[w]["fname"].ToString();
                                        }
                                    }
                                }
                                else if (Objtype.Trim() == "FiskehjellGrense" || Objtype.Trim() == "BeskrivendeHjelpelinjeAnlegg") //Objtype.Trim() == "Veranda" || 20150522
                                {
                                    for (int w = 0; w < oRow1.Length; w++)
                                    {
                                        if (oRow1[w]["HREF"].ToString().Trim() == HREF.Trim())
                                        {
                                            DGNlay = oRow1[w]["fname"].ToString();
                                        }
                                    }
                                }//
                                else if (Objtype.Trim() == "Veranda") // || 20150522
                                {
                                    for (int w = 0; w < oRow1.Length; w++)
                                    {
                                        if (oRow1[w]["MEDIUM"].ToString().Trim() == MEDIUM.Trim())
                                        {
                                            DGNlay = oRow1[w]["fname"].ToString();
                                        }
                                    }
                                }//
                                else if (Objtype.Trim() == "LufthavnInstrumentGrense" || Objtype.Trim() == "LufthavnInstrument")
                                {
                                    for (int w = 0; w < oRow1.Length; w++)
                                    {
                                        if (oRow1[w]["LHINST_TYPE"].ToString().Trim() == LHINST_TYPE_val.Trim())
                                        {
                                            DGNlay = oRow1[w]["fname"].ToString();
                                        }
                                    }
                                }//
                                else if (Objtype.Trim() == "Lufthavnlys" )
                                {
                                    for (int w = 0; w < oRow1.Length; w++)
                                    {
                                        if ((oRow1[w]["LHLYSFARGE"].ToString().Trim() == LHLYSFARGE_val.Trim()) && (oRow1[w]["LHLYS_OPPHØYD_NEDFELT"].ToString().Trim() == LHLYS_OPPHØYD_NEDFELT_val.Trim()) && (oRow1[w]["LHLYSTYPE"].ToString().Trim() == LHLYSTYPE_val.Trim()))
                                        {
                                            DGNlay = oRow1[w]["fname"].ToString();
                                        }
                                    }
                                }
                                //--------20150512--------------
                                else if (Objtype.Trim() == "EL_Belysningspunkt")
                                {
                                    for (int w = 0; w < oRow1.Length; w++)
                                    {
                                        if ((oRow1[w]["BELYSNINGSPLASSERING"].ToString().Trim() == BELYSNINGSPLASSERING_val.Trim()) && (oRow1[w]["LEDN_HØYDEREFERANSE"].ToString().Trim() == LEDN_HØYDEREFERANSE_val.Trim()))
                                        {
                                            DGNlay = oRow1[w]["fname"].ToString();
                                        }
                                    }
                                }
                                else if (Objtype.Trim() == "EL_Nettstasjon")
                                {
                                    for (int w = 0; w < oRow1.Length; w++)
                                    {
                                        if (oRow1[w]["EL_STASJONSTYPE"].ToString().Trim() == EL_STASJONSTYPE_val.Trim())
                                        {
                                             DGNlay = oRow1[w]["fname"].ToString();
                                        }
                                    }
                                }
                                else if (Objtype.Trim() == "KystkonturTekniskeAnlegg")
                                {
                                    for (int w = 0; w < oRow1.Length; w++)
                                    {
                                        if (oRow1[w]["KYSTKONSTRUKSJONSTYPE"].ToString().Trim() == KYSTKONSTRUKSJONSTYPE_val.Trim())
                                        {
                                            DGNlay = oRow1[w]["fname"].ToString();
                                        }
                                    }
                                }
                                //
                                //--------------------
                            }
                            //------------------------------------Get color-----------------------------------------------------------------
                            string Colorcoad = "1";
                            int MSTNC_COL = 1;
                            DataRow[] oRow22 = DataTableFeature.Select("fname='" + DGNlay + "'");
                            if (oRow22.Length == 1)
                            {
                                Colorcoad = oRow22[0]["fcolor"].ToString();
                            }

                            if (Colorcoad != "") { MSTNC_COL = Convert.ToInt32(Colorcoad); }

                            //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            if(DGNlay==""){DGNlay="Default";}
                            if (Datagroup == "FLATE") { DGNlay = DGNlay + "_Existing"; }


                            //-------------------20150512---------------------------------------

                            if (Objtype.Trim() == "Bygning" || Objtype == "Veranda")
                            {
                            }
                            else
                            {
                                if (Datagroup != "FLATE")
                                {

                                    if (MEDIUM == "U")
                                    {
                                        DGNlay = "20" + DGNlay;
                                    }
                                    else if (MEDIUM == "L")
                                    {
                                        DGNlay = "30" + DGNlay;
                                    }
                                    else if (MEDIUM == "B")
                                    {
                                        DGNlay = "40" + DGNlay;
                                    }
                                }
                            }
                            //------------------------------------------------------------------


                            if (mApp.ActiveDesignFile.Levels.Find(DGNlay, null) == null)
                            {
                                mApp.ActiveDesignFile.AddNewLevel(DGNlay);
                                mApp.ActiveDesignFile.Levels.Rewrite();
                            }
                            Level FLv = mApp.ActiveDesignFile.Levels[DGNlay];
                            string DGN_ELE_ID_Val = "";
                            DatabaseLink DLink_ForNew = mApp.CreateDatabaseLink(Mlink, 1, MsdDatabaseLinkage.msdDatabaseLinkageOdbc, true, 1);
                            if (Datagroup == "KURVE")
                            {
                                DatabaseLink DLink = mApp.CreateDatabaseLink(Mlink, 6, MsdDatabaseLinkage.msdDatabaseLinkageOdbc, true, 1);
                                
                                LineElement L1 = null;
                                if (Vlist.Length < 5000)
                                {
                                    if (Vlist.Length == 1)
                                    {
                                        Array.Resize(ref Vlist, 2);
                                        Vlist[1] = Vlist[0];
                                    }
                                    Array VlistArray = Vlist as Array;
                                    L1 = mApp.CreateLineElement1(null, ref VlistArray);
                                    if ((DOF_NewFeature == Val_DATAFANGSTDATO) && DOF_NewFeature != "")
                                    {
                                        L1.AddDatabaseLink(DLink_ForNew);
                                        L1.LineWeight = 4;
                                    }
                                    else
                                    {
                                        L1.AddDatabaseLink(DLink);
                                        L1.LineWeight = 0;
                                    }
                                    //L1.AddDatabaseLink(DLink);
                                    L1.Level = FLv;
                                    L1.Color = MSTNC_COL;
                                    mApp.ActiveModelReference.AddElement((_Element)L1);
                                    L1.Redraw();
                                    linecnt++;
                                    DGN_ELE_ID_Val = L1.ID.Low.ToString();

                                    //------------jan 07-------------------------------------------------------------
                                    Array aDataBlocks;
                                    aDataBlocks = L1.GetUserAttributeData(SOSIFileNameID);
                                    if (aDataBlocks.Length > 0) { L1.DeleteUserAttributeData(SOSIFileNameID, 0); }
                                    MicroStationDGN.DataBlock oDataBlock;
                                    oDataBlock = new MicroStationDGN.DataBlock();

                                    oDataBlock.CopyString(ref SOSIfilename, true);
                                    L1.AddUserAttributeData(SOSIFileNameID, oDataBlock);
                                    L1.Rewrite();
                                    //-----------------------------Feb, 25 2015---------for complex chain handeling vertex >5000---------------------------------------
                                }
                                else
                                {
                                    Point3d[] NewVlist = null;
                                    int q = 0;
                                    for (int y = 0; y < Vlist.Length; y++)
                                    {
                                        if (q < 5000)
                                        {
                                            Array.Resize(ref NewVlist, q + 1);
                                            NewVlist[q] = Vlist[y];
                                            if (q == 4999 || y == Vlist.Length - 1)
                                            {
                                                Array NewVlistarray = NewVlist as Array;

                                                L1 = mApp.CreateLineElement1(null, ref NewVlistarray);

                                                if ((DOF_NewFeature == Val_DATAFANGSTDATO) && DOF_NewFeature != "")
                                                {
                                                    L1.AddDatabaseLink(DLink_ForNew);
                                                    L1.LineWeight = 4;
                                                }
                                                else
                                                {
                                                    L1.AddDatabaseLink(DLink);
                                                    L1.LineWeight = 0;
                                                }


                                                //L1.AddDatabaseLink(DLink);
                                                L1.Level = FLv;
                                                L1.Color = MSTNC_COL;
                                                mApp.ActiveModelReference.AddElement((_Element)L1);
                                                L1.Redraw();
                                                linecnt++;
                                                DGN_ELE_ID_Val = L1.ID.Low.ToString();

                                                Array aDataBlocks;
                                                aDataBlocks = L1.GetUserAttributeData(SOSIFileNameID);
                                                if (aDataBlocks.Length > 0) { L1.DeleteUserAttributeData(SOSIFileNameID, 0); }
                                                MicroStationDGN.DataBlock oDataBlock;
                                                oDataBlock = new MicroStationDGN.DataBlock();

                                                oDataBlock.CopyString(ref SOSIfilename, true);
                                                L1.AddUserAttributeData(SOSIFileNameID, oDataBlock);
                                                L1.Rewrite();


                                                NewVlist = null;
                                                q = 0;
                                                if (y != Vlist.Length - 1) { y = y - 1; }
                                            }
                                            else
                                            {
                                                q++;
                                            }
                                        }

                                    }
                                }
                                //---------------------------------------
                                OleDbCommand cmd = conn.CreateCommand();

                                string commtext1 = @"INSERT INTO line([SOSI_FILE_NAME],[MSLINK],[DGN_ELE_ID], [OBJID],[OBJTYPE],";
                                string commtextval1 = SOSIfilename +"','"  + Mlink + "','" + DGN_ELE_ID_Val + "','" + DataId + "','" + Objtype + "','";
                                string commtext2 = "[TRE_D_NIVÅ],[KVALITET],[DATAFANGSTDATO],[REGISTRERINGSVERSJON],[REGISTRERINGKRETSNR],";
                                string commtextval2 = Val_TRE_D_NIVA + "','" + Val_KVALITET + "','" + Val_DATAFANGSTDATO + "','" + Val_REGISTRERINGSVERSJON + "','" + REGISTRERINGKRETSNR_val+"','"; 
                                string commtext3 = "[MEDIUM],[HREF],[STATUS],[KOPIDATA],";
                                string commtextval3 = MEDIUM + "','" + HREF + "','" + STATUS_val + "','" + KOPIDATA_avl + "','";
                                string commtext4 = "[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO],[OPPDATERINGSDATO],";
                                string commtextval4 = OMRADEID_val + "','" + ORIGINALDATAVERT_val + "','" + KOPIDATO_val + "','" + OPPDATERINGSDATO_val + "','";
                                string commtext5 = "[VERIFISERINGSDATO],[INFORMASJON],[PROSESS_HISTORIE],[MATRIKKEL-ID],";
                                string commtextval5 = VERIFISERINGSDATO_val + "','" + ppINFORMASJON + "','" + PROSESS_HISTORIE_val + "','" + MATRIKKEL_ID+"','";

                                string commtext6 = "[VANNBR],[LHLYSRETN],[LHLYSFARGE],[LHLYS_OPPHØYD_NEDFELT],";
                                string commtextval6 = VANNBR_val + "','" + LHLYSRETN_val + "','" + LHLYSRETN_val + "','" + LHLYS_OPPHØYD_NEDFELT_val + "','";
                                string commtext7 = "[LHLYSTYPE],[HØYDE],[KYSTREF],[KYSTKONSTRUKSJONSTYPE],";
                                string commtextval7 = LHLYSTYPE_val + "','" + HØYDE_val + "','" + KYSTREF_val + "','" + KYSTKONSTRUKSJONSTYPE_val + "','";
                                string commtext8 = "[TRE_TYP],[BRUTRAFIKKTYPE],[BRUOVERBRU],[SKJERMINGFUNK],";
                                string commtextval8 = TRE_TYP_val + "','" + BRUTRAFIKKTYPE_val + "','" + BRUOVERBRU_val + "','" + SKJERMINGFUNK_val + "','";
                                string commtext9 = "[HOB],[IDENTIFIKASJON],[LOKALID],[NAVNEROM],";
                                string commtextval9 = HOB_val + "','" + IDENTIFIKASJON_val + "','" + LOKALID_val + "','" + NAVNEROM_val + "','";
                                string commtext10 = "[VERSJONID],[LEDN_EIER],[LEDN_EIERTYPE],[LEDN_EIERNAVN],";
                                string commtextval10 = VERSJONID_val + "','" + LEDN_EIER_val + "','" + LEDN_EIERTYPE_val + "','" + LEDN_EIERNAVN_val + "','";
                                string commtext11 = "[LEDN_EIERANDEL],[DRIFTSMERKING],[LEDN_HØYDEREFERANSE],[EL_STASJONSTYPE],";
                                string commtextval11 = LEDN_EIERANDEL_val + "','" + DRIFTSMERKING_val + "','" + LEDN_HØYDEREFERANSE_val + "','" + EL_STASJONSTYPE_val + "','";
                                string commtext12 = "[MASTEFUNKSJON],[MASTEKONSTRUKSJON],[VNR],[MAST_LUFTFARTSHINDERMERKING],";
                                string commtextval12 = MASTEFUNKSJON_val + "','" + MASTEKONSTRUKSJON_val + "','" + VNR_val + "','" + MAST_LUFTFARTSHINDERMERKING_val + "','";
                                string commtext13 = "[LEDNINGNETTVERKSTYPE],[FELLESFØRING],[LEDNINGSNETTVERKSTYPE],[LEDN_LEIETAKER],";
                                string commtextval13 = LEDNINGNETTVERKSTYPE_val + "','" + FELLESFØRING_val + "','" + LEDNINGSNETTVERKSTYPE_val + "','" + LEDN_LEIETAKER_val + "','";
                                string commtext14 = "[LINJEBREDDE],[VEGREKKVERKTYPE],[NEDSENKETKANTSTEIN],[VEGKATEGORI],";
                                string commtextval14 = LINJEBREDDE_val + "','" + VEGREKKVERKTYPE_val + "','" + NEDSENKETKANTSTEIN_val + "','" + VEGKATEGORI_val + "','";
                                string commtext15 = "[VEGSTATUS],[VEGNUMMER],[VEGOVERVEG],[JERNBANETYPE],";
                                string commtextval15 = VEGSTATUS_val + "','" + VEGNUMMER_val + "','" + VEGOVERVEG_val + "','" + JERNBANETYPE_val + "','";
                                string commtext16 = "[JERNBANEEIER],[LHINST_TYPE],[RESTR_OMR],[RETN],";
                                string commtextval16 = JERNBANEEIER_val + "','" + LHINST_TYPE_val + "','" + RESTR_OMR_val + "','" + RETN_val + "','";
                                string commtext17 = "[TWYMERK], [LHAREAL],[ANNENLUFTHAVN],[PLFMERK],";
                                string commtextval17 = TWYMERK_val + "','" + LHAREAL_val + "','" + ANNENLUFTHAVN_val + "','" + PLFMERK_val + "','";
                                string commtext18 = "[LHSKILTTYPE],[LHSKILTKATEGORI],[BYGGNR],[BYGGTYP_NBR],";
                                string commtextval18 = LHSKILTTYPE_val + "','" + LHSKILTKATEGORI_val + "','" + BYGGNR_val + "','" + BYGGTYP_NBR_val + "','";
                                string commtext19 = "[BYGGSTAT],[SEFRAK_ID],[SEFRAKKOMMUNE],[HUSLØPENR],";
                                string commtextval19 = BYGGSTAT_val + "','" + SEFRAK_ID_val + "','" + SEFRAKKOMMUNE_val + "','" + HUSLØPENR_val + "','";

                                string commtext199 = "[VEGSPERRINGTYPE],[VPA],[VKJORFLT],[VFRADATO],[LBVKLASSE],[KOMM_2],";
                                string commtextval199 = VEGSPERRINGTYPE_val + "','" + VPA_val + "','" + VKJORFLT_val + "','" + VFRADATO_val + "','" + LBVKLASSE_val + "','" + KOMM_2_val + "','";


                                string commtext20 = "[KOMM],[STED_VERIF],[TAKSKJEGG],";
                                string commtextval20 = KOMM_val + "','" + STED_VERIF_val + "','" + TAKSKJEGG_val + "','";


                                //--------------dec 23---------------
                                 string commtext21 = "[SLUSETYP],[ENDRINGSFLAGG],[ENDRET_TYPE],[ENDRET_TID],";
                                 string commtextval21=SLUSETYP_val+ "','" +ENDRINGSFLAGG_val+ "','" +ENDRET_TYPE_val+ "','" +ENDRET_TID_val+ "','" ;

                                 string commtext22 = "[NETTVERKSTASJONTYPE],[KONSTRUKSJONSHØYDE],[BELYSNINGSBRUK],[NEDSENKET],";
                                 string commtextval22=NETTVERKSTASJONTYPE_val+ "','" +KONSTRUKSJONSHØYDE_val+ "','" +BELYSNINGSBRUK_val+ "','" +NEDSENKET_val+ "','" ;

                                 string commtext23 = "[KANTSTEIN],[VTILDATO],[METER-TIL],[METER-FRA],";
                                 string commtextval23=KANTSTEIN_val+ "','" +VTILDATO_val+ "','" +METER_TIL_val+ "','" +METER_FRA_val+ "','" ;
                                
                                 
                                 string commtext24 = "[HOVEDPARSELL],[VLENKEID],[GATENR],[GATENAVN],";
                                 string commtextval24=HOVEDPARSELL_val+ "','" +VLENKEID_val+ "','" +GATENR_val+ "','" +GATENAVN_val+ "','" ;

                                 

                                 string commtext25 = "[BRUKSKLASSE],[BRUKSKLASSEHELÅR],[BRUKSKLASSEVINTER],[BRUKSKLASSETELE],";
                                 string commtextval25=BRUKSKLASSE_val+ "','" +BRUKSKLASSEHELÅR_val+ "','" +BRUKSKLASSEVINTER_val+ "','" +BRUKSKLASSETELE_val+ "','" ;

                                 string commtext26 = "[MAKSVOGNTOGLENGDE],[MAKSTOTALVEKT],[MAKSTOTALVEKTSKILTET],[LINEÆRREFERANSE],";
                                 string commtextval26=MAKSVOGNTOGLENGDE_val+ "','" +MAKSTOTALVEKT_val+ "','" +MAKSTOTALVEKTSKILTET_val+ "','" +LINEÆRREFERANSE_val+ "','" ;

                                 string commtext27 = "[LINEÆRREFERANSETYPE],[REFERANSEFRA],[REFERANSETIL],[KJØREFELT],";
                                 string commtextval27=LINEÆRREFERANSETYPE_val+ "','" +REFERANSEFRA_val+ "','" +REFERANSETIL_val+ "','" +KJØREFELT_val+ "','" ;

                                 string commtext28 = "[FARTSGRENSE],[FARTSGRENSEVERDI],[SVINGEFORBUDREFID],[FORBUDRETNING],";
                                 string commtextval28 = FARTSGRENSE_val + "','" + FARTSGRENSEVERDI_val + "','" + SVINGEFORBUDREFID_val + "','" + FORBUDRETNING_val + "','";

                                 string commtext29 = "[SKILTAHØYDE],[LHFDET],[LHELEV],[LHSKILTLYS],";
                                 string commtextval29=SKILTAHØYDE_val+ "','" +LHFDET_val+ "','" +LHELEV_val+ "','" +LHSKILTLYS_val+ "','" ;

                                 string commtext30 = "[OPAREALAVGRTYPE],[RWYMERK],[TYPEVEG],[KONNEKTERINGSLENKE],";
                                 string commtextval30=OPAREALAVGRTYPE_val+ "','" +RWYMERK_val+ "','" +TYPEVEG_val+ "','" +KONNEKTERINGSLENKE_val+ "','" ;

                                 string commtext31 = "[NVDB_KLASSELANDBRUKSVEG],[VEGLENKEADRESSE],[ADRESSEKODE],[ADRESSENAVN],";
                                 string commtextval31=NVDB_KLASSELANDBRUKSVEG_val+ "','" +VEGLENKEADRESSE_val+ "','" +ADRESSEKODE_val+ "','" +ADRESSENAVN_val+ "','" ;

                                 string commtext32 = "[SIDEVEG],[BARMARKSLØYPE],[BELYSNING],[RUTEMERKING],";
                                 string commtextval32 = SIDEVEG_val + "','" + BARMARKSLØYPE_val + "','" + BELYSNING_val + "','" + RUTEMERKING_val + "','";

                               
                                //---------jan 07--
                                 string commtext33 = "[OPPHAV],[NETTVERKSSTASJONADKOMSTTYPE],[BELYSNINGSPLASSERING],[KUMFUNKSJON],";
                                 string commtextval33 =  OPPHAV_val + "','" + NETTVERKSSTASJONADKOMSTTYPE_val + "','"+BELYSNINGSPLASSERING_val + "','" + KUMFUNKSJON_val + "','";
                                 string commtext34 = "[TRASEBREDDE],[MAX-AVVIK],[PRODUKTSPEK],[KORTNAVN],[OPPDATERT],[HRV],";
                                 string commtextval34 =TRASEBREDDE_val + "','"+MAX_AVVIK_val+ "','"+PRODUKTSPEK_val + "','"+KORTNAVN_val + "','"+OPPDATERT_val + "','"+ HRV_val + "','";
                                 string commtext35 = "[LRV],[PRODUKTNAVN],[BYGN_ENDR_LØPENR],[BYGN_ENDR_KODE],[SKAL_AVGR_BYGN])VALUES('";
                                 string commtextval35 = LRV_val + "','" + PRODUKTNAVN_val + "','" + BYGN_ENDR_LØPENR_val + "','" + BYGN_ENDR_KODE_val + "','" + SKAL_AVGR_BYGN_val + "')";


                                 string finalcommand = commtext1 + commtext2 + commtext3 + commtext4 + commtext5 + commtext6 + commtext7 + commtext8 + commtext9 + commtext10 + commtext11 + commtext12 + commtext13 + commtext14 + commtext15 + commtext16 + commtext17 + commtext18 + commtext19 + commtext199 + commtext20 + commtext21 + commtext22 + commtext23 + commtext24 + commtext25 + commtext26 + commtext27 + commtext28 + commtext29 + commtext30 + commtext31 + commtext32 + commtext33 + commtext34 + commtext35;
                                 string finalcommandval = commtextval1 + commtextval2 + commtextval3 + commtextval4 + commtextval5 + commtextval6 + commtextval7 + commtextval8 + commtextval9 + commtextval10 + commtextval11 + commtextval12 + commtextval13 + commtextval14 + commtextval15 + commtextval16 + commtextval17 + commtextval18 + commtextval19 + commtextval199 + commtextval20 + commtextval21 + commtextval22 + commtextval23 + commtextval24 + commtextval25 + commtextval26 + commtextval27 + commtextval28 + commtextval29 + commtextval30 + commtextval31 + commtextval32 + commtextval33 + commtextval34 + commtextval35;
                                string Quearystr = finalcommand + finalcommandval;
                                cmd.CommandText = Quearystr;
                                cmd.ExecuteNonQuery();

                                //cmd.CommandText = @"INSERT INTO line([MSLINK],[DGN_ELE_ID], [OBJID],[OBJTYPE],[TRE_D_NIVÅ],[KVALITET],[MÅLEMETODE],[NØYAKTIGHET],[SYNBARHET],[H-MÅLEMETODE],[H-NØYAKTIGHET],[DATAFANGSTDATO],[REGISTRERINGSVERSJON],[MEDIUM],[HREF])VALUES('" + Mlink + "','" + DGN_ELE_ID_Val + "','" + DataId + "','" + Objtype + "','" + Val_TRE_D_NIVA + "','" + Val_KVALITET + "','" + MALEMETODE + "','" + NOYAKTIGHET + "','" + SYNBARHET + "','" + HMALEMETODE + "','" + HNOYAKTIGHET + "','" + Val_DATAFANGSTDATO + "','" + Val_REGISTRERINGSVERSJON + "','" + MEDIUM + "','" + HREF + "')";
                                ////string commtext1 = @"INSERT INTO line([MSLINK],[DGN_ELE_ID], [OBJID],[OBJTYPE],[TRE_D_NIVÅ],[KVALITET],[DATAFANGSTDATO],[REGISTRERINGSVERSJON],[MEDIUM],[HREF],[STATUS],[KOPIDATA],[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO],[OPPDATERINGSDATO],[VERIFISERINGSDATO],[INFORMASJON],[PROSESS_HISTORIE])VALUES('";
                                ////string commtextval1 = Mlink + "','" + DGN_ELE_ID_Val + "','" + DataId + "','" + Objtype + "','" + Val_TRE_D_NIVA + "','" + Val_KVALITET + "','" + Val_DATAFANGSTDATO + "','" + Val_REGISTRERINGSVERSJON + "','" + MEDIUM + "','" + HREF + "','" + STATUS_val + "','" + KOPIDATA_avl + "','" + OMRADEID_val + "','" + ORIGINALDATAVERT_val + "','" + KOPIDATO_val + "','" + OPPDATERINGSDATO_val + "','" + VERIFISERINGSDATO_val + "','"+ppINFORMASJON+"','" + PROSESS_HISTORIE_val + "')";

                                //string Quearystr = commtext1 + commtextval1;
                                //cmd.CommandText = Quearystr;
                                //cmd.ExecuteNonQuery();
                            }
                            else if (Datagroup == "PUNKT")
                            {
                                if (Cellname == "")
                                {
                                    DatabaseLink DLink = mApp.CreateDatabaseLink(Mlink, 5, MsdDatabaseLinkage.msdDatabaseLinkageOdbc, true, 1);
                                    Array VlistArray = Vlist as Array;
                                    LineElement L1 = mApp.CreateLineElement2(null, ref Vlist[0], ref Vlist[0]);
                                    //---------------------20150512----------------------------------------------
                                    if ((DOF_NewFeature == Val_DATAFANGSTDATO) && DOF_NewFeature != "")
                                    {
                                        L1.AddDatabaseLink(DLink_ForNew);
                                        L1.LineWeight = 4;
                                    }
                                    else
                                    {
                                        L1.AddDatabaseLink(DLink);
                                        L1.LineWeight = 0;
                                    }
                                    //---------------------------------------------------------------------------
                                    //L1.AddDatabaseLink(DLink);
                                    L1.LineWeight = 5;
                                    L1.Level = FLv;
                                    L1.Color = MSTNC_COL;
                                    mApp.ActiveModelReference.AddElement((_Element)L1);
                                    L1.Redraw();
                                    pointcnt++;
                                    DGN_ELE_ID_Val = L1.ID.Low.ToString();

                                    //------------jan 07-------------------------------------------------------------
                                    Array aDataBlocks;
                                    aDataBlocks = L1.GetUserAttributeData(SOSIFileNameID);
                                    if (aDataBlocks.Length > 0) { L1.DeleteUserAttributeData(SOSIFileNameID, 0); }
                                    MicroStationDGN.DataBlock oDataBlock;
                                    oDataBlock = new MicroStationDGN.DataBlock();

                                    oDataBlock.CopyString(ref SOSIfilename, true);
                                    L1.AddUserAttributeData(SOSIFileNameID, oDataBlock);
                                    L1.Rewrite();

                                    string commtext1 = @"INSERT INTO point([SOSI_FILE_NAME],[MSLINK],[DGN_ELE_ID], [OBJID],[OBJTYPE],";
                                    string commtextval1 = SOSIfilename + "','" + Mlink + "','" + DGN_ELE_ID_Val + "','" + DataId + "','" + Objtype + "','";

                                    //string commtext2 = "[INFORMASJON],[TRE_D_NIVÅ],[KVALITET],[DATAFANGSTDATO],";
                                    //string commtextval2 = ppINFORMASJON + "','" + Val_TRE_D_NIVA + "','" + Val_KVALITET + "','" + Val_DATAFANGSTDATO + "','";
                                    //string commtext3 = "[REGISTRERINGSVERSJON],[MEDIUM],[HREF],[STATUS],";
                                    //string commtextval3 = Val_REGISTRERINGSVERSJON + "','" + MEDIUM + "','" + HREF + "','" + STATUS_val + "','";
                                    //string commtext4 = "[KOPIDATA],[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO],";
                                    //string commtextval4 = KOPIDATA_avl + "','" + OMRADEID_val + "','" + ORIGINALDATAVERT_val + "','" + KOPIDATO_val + "','";
                                    //string commtext5 = "[OPPDATERINGSDATO],[VERIFISERINGSDATO],[PROSESS_HISTORIE],";
                                    //string commtextval5 = OPPDATERINGSDATO_val + "','" + VERIFISERINGSDATO_val + "','" + PROSESS_HISTORIE_val + "','";

                                    string commtext2 = "[TRE_D_NIVÅ],[KVALITET],[DATAFANGSTDATO],[REGISTRERINGSVERSJON],[REGISTRERINGKRETSNR],";
                                    string commtextval2 = Val_TRE_D_NIVA + "','" + Val_KVALITET + "','" + Val_DATAFANGSTDATO + "','" + Val_REGISTRERINGSVERSJON + "','" + REGISTRERINGKRETSNR_val + "','";
                                    string commtext3 = "[MEDIUM],[HREF],[STATUS],[KOPIDATA],";
                                    string commtextval3 = MEDIUM + "','" + HREF + "','" + STATUS_val + "','" + KOPIDATA_avl + "','";
                                    string commtext4 = "[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO],[OPPDATERINGSDATO],";
                                    string commtextval4 = OMRADEID_val + "','" + ORIGINALDATAVERT_val + "','" + KOPIDATO_val + "','" + OPPDATERINGSDATO_val + "','";
                                    string commtext5 = "[VERIFISERINGSDATO],[INFORMASJON],[PROSESS_HISTORIE],[MATRIKKEL-ID],";
                                    string commtextval5 = VERIFISERINGSDATO_val + "','" + ppINFORMASJON + "','" + PROSESS_HISTORIE_val + "','" + MATRIKKEL_ID + "','";


                                    string commtext6 = "[VANNBR],[LHLYSRETN],[LHLYSFARGE],[LHLYS_OPPHØYD_NEDFELT],";
                                    string commtextval6 = VANNBR_val + "','" + LHLYSRETN_val + "','" + LHLYSRETN_val + "','" + LHLYS_OPPHØYD_NEDFELT_val + "','";
                                    string commtext7 = "[LHLYSTYPE],[HØYDE],[KYSTREF],[KYSTKONSTRUKSJONSTYPE],";
                                    string commtextval7 = LHLYSTYPE_val + "','" + HØYDE_val + "','" + KYSTREF_val + "','" + KYSTKONSTRUKSJONSTYPE_val + "','";
                                    string commtext8 = "[TRE_TYP],[BRUTRAFIKKTYPE],[BRUOVERBRU],[SKJERMINGFUNK],";
                                    string commtextval8 = TRE_TYP_val + "','" + BRUTRAFIKKTYPE_val + "','" + BRUOVERBRU_val + "','" + SKJERMINGFUNK_val + "','";
                                    string commtext9 = "[HOB],[IDENTIFIKASJON],[LOKALID],[NAVNEROM],";
                                    string commtextval9 = HOB_val + "','" + IDENTIFIKASJON_val + "','" + LOKALID_val + "','" + NAVNEROM_val + "','";
                                    string commtext10 = "[VERSJONID],[LEDN_EIER],[LEDN_EIERTYPE],[LEDN_EIERNAVN],";
                                    string commtextval10 = VERSJONID_val + "','" + LEDN_EIER_val + "','" + LEDN_EIERTYPE_val + "','" + LEDN_EIERNAVN_val + "','";
                                    string commtext11 = "[LEDN_EIERANDEL],[DRIFTSMERKING],[LEDN_HØYDEREFERANSE],[EL_STASJONSTYPE],";
                                    string commtextval11 = LEDN_EIERANDEL_val + "','" + DRIFTSMERKING_val + "','" + LEDN_HØYDEREFERANSE_val + "','" + EL_STASJONSTYPE_val + "','";
                                    string commtext12 = "[MASTEFUNKSJON],[MASTEKONSTRUKSJON],[VNR],[MAST_LUFTFARTSHINDERMERKING],";
                                    string commtextval12 = MASTEFUNKSJON_val + "','" + MASTEKONSTRUKSJON_val + "','" + VNR_val + "','" + MAST_LUFTFARTSHINDERMERKING_val + "','";
                                    string commtext13 = "[LEDNINGNETTVERKSTYPE],[FELLESFØRING],[LEDNINGSNETTVERKSTYPE],[LEDN_LEIETAKER],";
                                    string commtextval13 = LEDNINGNETTVERKSTYPE_val + "','" + FELLESFØRING_val + "','" + LEDNINGSNETTVERKSTYPE_val + "','" + LEDN_LEIETAKER_val + "','";
                                    string commtext14 = "[LINJEBREDDE],[VEGREKKVERKTYPE],[NEDSENKETKANTSTEIN],[VEGKATEGORI],";
                                    string commtextval14 = LINJEBREDDE_val + "','" + VEGREKKVERKTYPE_val + "','" + NEDSENKETKANTSTEIN_val + "','" + VEGKATEGORI_val + "','";
                                    string commtext15 = "[VEGSTATUS],[VEGNUMMER],[VEGOVERVEG],[JERNBANETYPE],";
                                    string commtextval15 = VEGSTATUS_val + "','" + VEGNUMMER_val + "','" + VEGOVERVEG_val + "','" + JERNBANETYPE_val + "','";
                                    string commtext16 = "[JERNBANEEIER],[LHINST_TYPE],[RESTR_OMR],[RETN],";
                                    string commtextval16 = JERNBANEEIER_val + "','" + LHINST_TYPE_val + "','" + RESTR_OMR_val + "','" + RETN_val + "','";
                                    string commtext17 = "[TWYMERK], [LHAREAL],[ANNENLUFTHAVN],[PLFMERK],";
                                    string commtextval17 = TWYMERK_val + "','" + LHAREAL_val + "','" + ANNENLUFTHAVN_val + "','" + PLFMERK_val + "','";
                                    string commtext18 = "[LHSKILTTYPE],[LHSKILTKATEGORI],[BYGGNR],[BYGGTYP_NBR],";
                                    string commtextval18 = LHSKILTTYPE_val + "','" + LHSKILTKATEGORI_val + "','" + BYGGNR_val + "','" + BYGGTYP_NBR_val + "','";
                                    string commtext19 = "[BYGGSTAT],[SEFRAK_ID],[SEFRAKKOMMUNE],[HUSLØPENR],";
                                    string commtextval19 = BYGGSTAT_val + "','" + SEFRAK_ID_val + "','" + SEFRAKKOMMUNE_val + "','" + HUSLØPENR_val + "','";
                                    string commtext199 = "[VEGSPERRINGTYPE],[VPA],[VKJORFLT],[VFRADATO],[LBVKLASSE],[KOMM_2],";
                                    string commtextval199 = VEGSPERRINGTYPE_val + "','" + VPA_val + "','" + VKJORFLT_val + "','" + VFRADATO_val + "','" + LBVKLASSE_val + "','" + KOMM_2_val + "','";

                                    string commtext20 = "[KOMM],[STED_VERIF],[TAKSKJEGG],";
                                    string commtextval20 = KOMM_val + "','" + STED_VERIF_val + "','" + TAKSKJEGG_val + "','";


                                    //--------------dec 23---------------
                                    string commtext21 = "[SLUSETYP],[ENDRINGSFLAGG],[ENDRET_TYPE],[ENDRET_TID],";
                                    string commtextval21 = SLUSETYP_val + "','" + ENDRINGSFLAGG_val + "','" + ENDRET_TYPE_val + "','" + ENDRET_TID_val + "','";

                                    string commtext22 = "[NETTVERKSTASJONTYPE],[KONSTRUKSJONSHØYDE],[BELYSNINGSBRUK],[NEDSENKET],";
                                    string commtextval22 = NETTVERKSTASJONTYPE_val + "','" + KONSTRUKSJONSHØYDE_val + "','" + BELYSNINGSBRUK_val + "','" + NEDSENKET_val + "','";

                                    string commtext23 = "[KANTSTEIN],[VTILDATO],[METER-TIL],[METER-FRA],";
                                    string commtextval23 = KANTSTEIN_val + "','" + VTILDATO_val + "','" + METER_TIL_val + "','" + METER_FRA_val + "','";

                                    string commtext24 = "[HOVEDPARSELL],[VLENKEID],[GATENR],[GATENAVN],";
                                    string commtextval24 = HOVEDPARSELL_val + "','" + VLENKEID_val + "','" + GATENR_val + "','" + GATENAVN_val + "','";

                                    string commtext25 = "[BRUKSKLASSE],[BRUKSKLASSEHELÅR],[BRUKSKLASSEVINTER],[BRUKSKLASSETELE],";
                                    string commtextval25 = BRUKSKLASSE_val + "','" + BRUKSKLASSEHELÅR_val + "','" + BRUKSKLASSEVINTER_val + "','" + BRUKSKLASSETELE_val + "','";

                                    string commtext26 = "[MAKSVOGNTOGLENGDE],[MAKSTOTALVEKT],[MAKSTOTALVEKTSKILTET],[LINEÆRREFERANSE],";
                                    string commtextval26 = MAKSVOGNTOGLENGDE_val + "','" + MAKSTOTALVEKT_val + "','" + MAKSTOTALVEKTSKILTET_val + "','" + LINEÆRREFERANSE_val + "','";

                                    string commtext27 = "[LINEÆRREFERANSETYPE],[REFERANSEFRA],[REFERANSETIL],[KJØREFELT],";
                                    string commtextval27 = LINEÆRREFERANSETYPE_val + "','" + REFERANSEFRA_val + "','" + REFERANSETIL_val + "','" + KJØREFELT_val + "','";

                                    string commtext28 = "[FARTSGRENSE],[FARTSGRENSEVERDI],[SVINGEFORBUDREFID],[FORBUDRETNING],";
                                    string commtextval28 = FARTSGRENSE_val + "','" + FARTSGRENSEVERDI_val + "','" + SVINGEFORBUDREFID_val + "','" + FORBUDRETNING_val + "','";

                                    string commtext29 = "[SKILTAHØYDE],[LHFDET],[LHELEV],[LHSKILTLYS],";
                                    string commtextval29 = SKILTAHØYDE_val + "','" + LHFDET_val + "','" + LHELEV_val + "','" + LHSKILTLYS_val + "','";

                                    string commtext30 = "[OPAREALAVGRTYPE],[RWYMERK],[TYPEVEG],[KONNEKTERINGSLENKE],";
                                    string commtextval30 = OPAREALAVGRTYPE_val + "','" + RWYMERK_val + "','" + TYPEVEG_val + "','" + KONNEKTERINGSLENKE_val + "','";

                                    string commtext31 = "[NVDB_KLASSELANDBRUKSVEG],[VEGLENKEADRESSE],[ADRESSEKODE],[ADRESSENAVN],";
                                    string commtextval31 = NVDB_KLASSELANDBRUKSVEG_val + "','" + VEGLENKEADRESSE_val + "','" + ADRESSEKODE_val + "','" + ADRESSENAVN_val + "','";

                                    //string commtext32 = "[SIDEVEG],[BARMARKSLØYPE],[BELYSNING],[RUTEMERKING])VALUES('";
                                    //string commtextval32 = SIDEVEG_val + "','" + BARMARKSLØYPE_val + "','" + BELYSNING_val + "','" + RUTEMERKING_val + "')";

                                

                                    //string finalcommand = commtext1 + commtext2 + commtext3 + commtext4 + commtext5 + commtext6 + commtext7 + commtext8 + commtext9 + commtext10 + commtext11 + commtext12 + commtext13 + commtext14 + commtext15 + commtext16 + commtext17 + commtext18 + commtext19 + commtext199 + commtext20 + commtext21 + commtext22 + commtext23 + commtext24 + commtext25 + commtext26 + commtext27 + commtext28 + commtext29 + commtext30 + commtext31 + commtext32;
                                    //string finalcommandval = commtextval1 + commtextval2 + commtextval3 + commtextval4 + commtextval5 + commtextval6 + commtextval7 + commtextval8 + commtextval9 + commtextval10 + commtextval11 + commtextval12 + commtextval13 + commtextval14 + commtextval15 + commtextval16 + commtextval17 + commtextval18 + commtextval19 + commtextval199 + commtextval20 + commtextval21 + commtextval22 + commtextval23 + commtextval24 + commtextval25 + commtextval26 + commtextval27 + commtextval28 + commtextval29 + commtextval30 + commtextval31 + commtextval32;
                                    string commtext32 = "[SIDEVEG],[BARMARKSLØYPE],[BELYSNING],[RUTEMERKING],";
                                    string commtextval32 = SIDEVEG_val + "','" + BARMARKSLØYPE_val + "','" + BELYSNING_val + "','" + RUTEMERKING_val + "','";


                                    //---------jan 07--
                                    string commtext33 = "[OPPHAV],[NETTVERKSSTASJONADKOMSTTYPE],[BELYSNINGSPLASSERING],[KUMFUNKSJON],";
                                    string commtextval33 = OPPHAV_val + "','" + NETTVERKSSTASJONADKOMSTTYPE_val + "','" + BELYSNINGSPLASSERING_val + "','" + KUMFUNKSJON_val + "','";
                                    string commtext34 = "[TRASEBREDDE],[MAX-AVVIK],[PRODUKTSPEK],[KORTNAVN],[OPPDATERT],[HRV],";
                                    string commtextval34 = TRASEBREDDE_val + "','" + MAX_AVVIK_val + "','" + PRODUKTSPEK_val + "','" + KORTNAVN_val + "','" + OPPDATERT_val + "','" + HRV_val + "','";
                                    string commtext35 = "[LRV],[PRODUKTNAVN],[BYGN_ENDR_LØPENR],[BYGN_ENDR_KODE],[SKAL_AVGR_BYGN])VALUES('";
                                    string commtextval35 = LRV_val + "','" + PRODUKTNAVN_val + "','" + BYGN_ENDR_LØPENR_val + "','" + BYGN_ENDR_KODE_val + "','" + SKAL_AVGR_BYGN_val + "')";


                                    string finalcommand = commtext1 + commtext2 + commtext3 + commtext4 + commtext5 + commtext6 + commtext7 + commtext8 + commtext9 + commtext10 + commtext11 + commtext12 + commtext13 + commtext14 + commtext15 + commtext16 + commtext17 + commtext18 + commtext19 + commtext199 + commtext20 + commtext21 + commtext22 + commtext23 + commtext24 + commtext25 + commtext26 + commtext27 + commtext28 + commtext29 + commtext30 + commtext31 + commtext32 + commtext33 + commtext34 + commtext35;
                                    string finalcommandval = commtextval1 + commtextval2 + commtextval3 + commtextval4 + commtextval5 + commtextval6 + commtextval7 + commtextval8 + commtextval9 + commtextval10 + commtextval11 + commtextval12 + commtextval13 + commtextval14 + commtextval15 + commtextval16 + commtextval17 + commtextval18 + commtextval19 + commtextval199 + commtextval20 + commtextval21 + commtextval22 + commtextval23 + commtextval24 + commtextval25 + commtextval26 + commtextval27 + commtextval28 + commtextval29 + commtextval30 + commtextval31 + commtextval32 + commtextval33 + commtextval34 + commtextval35;

                                    string Quearystr = finalcommand + finalcommandval;
                                    OleDbCommand cmd = conn.CreateCommand();
                                    cmd.CommandText = Quearystr;
                                    cmd.ExecuteNonQuery();






                                    //OleDbCommand cmd = conn.CreateCommand();
                                    ////cmd.CommandText = @"INSERT INTO point([MSLINK],[DGN_ELE_ID], [OBJID],[OBJTYPE],[KOMM],[BYGGNR],[BYGGTYP_NBR],[BYGGSTAT],[INFORMASJON])VALUES('" + Mlink + "','" + DGN_ELE_ID_Val + "','" + DataId + "','" + Objtype + "','" + ppKOMM + "','" + ppBYGGNR + "','" + BYGGTYP_NBR + "','" + ppBYGGSTAT + "','" + ppINFORMASJON + "')";
                                    ////cmd.ExecuteNonQuery();

                                    //string commtext1 = @"INSERT INTO point([MSLINK],[DGN_ELE_ID], [OBJID],[OBJTYPE],[KOMM],[BYGGNR],[BYGGTYP_NBR],[BYGGSTAT],[INFORMASJON],[TRE_D_NIVÅ],[KVALITET],[DATAFANGSTDATO],[REGISTRERINGSVERSJON],[MEDIUM],[HREF],[STATUS],[KOPIDATA],[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO],[OPPDATERINGSDATO],[VERIFISERINGSDATO],[PROSESS_HISTORIE])VALUES('";
                                    //string commtextval1 = Mlink + "','" + DGN_ELE_ID_Val + "','" + DataId + "','" + Objtype + "','" + ppKOMM + "','" + ppBYGGNR + "','" + BYGGTYP_NBR + "','" + ppBYGGSTAT + "','" + ppINFORMASJON + "','" + Val_TRE_D_NIVA + "','" + Val_KVALITET + "','" + Val_DATAFANGSTDATO + "','" + Val_REGISTRERINGSVERSJON + "','" + MEDIUM + "','" + HREF + "','" + STATUS_val + "','" + KOPIDATA_avl + "','" + OMRADEID_val + "','" + ORIGINALDATAVERT_val + "','" + KOPIDATO_val + "','" + OPPDATERINGSDATO_val + "','" + VERIFISERINGSDATO_val + "','" + PROSESS_HISTORIE_val + "')";
                                    //string Quearystr = commtext1 + commtextval1;
                                    //cmd.CommandText = Quearystr;
                                    //cmd.ExecuteNonQuery();
                                }
                                else if (Cellname != "")
                                {
                                    double CellScale_Val = 1.0;
                                    if (CellScale != "")
                                    {
                                        bool res = double.TryParse(CellScale, out CellScale_Val);
                                        if (res == false)
                                        {
                                            CellScale_Val = 1.0;
                                        }
                                    }
                                    DatabaseLink DLink = mApp.CreateDatabaseLink(Mlink, 5, MsdDatabaseLinkage.msdDatabaseLinkageOdbc, true, 1);
                                    CellElement CellEle = mApp.CreateCellElement3(Cellname.Trim(), ref Vlist[0], true);
                                    CellEle.ScaleAll(ref Vlist[0], CellScale_Val, CellScale_Val, CellScale_Val);
                                    //--------------------------20150512-------
                                    if ((DOF_NewFeature == Val_DATAFANGSTDATO) && DOF_NewFeature != "")
                                    {
                                        //L1.AddDatabaseLink(DLink_ForNew);
                                        CellEle.AddDatabaseLink(DLink_ForNew);
                                        CellEle.LineWeight = 4;
                                        
                                    }
                                    else
                                    {
                                        CellEle.AddDatabaseLink(DLink);
                                        //CellEle.LineWeight = 4;
                                       
                                    }
                                    //---------------------------------------------------------
                                    //CellEle.AddDatabaseLink(DLink);
                                    CellEle.Level = FLv;
                                    CellEle.Color = MSTNC_COL;
                                    CellEle.Redraw(MsdDrawingMode.msdDrawingModeNormal);
                                    mApp.ActiveModelReference.AddElement((_Element)CellEle);
                                    DGN_ELE_ID_Val = CellEle.ID.Low.ToString();
                                    pointcnt++;


                                    //------------jan 07-------------------------------------------------------------
                                    Array aDataBlocks;
                                    aDataBlocks = CellEle.GetUserAttributeData(SOSIFileNameID);
                                    if (aDataBlocks.Length > 0) { CellEle.DeleteUserAttributeData(SOSIFileNameID, 0); }
                                    MicroStationDGN.DataBlock oDataBlock;
                                    oDataBlock = new MicroStationDGN.DataBlock();

                                    oDataBlock.CopyString(ref SOSIfilename, true);
                                    CellEle.AddUserAttributeData(SOSIFileNameID, oDataBlock);
                                    CellEle.Rewrite();


                                    string commtext1 = @"INSERT INTO point([SOSI_FILE_NAME],[MSLINK],[DGN_ELE_ID], [OBJID],[OBJTYPE],";
                                    string commtextval1 = SOSIfilename + "','" + Mlink + "','" + DGN_ELE_ID_Val + "','" + DataId + "','" + Objtype + "','";
                               
                                    //string commtext2 = "[INFORMASJON],[TRE_D_NIVÅ],[KVALITET],[DATAFANGSTDATO],";
                                    //string commtextval2 = ppINFORMASJON + "','" + Val_TRE_D_NIVA + "','" + Val_KVALITET + "','" + Val_DATAFANGSTDATO + "','";
                                    //string commtext3 = "[REGISTRERINGSVERSJON],[MEDIUM],[HREF],[STATUS],";
                                    //string commtextval3 = Val_REGISTRERINGSVERSJON + "','" + MEDIUM + "','" + HREF + "','" + STATUS_val + "','";
                                    //string commtext4 = "[KOPIDATA],[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO],";
                                    //string commtextval4 = KOPIDATA_avl + "','" + OMRADEID_val + "','" + ORIGINALDATAVERT_val + "','" + KOPIDATO_val + "','";
                                    //string commtext5 = "[OPPDATERINGSDATO],[VERIFISERINGSDATO],[PROSESS_HISTORIE],";
                                    //string commtextval5 = OPPDATERINGSDATO_val + "','" + VERIFISERINGSDATO_val + "','" + PROSESS_HISTORIE_val + "','";

                                    string commtext2 = "[TRE_D_NIVÅ],[KVALITET],[DATAFANGSTDATO],[REGISTRERINGSVERSJON],[REGISTRERINGKRETSNR],";
                                    string commtextval2 = Val_TRE_D_NIVA + "','" + Val_KVALITET + "','" + Val_DATAFANGSTDATO + "','" + Val_REGISTRERINGSVERSJON + "','" + REGISTRERINGKRETSNR_val + "','";
                                    string commtext3 = "[MEDIUM],[HREF],[STATUS],[KOPIDATA],";
                                    string commtextval3 = MEDIUM + "','" + HREF + "','" + STATUS_val + "','" + KOPIDATA_avl + "','";
                                    string commtext4 = "[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO],[OPPDATERINGSDATO],";
                                    string commtextval4 = OMRADEID_val + "','" + ORIGINALDATAVERT_val + "','" + KOPIDATO_val + "','" + OPPDATERINGSDATO_val + "','";
                                    string commtext5 = "[VERIFISERINGSDATO],[INFORMASJON],[PROSESS_HISTORIE],[MATRIKKEL-ID],";
                                    string commtextval5 = VERIFISERINGSDATO_val + "','" + ppINFORMASJON + "','" + PROSESS_HISTORIE_val + "','" + MATRIKKEL_ID + "','";


                                    string commtext6 = "[VANNBR],[LHLYSRETN],[LHLYSFARGE],[LHLYS_OPPHØYD_NEDFELT],";
                                    string commtextval6 = VANNBR_val + "','" + LHLYSRETN_val + "','" + LHLYSRETN_val + "','" + LHLYS_OPPHØYD_NEDFELT_val + "','";
                                    string commtext7 = "[LHLYSTYPE],[HØYDE],[KYSTREF],[KYSTKONSTRUKSJONSTYPE],";
                                    string commtextval7 = LHLYSTYPE_val + "','" + HØYDE_val + "','" + KYSTREF_val + "','" + KYSTKONSTRUKSJONSTYPE_val + "','";
                                    string commtext8 = "[TRE_TYP],[BRUTRAFIKKTYPE],[BRUOVERBRU],[SKJERMINGFUNK],";
                                    string commtextval8 = TRE_TYP_val + "','" + BRUTRAFIKKTYPE_val + "','" + BRUOVERBRU_val + "','" + SKJERMINGFUNK_val + "','";
                                    string commtext9 = "[HOB],[IDENTIFIKASJON],[LOKALID],[NAVNEROM],";
                                    string commtextval9 = HOB_val + "','" + IDENTIFIKASJON_val + "','" + LOKALID_val + "','" + NAVNEROM_val + "','";
                                    string commtext10 = "[VERSJONID],[LEDN_EIER],[LEDN_EIERTYPE],[LEDN_EIERNAVN],";
                                    string commtextval10 = VERSJONID_val + "','" + LEDN_EIER_val + "','" + LEDN_EIERTYPE_val + "','" + LEDN_EIERNAVN_val + "','";
                                    string commtext11 = "[LEDN_EIERANDEL],[DRIFTSMERKING],[LEDN_HØYDEREFERANSE],[EL_STASJONSTYPE],";
                                    string commtextval11 = LEDN_EIERANDEL_val + "','" + DRIFTSMERKING_val + "','" + LEDN_HØYDEREFERANSE_val + "','" + EL_STASJONSTYPE_val + "','";
                                    string commtext12 = "[MASTEFUNKSJON],[MASTEKONSTRUKSJON],[VNR],[MAST_LUFTFARTSHINDERMERKING],";
                                    string commtextval12 = MASTEFUNKSJON_val + "','" + MASTEKONSTRUKSJON_val + "','" + VNR_val + "','" + MAST_LUFTFARTSHINDERMERKING_val + "','";
                                    string commtext13 = "[LEDNINGNETTVERKSTYPE],[FELLESFØRING],[LEDNINGSNETTVERKSTYPE],[LEDN_LEIETAKER],";
                                    string commtextval13 = LEDNINGNETTVERKSTYPE_val + "','" + FELLESFØRING_val + "','" + LEDNINGSNETTVERKSTYPE_val + "','" + LEDN_LEIETAKER_val + "','";
                                    string commtext14 = "[LINJEBREDDE],[VEGREKKVERKTYPE],[NEDSENKETKANTSTEIN],[VEGKATEGORI],";
                                    string commtextval14 = LINJEBREDDE_val + "','" + VEGREKKVERKTYPE_val + "','" + NEDSENKETKANTSTEIN_val + "','" + VEGKATEGORI_val + "','";
                                    string commtext15 = "[VEGSTATUS],[VEGNUMMER],[VEGOVERVEG],[JERNBANETYPE],";
                                    string commtextval15 = VEGSTATUS_val + "','" + VEGNUMMER_val + "','" + VEGOVERVEG_val + "','" + JERNBANETYPE_val + "','";
                                    string commtext16 = "[JERNBANEEIER],[LHINST_TYPE],[RESTR_OMR],[RETN],";
                                    string commtextval16 = JERNBANEEIER_val + "','" + LHINST_TYPE_val + "','" + RESTR_OMR_val + "','" + RETN_val + "','";
                                    string commtext17 = "[TWYMERK], [LHAREAL],[ANNENLUFTHAVN],[PLFMERK],";
                                    string commtextval17 = TWYMERK_val + "','" + LHAREAL_val + "','" + ANNENLUFTHAVN_val + "','" + PLFMERK_val + "','";
                                    string commtext18 = "[LHSKILTTYPE],[LHSKILTKATEGORI],[BYGGNR],[BYGGTYP_NBR],";
                                    string commtextval18 = LHSKILTTYPE_val + "','" + LHSKILTKATEGORI_val + "','" + BYGGNR_val + "','" + BYGGTYP_NBR_val + "','";
                                    string commtext19 = "[BYGGSTAT],[SEFRAK_ID],[SEFRAKKOMMUNE],[HUSLØPENR],";
                                    string commtextval19 = BYGGSTAT_val + "','" + SEFRAK_ID_val + "','" + SEFRAKKOMMUNE_val + "','" + HUSLØPENR_val + "','";
                                    string commtext199 = "[VEGSPERRINGTYPE],[VPA],[VKJORFLT],[VFRADATO],[LBVKLASSE],[KOMM_2],";
                                    string commtextval199 = VEGSPERRINGTYPE_val + "','" + VPA_val + "','" + VKJORFLT_val + "','" + VFRADATO_val + "','" + LBVKLASSE_val + "','" + KOMM_2_val + "','";

                                    string commtext20 = "[KOMM],[STED_VERIF],[TAKSKJEGG],";
                                    string commtextval20 = KOMM_val + "','" + STED_VERIF_val + "','" + TAKSKJEGG_val + "','";


                                    //--------------dec 23---------------
                                    string commtext21 = "[SLUSETYP],[ENDRINGSFLAGG],[ENDRET_TYPE],[ENDRET_TID],";
                                    string commtextval21 = SLUSETYP_val + "','" + ENDRINGSFLAGG_val + "','" + ENDRET_TYPE_val + "','" + ENDRET_TID_val + "','";

                                    string commtext22 = "[NETTVERKSTASJONTYPE],[KONSTRUKSJONSHØYDE],[BELYSNINGSBRUK],[NEDSENKET],";
                                    string commtextval22 = NETTVERKSTASJONTYPE_val + "','" + KONSTRUKSJONSHØYDE_val + "','" + BELYSNINGSBRUK_val + "','" + NEDSENKET_val + "','";

                                    string commtext23 = "[KANTSTEIN],[VTILDATO],[METER-TIL],[METER-FRA],";
                                    string commtextval23 = KANTSTEIN_val + "','" + VTILDATO_val + "','" + METER_TIL_val + "','" + METER_FRA_val + "','";

                                    string commtext24 = "[HOVEDPARSELL],[VLENKEID],[GATENR],[GATENAVN],";
                                    string commtextval24 = HOVEDPARSELL_val + "','" + VLENKEID_val + "','" + GATENR_val + "','" + GATENAVN_val + "','";

                                    string commtext25 = "[BRUKSKLASSE],[BRUKSKLASSEHELÅR],[BRUKSKLASSEVINTER],[BRUKSKLASSETELE],";
                                    string commtextval25 = BRUKSKLASSE_val + "','" + BRUKSKLASSEHELÅR_val + "','" + BRUKSKLASSEVINTER_val + "','" + BRUKSKLASSETELE_val + "','";

                                    string commtext26 = "[MAKSVOGNTOGLENGDE],[MAKSTOTALVEKT],[MAKSTOTALVEKTSKILTET],[LINEÆRREFERANSE],";
                                    string commtextval26 = MAKSVOGNTOGLENGDE_val + "','" + MAKSTOTALVEKT_val + "','" + MAKSTOTALVEKTSKILTET_val + "','" + LINEÆRREFERANSE_val + "','";

                                    string commtext27 = "[LINEÆRREFERANSETYPE],[REFERANSEFRA],[REFERANSETIL],[KJØREFELT],";
                                    string commtextval27 = LINEÆRREFERANSETYPE_val + "','" + REFERANSEFRA_val + "','" + REFERANSETIL_val + "','" + KJØREFELT_val + "','";

                                    string commtext28 = "[FARTSGRENSE],[FARTSGRENSEVERDI],[SVINGEFORBUDREFID],[FORBUDRETNING],";
                                    string commtextval28 = FARTSGRENSE_val + "','" + FARTSGRENSEVERDI_val + "','" + SVINGEFORBUDREFID_val + "','" + FORBUDRETNING_val + "','";

                                    string commtext29 = "[SKILTAHØYDE],[LHFDET],[LHELEV],[LHSKILTLYS],";
                                    string commtextval29 = SKILTAHØYDE_val + "','" + LHFDET_val + "','" + LHELEV_val + "','" + LHSKILTLYS_val + "','";

                                    string commtext30 = "[OPAREALAVGRTYPE],[RWYMERK],[TYPEVEG],[KONNEKTERINGSLENKE],";
                                    string commtextval30 = OPAREALAVGRTYPE_val + "','" + RWYMERK_val + "','" + TYPEVEG_val + "','" + KONNEKTERINGSLENKE_val + "','";

                                    string commtext31 = "[NVDB_KLASSELANDBRUKSVEG],[VEGLENKEADRESSE],[ADRESSEKODE],[ADRESSENAVN],";
                                    string commtextval31 = NVDB_KLASSELANDBRUKSVEG_val + "','" + VEGLENKEADRESSE_val + "','" + ADRESSEKODE_val + "','" + ADRESSENAVN_val + "','";

                                    //string commtext32 = "[SIDEVEG],[BARMARKSLØYPE],[BELYSNING],[RUTEMERKING])VALUES('";
                                    //string commtextval32 = SIDEVEG_val + "','" + BARMARKSLØYPE_val + "','" + BELYSNING_val + "','" + RUTEMERKING_val + "')";
                                 
                                    //string finalcommand = commtext1 + commtext2 + commtext3 + commtext4 + commtext5 + commtext6 + commtext7 + commtext8 + commtext9 + commtext10 + commtext11 + commtext12 + commtext13 + commtext14 + commtext15 + commtext16 + commtext17 + commtext18 + commtext19 + commtext199 + commtext20 + commtext21 + commtext22 + commtext23 + commtext24 + commtext25 + commtext26 + commtext27 + commtext28 + commtext29 + commtext30 + commtext31 + commtext32;
                                    //string finalcommandval = commtextval1 + commtextval2 + commtextval3 + commtextval4 + commtextval5 + commtextval6 + commtextval7 + commtextval8 + commtextval9 + commtextval10 + commtextval11 + commtextval12 + commtextval13 + commtextval14 + commtextval15 + commtextval16 + commtextval17 + commtextval18 + commtextval19 + commtextval199 + commtextval20 + commtextval21 + commtextval22 + commtextval23 + commtextval24 + commtextval25 + commtextval26 + commtextval27 + commtextval28 + commtextval29 + commtextval30 + commtextval31 + commtextval32;
                                    string commtext32 = "[SIDEVEG],[BARMARKSLØYPE],[BELYSNING],[RUTEMERKING],";
                                    string commtextval32 = SIDEVEG_val + "','" + BARMARKSLØYPE_val + "','" + BELYSNING_val + "','" + RUTEMERKING_val + "','";


                                    //---------jan 07--
                                    string commtext33 = "[OPPHAV],[NETTVERKSSTASJONADKOMSTTYPE],[BELYSNINGSPLASSERING],[KUMFUNKSJON],";
                                    string commtextval33 = OPPHAV_val + "','" + NETTVERKSSTASJONADKOMSTTYPE_val + "','" + BELYSNINGSPLASSERING_val + "','" + KUMFUNKSJON_val + "','";
                                    string commtext34 = "[TRASEBREDDE],[MAX-AVVIK],[PRODUKTSPEK],[KORTNAVN],[OPPDATERT],[HRV],";
                                    string commtextval34 = TRASEBREDDE_val + "','" + MAX_AVVIK_val + "','" + PRODUKTSPEK_val + "','" + KORTNAVN_val + "','" + OPPDATERT_val + "','" + HRV_val + "','";
                                    string commtext35 = "[LRV],[PRODUKTNAVN],[BYGN_ENDR_LØPENR],[BYGN_ENDR_KODE],[SKAL_AVGR_BYGN])VALUES('";
                                    string commtextval35 = LRV_val + "','" + PRODUKTNAVN_val + "','" + BYGN_ENDR_LØPENR_val + "','" + BYGN_ENDR_KODE_val + "','" + SKAL_AVGR_BYGN_val + "')";


                                    string finalcommand = commtext1 + commtext2 + commtext3 + commtext4 + commtext5 + commtext6 + commtext7 + commtext8 + commtext9 + commtext10 + commtext11 + commtext12 + commtext13 + commtext14 + commtext15 + commtext16 + commtext17 + commtext18 + commtext19 + commtext199 + commtext20 + commtext21 + commtext22 + commtext23 + commtext24 + commtext25 + commtext26 + commtext27 + commtext28 + commtext29 + commtext30 + commtext31 + commtext32 + commtext33 + commtext34 + commtext35;
                                    string finalcommandval = commtextval1 + commtextval2 + commtextval3 + commtextval4 + commtextval5 + commtextval6 + commtextval7 + commtextval8 + commtextval9 + commtextval10 + commtextval11 + commtextval12 + commtextval13 + commtextval14 + commtextval15 + commtextval16 + commtextval17 + commtextval18 + commtextval19 + commtextval199 + commtextval20 + commtextval21 + commtextval22 + commtextval23 + commtextval24 + commtextval25 + commtextval26 + commtextval27 + commtextval28 + commtextval29 + commtextval30 + commtextval31 + commtextval32 + commtextval33 + commtextval34 + commtextval35;

                                    string Quearystr = finalcommand + finalcommandval;
                                    OleDbCommand cmd = conn.CreateCommand();
                                    cmd.CommandText = Quearystr;
                                    cmd.ExecuteNonQuery();

                                }

                            }
                            else if (Datagroup == "FLATE")
                            {
                                DatabaseLink DLink = mApp.CreateDatabaseLink(Mlink, 7, MsdDatabaseLinkage.msdDatabaseLinkageOdbc, true, 1);
                                Array VlistArray = Vlist as Array;
                                LineElement L1 = mApp.CreateLineElement2(null, ref Vlist[0], ref Vlist[0]);
                                //---------------------20150512----------------------------------------------
                                if ((DOF_NewFeature == Val_DATAFANGSTDATO) && DOF_NewFeature != "")
                                {
                                    L1.AddDatabaseLink(DLink_ForNew);
                                    L1.LineWeight = 8;
                                    L1.Color = 7;
                                }
                                else
                                {
                                    L1.AddDatabaseLink(DLink);
                                    L1.LineWeight = 4;
                                    L1.Color = MSTNC_COL;
                                }
                                //---------------------------------------------------------------------------


                                //L1.AddDatabaseLink(DLink);
                                //L1.LineWeight = 5;
                                //L1.Color = MSTNC_COL;
                                L1.Level = FLv;
                                mApp.ActiveModelReference.AddElement((_Element)L1);
                                L1.Redraw();
                                polycnt++;
                                DGN_ELE_ID_Val = L1.ID.Low.ToString();

                                //------------jan 07-------------------------------------------------------------
                                Array aDataBlocks;
                                aDataBlocks = L1.GetUserAttributeData(SOSIFileNameID);
                                if (aDataBlocks.Length > 0) { L1.DeleteUserAttributeData(SOSIFileNameID, 0); }
                                MicroStationDGN.DataBlock oDataBlock;
                                oDataBlock = new MicroStationDGN.DataBlock();

                                oDataBlock.CopyString(ref SOSIfilename, true);
                                L1.AddUserAttributeData(SOSIFileNameID, oDataBlock);
                                L1.Rewrite();

                                string commtext1 = @"INSERT INTO polygon([SOSI_FILE_NAME],[MSLINK],[DGN_ELE_ID], [OBJID],[OBJTYPE],";
                                string commtextval1 = SOSIfilename + "','" + Mlink + "','" + DGN_ELE_ID_Val + "','" + DataId + "','" + Objtype + "','";


                                //string commtext2 = "[TRE_D_NIVÅ],[KVALITET],[DATAFANGSTDATO],";
                                //string commtextval2 = Val_TRE_D_NIVA + "','" + Val_KVALITET  + "','" + Val_DATAFANGSTDATO + "','";
                                //string commtext3 = "[REGISTRERINGSVERSJON],[REGISTRERINGKRETSNR],[MEDIUM],[HREF],[STATUS],";
                                //string commtextval3 = Val_REGISTRERINGSVERSJON + "','" + REGISTRERINGKRETSNR + "','" + MEDIUM + "','" + HREF + "','" + STATUS_val + "','";

                                //string commtext4 = "[REF],[KOPIDATA],[OMRÅDEID],[ORIGINALDATAVERT],";
                                //string commtextval4 = ppREF + "','" + KOPIDATA_avl + "','" + OMRADEID_val + "','" + ORIGINALDATAVERT_val + "','";

                                //string commtext5 = "[KOPIDATO],[FLATESTAT],[MATRIKKEL-ID],[REF1],[REF2],[REF3],";
                                //string commtextval5 = KOPIDATO_val + "','Existing" + "','" + MATRIKKEL_ID + "','" + ppREF1 + "','" + ppREF2 + "','" + ppREF3 + "','";


                                string commtext2 = "[TRE_D_NIVÅ],[KVALITET],[DATAFANGSTDATO],[REGISTRERINGSVERSJON],[REGISTRERINGKRETSNR],";
                                string commtextval2 = Val_TRE_D_NIVA + "','" + Val_KVALITET + "','" + Val_DATAFANGSTDATO + "','" + Val_REGISTRERINGSVERSJON + "','" + REGISTRERINGKRETSNR_val + "','";
                                string commtext3 = "[MEDIUM],[HREF],[STATUS],[KOPIDATA],";
                                string commtextval3 = MEDIUM + "','" + HREF + "','" + STATUS_val + "','" + KOPIDATA_avl + "','";
                                string commtext4 = "[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO],[OPPDATERINGSDATO],";
                                string commtextval4 = OMRADEID_val + "','" + ORIGINALDATAVERT_val + "','" + KOPIDATO_val + "','" + OPPDATERINGSDATO_val + "','";
                                string commtext5 = "[VERIFISERINGSDATO],[INFORMASJON],[PROSESS_HISTORIE],[MATRIKKEL-ID],[FLATESTAT],[REF],[REF1],[REF2],[REF3],";
                                string commtextval5 = VERIFISERINGSDATO_val + "','" + ppINFORMASJON + "','" + PROSESS_HISTORIE_val + "','" + MATRIKKEL_ID + "','Existing" + "','" + ppREF + "','"+ ppREF1 + "','" + ppREF2 + "','" + ppREF3 + "','";



                                string commtext6 = "[VANNBR],[LHLYSRETN],[LHLYSFARGE],[LHLYS_OPPHØYD_NEDFELT],";
                                string commtextval6 = VANNBR_val + "','" + LHLYSRETN_val + "','" + LHLYSRETN_val + "','" + LHLYS_OPPHØYD_NEDFELT_val + "','";
                                string commtext7 = "[LHLYSTYPE],[HØYDE],[KYSTREF],[KYSTKONSTRUKSJONSTYPE],";
                                string commtextval7 = LHLYSTYPE_val + "','" + HØYDE_val + "','" + KYSTREF_val + "','" + KYSTKONSTRUKSJONSTYPE_val + "','";
                                string commtext8 = "[TRE_TYP],[BRUTRAFIKKTYPE],[BRUOVERBRU],[SKJERMINGFUNK],";
                                string commtextval8 = TRE_TYP_val + "','" + BRUTRAFIKKTYPE_val + "','" + BRUOVERBRU_val + "','" + SKJERMINGFUNK_val + "','";
                                string commtext9 = "[HOB],[IDENTIFIKASJON],[LOKALID],[NAVNEROM],";
                                string commtextval9 = HOB_val + "','" + IDENTIFIKASJON_val + "','" + LOKALID_val + "','" + NAVNEROM_val + "','";
                                string commtext10 = "[VERSJONID],[LEDN_EIER],[LEDN_EIERTYPE],[LEDN_EIERNAVN],";
                                string commtextval10 = VERSJONID_val + "','" + LEDN_EIER_val + "','" + LEDN_EIERTYPE_val + "','" + LEDN_EIERNAVN_val + "','";
                                string commtext11 = "[LEDN_EIERANDEL],[DRIFTSMERKING],[LEDN_HØYDEREFERANSE],[EL_STASJONSTYPE],";
                                string commtextval11 = LEDN_EIERANDEL_val + "','" + DRIFTSMERKING_val + "','" + LEDN_HØYDEREFERANSE_val + "','" + EL_STASJONSTYPE_val + "','";
                                string commtext12 = "[MASTEFUNKSJON],[MASTEKONSTRUKSJON],[VNR],[MAST_LUFTFARTSHINDERMERKING],";
                                string commtextval12 = MASTEFUNKSJON_val + "','" + MASTEKONSTRUKSJON_val + "','" + VNR_val + "','" + MAST_LUFTFARTSHINDERMERKING_val + "','";
                                string commtext13 = "[LEDNINGNETTVERKSTYPE],[FELLESFØRING],[LEDNINGSNETTVERKSTYPE],[LEDN_LEIETAKER],";
                                string commtextval13 = LEDNINGNETTVERKSTYPE_val + "','" + FELLESFØRING_val + "','" + LEDNINGSNETTVERKSTYPE_val + "','" + LEDN_LEIETAKER_val + "','";
                                string commtext14 = "[LINJEBREDDE],[VEGREKKVERKTYPE],[NEDSENKETKANTSTEIN],[VEGKATEGORI],";
                                string commtextval14 = LINJEBREDDE_val + "','" + VEGREKKVERKTYPE_val + "','" + NEDSENKETKANTSTEIN_val + "','" + VEGKATEGORI_val + "','";
                                string commtext15 = "[VEGSTATUS],[VEGNUMMER],[VEGOVERVEG],[JERNBANETYPE],";
                                string commtextval15 = VEGSTATUS_val + "','" + VEGNUMMER_val + "','" + VEGOVERVEG_val + "','" + JERNBANETYPE_val + "','";
                                string commtext16 = "[JERNBANEEIER],[LHINST_TYPE],[RESTR_OMR],[RETN],";
                                string commtextval16 = JERNBANEEIER_val + "','" + LHINST_TYPE_val + "','" + RESTR_OMR_val + "','" + RETN_val + "','";
                                string commtext17 = "[TWYMERK], [LHAREAL],[ANNENLUFTHAVN],[PLFMERK],";
                                string commtextval17 = TWYMERK_val + "','" + LHAREAL_val + "','" + ANNENLUFTHAVN_val + "','" + PLFMERK_val + "','";
                                string commtext18 = "[LHSKILTTYPE],[LHSKILTKATEGORI],[BYGGNR],[BYGGTYP_NBR],";
                                string commtextval18 = LHSKILTTYPE_val + "','" + LHSKILTKATEGORI_val + "','" + BYGGNR_val + "','" + BYGGTYP_NBR_val + "','";
                                string commtext19 = "[BYGGSTAT],[SEFRAK_ID],[SEFRAKKOMMUNE],[HUSLØPENR],";
                                string commtextval19 = BYGGSTAT_val + "','" + SEFRAK_ID_val + "','" + SEFRAKKOMMUNE_val + "','" + HUSLØPENR_val + "','";
                                string commtext199 = "[VEGSPERRINGTYPE],[VPA],[VKJORFLT],[VFRADATO],[LBVKLASSE],[KOMM_2],";
                                string commtextval199 = VEGSPERRINGTYPE_val + "','" + VPA_val + "','" + VKJORFLT_val + "','" + VFRADATO_val + "','" + LBVKLASSE_val + "','" + KOMM_2_val + "','";

                                string commtext20 = "[KOMM],[STED_VERIF],[TAKSKJEGG],";
                                string commtextval20 = KOMM_val + "','" + STED_VERIF_val + "','" + TAKSKJEGG_val + "','";

                                //--------------dec 23---------------
                                string commtext21 = "[SLUSETYP],[ENDRINGSFLAGG],[ENDRET_TYPE],[ENDRET_TID],";
                                string commtextval21 = SLUSETYP_val + "','" + ENDRINGSFLAGG_val + "','" + ENDRET_TYPE_val + "','" + ENDRET_TID_val + "','";

                                string commtext22 = "[NETTVERKSTASJONTYPE],[KONSTRUKSJONSHØYDE],[BELYSNINGSBRUK],[NEDSENKET],";
                                string commtextval22 = NETTVERKSTASJONTYPE_val + "','" + KONSTRUKSJONSHØYDE_val + "','" + BELYSNINGSBRUK_val + "','" + NEDSENKET_val + "','";

                                string commtext23 = "[KANTSTEIN],[VTILDATO],[METER-TIL],[METER-FRA],";
                                string commtextval23 = KANTSTEIN_val + "','" + VTILDATO_val + "','" + METER_TIL_val + "','" + METER_FRA_val + "','";

                                string commtext24 = "[HOVEDPARSELL],[VLENKEID],[GATENR],[GATENAVN],";
                                string commtextval24 = HOVEDPARSELL_val + "','" + VLENKEID_val + "','" + GATENR_val + "','" + GATENAVN_val + "','";

                                string commtext25 = "[BRUKSKLASSE],[BRUKSKLASSEHELÅR],[BRUKSKLASSEVINTER],[BRUKSKLASSETELE],";
                                string commtextval25 = BRUKSKLASSE_val + "','" + BRUKSKLASSEHELÅR_val + "','" + BRUKSKLASSEVINTER_val + "','" + BRUKSKLASSETELE_val + "','";

                                string commtext26 = "[MAKSVOGNTOGLENGDE],[MAKSTOTALVEKT],[MAKSTOTALVEKTSKILTET],[LINEÆRREFERANSE],";
                                string commtextval26 = MAKSVOGNTOGLENGDE_val + "','" + MAKSTOTALVEKT_val + "','" + MAKSTOTALVEKTSKILTET_val + "','" + LINEÆRREFERANSE_val + "','";

                                string commtext27 = "[LINEÆRREFERANSETYPE],[REFERANSEFRA],[REFERANSETIL],[KJØREFELT],";
                                string commtextval27 = LINEÆRREFERANSETYPE_val + "','" + REFERANSEFRA_val + "','" + REFERANSETIL_val + "','" + KJØREFELT_val + "','";

                                string commtext28 = "[FARTSGRENSE],[FARTSGRENSEVERDI],[SVINGEFORBUDREFID],[FORBUDRETNING],";
                                string commtextval28 = FARTSGRENSE_val + "','" + FARTSGRENSEVERDI_val + "','" + SVINGEFORBUDREFID_val + "','" + FORBUDRETNING_val + "','";

                                string commtext29 = "[SKILTAHØYDE],[LHFDET],[LHELEV],[LHSKILTLYS],";
                                string commtextval29 = SKILTAHØYDE_val + "','" + LHFDET_val + "','" + LHELEV_val + "','" + LHSKILTLYS_val + "','";

                                string commtext30 = "[OPAREALAVGRTYPE],[RWYMERK],[TYPEVEG],[KONNEKTERINGSLENKE],";
                                string commtextval30 = OPAREALAVGRTYPE_val + "','" + RWYMERK_val + "','" + TYPEVEG_val + "','" + KONNEKTERINGSLENKE_val + "','";

                                string commtext31 = "[NVDB_KLASSELANDBRUKSVEG],[VEGLENKEADRESSE],[ADRESSEKODE],[ADRESSENAVN],";
                                string commtextval31 = NVDB_KLASSELANDBRUKSVEG_val + "','" + VEGLENKEADRESSE_val + "','" + ADRESSEKODE_val + "','" + ADRESSENAVN_val + "','";

                                string commtext32 = "[SIDEVEG],[BARMARKSLØYPE],[BELYSNING],[RUTEMERKING],";
                                string commtextval32 = SIDEVEG_val + "','" + BARMARKSLØYPE_val + "','" + BELYSNING_val + "','" + RUTEMERKING_val  +"','";
                                ppREF4 = "";ppREF5 = "";ppREF6 = "";ppREF7 = "";ppREF8 = "";ppREF9 = "";ppREF10 = "";ppREF11 = "";ppREF12 = "";ppREF13 = "";ppREF14 = "";ppREF15 = "";ppREF16 = "";ppREF17 = "";ppREF18 = "";ppREF19 = "";ppREF20 = "";ppREF21 = "";
                                ppREF22 = "";ppREF23 = "";ppREF24 = "";ppREF25 = "";ppREF26 = "";ppREF27 = "";ppREF28 = "";ppREF29 = "";ppREF30 = "";ppREF31 = "";ppREF32 = "";ppREF33 = "";ppREF34 = "";ppREF35 = "";

                                ppREF36 = ""; ppREF37 = ""; ppREF38 = ""; ppREF39 = ""; ppREF40 = "";  ppREF41 = ""; ppREF42 = ""; ppREF43 = "";  ppREF44 = ""; ppREF45 = ""; ppREF46 = ""; ppREF47 = "";ppREF48 = "";ppREF49 = ""; ppREF50 = "";
                                
                                string commtext33="[REF4],[REF5],[REF6],[REF7],[REF8],[REF9],[REF10],[REF11],[REF12],[REF13],[REF14],[REF15],";
                                string commtextval33 = ppREF4 + "','" + ppREF5 + "','" + ppREF6 + "','" + ppREF7 + "','" + ppREF8 + "','" + ppREF9 + "','" + ppREF10 + "','" + ppREF11 + "','" + ppREF12 + "','" + ppREF13 + "','" + ppREF14 + "','" + ppREF15 + "','";
                                string commtext34 = "[REF16],[REF17],[REF18],[REF19],[REF20],[REF21],[REF22],[REF23],[REF24],[REF25],[REF26],[REF27],[REF28],[REF29],[REF30] ,[REF31],[REF32],[REF33],[REF34],[REF35], [REF36],[REF37],[REF38],[REF39],[REF40],[REF41],[REF42],[REF43],[REF44],[REF45],[REF46],[REF47],[REF48],[REF49],[REF50],";
                                string commtextval34 = ppREF16 + "','" + ppREF17 + "','" + ppREF18 + "','" + ppREF19 + "','" + ppREF20 + "','" + ppREF21 + "','" + ppREF22 + "','" + ppREF23 + "','" + ppREF24 + "','" + ppREF25 + "','" + ppREF26 + "','" + ppREF27 + "','" + ppREF28 + "','" + ppREF29 + "','" + ppREF30 + "','" + ppREF31 + "','" + ppREF32 + "','" + ppREF33 + "','" + ppREF34 + "','" + ppREF35+"','"+ ppREF36+ "','" +ppREF37+ "','" +ppREF38 + "','" +ppREF39+ "','" +ppREF40+ "','" +ppREF41+ "','" +ppREF42+ "','" +ppREF43+ "','" +ppREF44+ "','" +ppREF45+ "','" +ppREF46+ "','" +ppREF47+ "','" +ppREF48+ "','" +ppREF49+ "','" +ppREF50+ "','";


                                //---------jan 07--
                                string commtext35 = "[OPPHAV],[NETTVERKSSTASJONADKOMSTTYPE],[BELYSNINGSPLASSERING],[KUMFUNKSJON],";
                                string commtextval35 = OPPHAV_val + "','" + NETTVERKSSTASJONADKOMSTTYPE_val + "','" + BELYSNINGSPLASSERING_val + "','" + KUMFUNKSJON_val + "','";
                                string commtext36 = "[TRASEBREDDE],[MAX-AVVIK],[PRODUKTSPEK],[KORTNAVN],[OPPDATERT],[HRV],";
                                string commtextval36 = TRASEBREDDE_val + "','" + MAX_AVVIK_val + "','" + PRODUKTSPEK_val + "','" + KORTNAVN_val + "','" + OPPDATERT_val + "','" + HRV_val + "','";
                                string commtext37 = "[LRV],[PRODUKTNAVN],[BYGN_ENDR_LØPENR],[BYGN_ENDR_KODE],[SKAL_AVGR_BYGN])VALUES('";
                                string commtextval37 = LRV_val + "','" + PRODUKTNAVN_val + "','" + BYGN_ENDR_LØPENR_val + "','" + BYGN_ENDR_KODE_val + "','" + SKAL_AVGR_BYGN_val + "')";


                                string finalcommand = commtext1 + commtext2 + commtext3 + commtext4 + commtext5 + commtext6 + commtext7 + commtext8 + commtext9 + commtext10 + commtext11 + commtext12 + commtext13 + commtext14 + commtext15 + commtext16 + commtext17 + commtext18 + commtext19 + commtext199 + commtext20 + commtext21 + commtext22 + commtext23 + commtext24 + commtext25 + commtext26 + commtext27 + commtext28 + commtext29 + commtext30 + commtext31 + commtext32 + commtext33 + commtext34 + commtext35 + commtext36 + commtext37;
                                string finalcommandval = commtextval1 + commtextval2 + commtextval3 + commtextval4 + commtextval5 + commtextval6 + commtextval7 + commtextval8 + commtextval9 + commtextval10 + commtextval11 + commtextval12 + commtextval13 + commtextval14 + commtextval15 + commtextval16 + commtextval17 + commtextval18 + commtextval19 + commtextval199 + commtextval20 + commtextval21 + commtextval22 + commtextval23 + commtextval24 + commtextval25 + commtextval26 + commtextval27 + commtextval28 + commtextval29 + commtextval30 + commtextval31 + commtextval32 + commtextval33 + commtextval34 + commtextval35 + commtextval36 + commtextval37;
                                string Quearystr = finalcommand + finalcommandval;
                                try
                                {
                                    OleDbCommand cmd = conn.CreateCommand();
                                    cmd.CommandText = Quearystr;
                                    cmd.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    //
                                    try
                                    {
                                        ppREF4 = ""; ppREF5 = ""; ppREF6 = ""; ppREF7 = ""; ppREF8 = ""; ppREF9 = ""; ppREF10 = ""; ppREF11 = ""; ppREF12 = ""; ppREF13 = ""; ppREF14 = ""; ppREF15 = ""; ppREF16 = ""; ppREF17 = ""; ppREF18 = ""; ppREF19 = ""; ppREF20 = ""; ppREF21 = "";
                                        ppREF22 = ""; ppREF23 = ""; ppREF24 = ""; ppREF25 = ""; ppREF26 = ""; ppREF27 = ""; ppREF28 = ""; ppREF29 = ""; ppREF30 = ""; ppREF31 = ""; ppREF32 = ""; ppREF33 = ""; ppREF34 = ""; ppREF35 = "";

                                        ppREF36 = ""; ppREF37 = ""; ppREF38 = ""; ppREF39 = ""; ppREF40 = ""; ppREF41 = ""; ppREF42 = ""; ppREF43 = ""; ppREF44 = ""; ppREF45 = ""; ppREF46 = ""; ppREF47 = ""; ppREF48 = ""; ppREF49 = ""; ppREF50 = "";

                                        commtext33 = "[REF4],[REF5],[REF6],[REF7],[REF8],[REF9],[REF10],[REF11],[REF12],[REF13],[REF14],[REF15],";
                                        commtextval33 = ppREF4 + "','" + ppREF5 + "','" + ppREF6 + "','" + ppREF7 + "','" + ppREF8 + "','" + ppREF9 + "','" + ppREF10 + "','" + ppREF11 + "','" + ppREF12 + "','" + ppREF13 + "','" + ppREF14 + "','" + ppREF15 + "','";
                                        commtext34 = "[REF16],[REF17],[REF18],[REF19],[REF20],[REF21],[REF22],[REF23],[REF24],[REF25],[REF26],[REF27],[REF28],[REF29],[REF30] ,[REF31],[REF32],[REF33],[REF34],[REF35], [REF36],[REF37],[REF38],[REF39],[REF40],[REF41],[REF42],[REF43],[REF44],[REF45],[REF46],[REF47],[REF48],[REF49],[REF50],";
                                        commtextval34 = ppREF16 + "','" + ppREF17 + "','" + ppREF18 + "','" + ppREF19 + "','" + ppREF20 + "','" + ppREF21 + "','" + ppREF22 + "','" + ppREF23 + "','" + ppREF24 + "','" + ppREF25 + "','" + ppREF26 + "','" + ppREF27 + "','" + ppREF28 + "','" + ppREF29 + "','" + ppREF30 + "','" + ppREF31 + "','" + ppREF32 + "','" + ppREF33 + "','" + ppREF34 + "','" + ppREF35 + "','" + ppREF36 + "','" + ppREF37 + "','" + ppREF38 + "','" + ppREF39 + "','" + ppREF40 + "','" + ppREF41 + "','" + ppREF42 + "','" + ppREF43 + "','" + ppREF44 + "','" + ppREF45 + "','" + ppREF46 + "','" + ppREF47 + "','" + ppREF48 + "','" + ppREF49 + "','" + ppREF50 + "','";

                                        finalcommand = commtext1 + commtext2 + commtext3 + commtext4 + commtext5 + commtext6 + commtext7 + commtext8 + commtext9 + commtext10 + commtext11 + commtext12 + commtext13 + commtext14 + commtext15 + commtext16 + commtext17 + commtext18 + commtext19 + commtext199 + commtext20 + commtext21 + commtext22 + commtext23 + commtext24 + commtext25 + commtext26 + commtext27 + commtext28 + commtext29 + commtext30 + commtext31 + commtext32 + commtext33 + commtext34 + commtext35 + commtext36 + commtext37;
                                        finalcommandval = commtextval1 + commtextval2 + commtextval3 + commtextval4 + commtextval5 + commtextval6 + commtextval7 + commtextval8 + commtextval9 + commtextval10 + commtextval11 + commtextval12 + commtextval13 + commtextval14 + commtextval15 + commtextval16 + commtextval17 + commtextval18 + commtextval19 + commtextval199 + commtextval20 + commtextval21 + commtextval22 + commtextval23 + commtextval24 + commtextval25 + commtextval26 + commtextval27 + commtextval28 + commtextval29 + commtextval30 + commtextval31 + commtextval32 + commtextval33 + commtextval34 + commtextval35 + commtextval36 + commtextval37;
                                        Quearystr = finalcommand + finalcommandval;

                                        OleDbCommand cmd = conn.CreateCommand();
                                        cmd.CommandText = Quearystr;
                                        cmd.ExecuteNonQuery();
                                    }
                                    catch (Exception ex1)
                                    {
                                    }

                                }

                             
                                //OleDbCommand cmd = conn.CreateCommand();
                                //cmd.CommandText = @"INSERT INTO polygon([MSLINK],[DGN_ELE_ID], [OBJID],[OBJTYPE],[TRE_D_NIVÅ],[KVALITET],[MÅLEMETODE],[NØYAKTIGHET],[SYNBARHET],[H-MÅLEMETODE],[H-NØYAKTIGHET],[DATAFANGSTDATO],[REGISTRERINGSVERSJON],[SEFRAK_ID],[SEFRAKKOMMUNE],[REGISTRERINGKRETSNR],[HUSLØPENR],[BYGGSTAT],[MEDIUM],[HREF],[KOMM],[BYGGNR],[BYGGTYP_NBR],[INFORMASJON],[REF],[KOPIDATA],[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO],[FLATESTAT],[MATRIKKEL-ID],[REF1],[REF2],[REF3])VALUES('" + Mlink + "','" + DGN_ELE_ID_Val + "','" + DataId + "','" + Objtype + "','" + Val_TRE_D_NIVA + "','" + Val_KVALITET + "','" + MALEMETODE + "','" + NOYAKTIGHET + "','" + SYNBARHET + "','" + HMALEMETODE + "','" + HNOYAKTIGHET + "','" + Val_DATAFANGSTDATO + "','" + Val_REGISTRERINGSVERSJON + "','" + SEFRAK_ID + "','" + SEFRAKKOMMUNE + "','" + REGISTRERINGKRETSNR + "','" + HUSLØPENR + "','" + ppBYGGSTAT + "','" + MEDIUM + "','" + HREF + "','" + ppKOMM + "','" + ppBYGGNR + "','" + BYGGTYP_NBR + "','" + ppINFORMASJON + "','" + ppREF + "','" + KOPIDATA_avl + "','" + OMRADEID_val + "','" + ORIGINALDATAVERT_val + "','" + KOPIDATO_val + "','Existing" + "','" + MATRIKKEL_ID + "','" + ppREF1 + "','" + ppREF2 + "','" + ppREF3 + "')";
                                ////+ DataId + Objtype + Val_TRE_D_NIVA +  Val_KVALITET  + MALEMETODE + NOYAKTIGHET + SYNBARHET + HMALEMETODE + HNOYAKTIGHET + SEFRAK_ID + "','" + SEFRAKKOMMUNE + "','" + REGISTRERINGKRETSNR + "','" + HUSLØPENR + "','" + BYGGSTAT + "','" + MEDIUM + "','" + HREF + "','" + ppKOMM + "','" + ppBYGGNR + "','" + ppBYGGSTAT + "','" + ppINFORMASJON + "','" + ppREF + "')";
                                //cmd.ExecuteNonQuery();
                            }
                            //else
                            //{
                            //    // MessageBox.Show(Datagroup);
                            //}
                            OleDbCommand cmd11 = conn.CreateCommand();
                            cmd11.CommandText = @"INSERT INTO SOSI_ObjectID([SOSI_FILE_NAME],[ObjectID],[DGN_ELE_ID])VALUES('" + SOSIfilename + "'," + Convert.ToInt32(Mlink) + "," + Convert.ToInt32(DGN_ELE_ID_Val) + ")";
                            cmd11.ExecuteNonQuery();
                            //------------------------------------------------------
                            this.Refresh();
                            progressBar1.Refresh();
                            progressBar1.Value = i + 1;
                        }
                        //--------------Header-------------------

                        string[] Headerline = Featurelist[0].Split(';');
                        for (int k = 0; k < Headerline.Length; k++)
                        {
                            label4.Text = "Writing SOSI header..";
                            OleDbCommand cmd = conn.CreateCommand();
                            cmd.CommandText = @"INSERT INTO SOSI_Header([Header],[SOSI_FILE_NAME])VALUES('" + Headerline[k].Trim() + "','" + SOSIfilename + "')";
                            cmd.ExecuteNonQuery();

                        }

                        conn.Close();
                        //-----------------------------------
                    }//if (Featurelist.Count > 1)
                    label4.Text = filestaus +"  Writing from SOSI: DONE";
                  


                    //======================================================For Attribute DATA =================================================================================
                    if (textBox3.Text != "" && filecount == 1)
                    {
                        string[] AttSOSIfilename1 = textBox3.Text.Split('\\');
                        string AttSOSIfilename = AttSOSIfilename1[AttSOSIfilename1.Length - 1];

                        label4.Text = filestaus + " Reading attribute file: IN Progress....";
                        file = new System.IO.StreamReader(textBox3.Text, System.Text.Encoding.UTF7);
                        Mline = "";
                        Featurelist.Clear();
                        FeatureAttributes = "";

                        while ((Mline = file.ReadLine()) != null)
                        {
                            if (Mline.Trim().Length > 0)
                            {
                                string[] splittedline = Mline.Trim().Split();
                                string Firstchar = Mline.Trim().Substring(0, 1);
                                string Secchar = Mline.Trim().Substring(1, 1);
                                if (Firstchar == "." && Secchar != ".")
                                {
                                    if (FeatureAttributes != "") Featurelist.Add(FeatureAttributes);
                                    FeatureAttributes = "";
                                    FeatureAttributes = Mline.Trim();
                                }
                                else
                                {
                                    FeatureAttributes = FeatureAttributes + ";" + Mline.Trim();
                                }
                            }

                        }
                        file.Close();
                        if (Featurelist.Count > 1)
                        {
                            //-----------------------------------------------------
                            double fileunit = 1;
                            string[] tempsplitedfline = Featurelist[0].Split(';');
                            for (int t = 0; t < tempsplitedfline.Length; t++)
                            {
                                if (tempsplitedfline[t].Length > 8)
                                {
                                    if (tempsplitedfline[t].Trim().Substring(3, 5) == "ENHET")
                                    {
                                        string[] linesplited = tempsplitedfline[t].Split(' ');
                                        fileunit = Convert.ToDouble(linesplited[1]);
                                    }
                                }
                            }
                            //---------------------------------------------------------------------------------------
                            OleDbConnection conn = new OleDbConnection("Provider = Microsoft.Jet.OLEDB.4.0; Data Source = " + OpDB);//OpDB
                            conn.Open();
                            var DataTableLayerList = new DataTable();
                            var query = "SELECT * from feature_Layer_Ref";
                            var adapter = new OleDbDataAdapter(query, conn);
                            OleDbCommandBuilder oleDbCommandBuilder = new OleDbCommandBuilder(adapter);
                            adapter.Fill(DataTableLayerList);
                            //---------------------------------------------------------------------------------------
                            var DataTableFeature = new DataTable();
                            var query1 = "SELECT * from feature";
                            var adapter1 = new OleDbDataAdapter(query1, conn);
                            OleDbCommandBuilder oleDbCommandBuilder1 = new OleDbCommandBuilder(adapter1);
                            adapter1.Fill(DataTableFeature);
                            //---------------------------------------------------------------------------------------
                            //OleDbCommand maxCommand = new OleDbCommand("SELECT max(ObjectID) from SOSI_ObjectID", conn);
                            //Int32 max = (Int32)maxCommand.ExecuteScalar();

                            //---------------------------------------------------------------------------------------
                            progressBar1.Minimum = 0;
                            if (Featurelist.Count > 1) { progressBar1.Maximum = Featurelist.Count; }

                            for (int i = 1; i < Featurelist.Count; i++)
                            {
                                label4.Text = filestaus+"  Writing from attribute SOSI: IN Progress....";

                                Point3d[] Vlist = null;
                                bool D3 = false;
                                string[] splitedfline = Featurelist[i].Split(';');
                                string FLayername = splitedfline[1].Split()[1].Trim();

                                for (int j = 2; j < splitedfline.Length; j++)
                                {
                                    string[] Fattributes = splitedfline[j].Split();

                                    string Firstch = splitedfline[j].Trim().Substring(0, 1);
                                    if (Firstch != "." && Firstch != ":")
                                    {
                                        string[] Vliststr = splitedfline[j].Trim().Split(' ');
                                        Point3d pt = mApp.Point3dZero();
                                        pt.X = Convert.ToDouble(Vliststr[1]) * fileunit;
                                        pt.Y = Convert.ToDouble(Vliststr[0]) * fileunit;
                                        //if ((splitedfline[j - 1].Trim().Length == 5) || (splitedfline[j - 1] == "..NØH"))
                                        if ((splitedfline[j - 1].Trim().Length == 5) || (splitedfline[j - 1] == "..NØH")) { pt.Z = Convert.ToDouble(Vliststr[2]) * fileunit; D3 = true; }
                                        if (Vlist == null)
                                        {
                                            Array.Resize(ref Vlist, 1);
                                        }
                                        else
                                        {
                                            Array.Resize(ref Vlist, Vlist.Length + 1);
                                        }
                                        Vlist[Vlist.Length - 1] = pt;
                                    }
                                }
                                //-----------------------Attributes part--------------------------------------------
                                string Datagroup = splitedfline[0].Split()[0].Trim();
                                Datagroup = Datagroup.Substring(1, Datagroup.Length - 1);
                                string DataId = splitedfline[0].Split()[1].Trim();
                                DataId = DataId.Substring(0, DataId.Length - 1);

                                int Mlink = Convert.ToInt32(DataId.Trim());

                                DataId = splitedfline[0].Substring(1);

                                string Objtype = "";
                                string Val_TRE_D_NIVA = "";
                                string Val_KVALITET = "";
                                string Val_DATAFANGSTDATO = "";
                                string Val_REGISTRERINGSVERSJON = "";

                                string STATUS_val = "";
                                string KOPIDATA_avl = "";
                                string OMRADEID_val = "";
                                string ORIGINALDATAVERT_val = "";
                                string KOPIDATO_val = "";
                                string OPPDATERINGSDATO_val = "";
                                string VERIFISERINGSDATO_val = "";
                                string PROSESS_HISTORIE_val = "";



                                string ppKOMM = "";
                                string ppBYGGNR = "";
                                string ppBYGGSTAT = "";
                                string ppINFORMASJON = "";
                                string ppREF = "";

                                string MALEMETODE = "";
                                string NOYAKTIGHET = "";
                                string SYNBARHET = "";
                                string HMALEMETODE = "";
                                string HNOYAKTIGHET = "";
                                string SEFRAK_ID = "";
                                string SEFRAKKOMMUNE = "";
                                string REGISTRERINGKRETSNR = "";
                                string HUSLØPENR = "";
                                string BYGGSTAT = "";
                                string MEDIUM = "";
                                string HREF = "";
                                string BYGGTYP_NBR = "";

                                //---------------------------------------------------------------------------------

                                string VANNBR_val = "";
                                string LHLYSRETN_val = "";
                                string LHLYSFARGE_val = "";
                                string LHLYS_OPPHØYD_NEDFELT_val = "";
                                string LHLYSTYPE_val = "";
                                string HØYDE_val = "";
                                string KYSTREF_val = "";
                                string KYSTKONSTRUKSJONSTYPE_val = "";
                                string TRE_TYP_val = "";
                                string BRUTRAFIKKTYPE_val = "";
                                string BRUOVERBRU_val = "";
                                string SKJERMINGFUNK_val = "";
                                string HOB_val = "";
                                string IDENTIFIKASJON_val = "";
                                string LOKALID_val = "";
                                string NAVNEROM_val = "";
                                string VERSJONID_val = "";
                                string LEDN_EIER_val = "";
                                string LEDN_EIERTYPE_val = "";
                                string LEDN_EIERNAVN_val = "";
                                string LEDN_EIERANDEL_val = "";
                                string DRIFTSMERKING_val = "";
                                string LEDN_HØYDEREFERANSE_val = "";
                                string EL_STASJONSTYPE_val = "";
                                string MASTEFUNKSJON_val = "";
                                string MASTEKONSTRUKSJON_val = "";
                                string VNR_val = "";
                                string MAST_LUFTFARTSHINDERMERKING_val = "";
                                string LEDNINGNETTVERKSTYPE_val = "";
                                string FELLESFØRING_val = "";
                                string LEDNINGSNETTVERKSTYPE_val = "";
                                string LEDN_LEIETAKER_val = "";
                                string LINJEBREDDE_val = "";
                                string VEGREKKVERKTYPE_val = "";
                                string NEDSENKETKANTSTEIN_val = "";
                                string VEGKATEGORI_val = "";
                                string VEGSTATUS_val = "";
                                string VEGNUMMER_val = "";
                                string VEGOVERVEG_val = "";
                                string JERNBANETYPE_val = "";
                                string JERNBANEEIER_val = "";
                                string LHINST_TYPE_val = "";
                                string RESTR_OMR_val = "";
                                string RETN_val = "";
                                string TWYMERK_val = "";
                                string LHAREAL_val = "";
                                string ANNENLUFTHAVN_val = "";
                                string PLFMERK_val = "";
                                string LHSKILTTYPE_val = "";
                                string LHSKILTKATEGORI_val = "";
                                string BYGGNR_val = "";
                                string BYGGTYP_NBR_val = "";
                                string BYGGSTAT_val = "";
                                string SEFRAK_ID_val = "";
                                string SEFRAKKOMMUNE_val = "";
                                string HUSLØPENR_val = "";
                                string KOMM_val = "";
                                string STED_VERIF_val = "";
                                string TAKSKJEGG_val = "";

                                string VEGSPERRINGTYPE_val = "";
                                string VPA_val = "";
                                string VKJORFLT_val = "";
                                string VFRADATO_val = "";
                                string LBVKLASSE_val = "";
                                string KOMM_2_val = "";
                                //----------------------DEC 23 ----------------
                                string SLUSETYP_val = "";
                                string ENDRINGSFLAGG_val = "";
                                string ENDRET_TYPE_val = "";
                                string ENDRET_TID_val = "";
                                string NETTVERKSTASJONTYPE_val = "";
                                string KONSTRUKSJONSHØYDE_val = "";
                                string BELYSNINGSBRUK_val = "";
                                string NEDSENKET_val = "";
                                string KANTSTEIN_val = "";
                                string VTILDATO_val = "";

                                string METER_TIL_val = "";
                                string METER_FRA_val = "";

                                string HOVEDPARSELL_val = "";
                                string VLENKEID_val = "";
                                string GATENR_val = "";
                                string GATENAVN_val = "";
                                string BRUKSKLASSE_val = "";
                                string BRUKSKLASSEHELÅR_val = "";
                                string BRUKSKLASSEVINTER_val = "";
                                string BRUKSKLASSETELE_val = "";
                                string MAKSVOGNTOGLENGDE_val = "";
                                string MAKSTOTALVEKT_val = "";
                                string MAKSTOTALVEKTSKILTET_val = "";
                                string LINEÆRREFERANSE_val = "";
                                string LINEÆRREFERANSETYPE_val = "";
                                string REFERANSEFRA_val = "";
                                string REFERANSETIL_val = "";
                                string KJØREFELT_val = "";
                                string FARTSGRENSE_val = "";
                                string FARTSGRENSEVERDI_val = "";
                                string SVINGEFORBUDREFID_val = "";
                                string FORBUDRETNING_val = "";
                                string SKILTAHØYDE_val = "";
                                string LHFDET_val = "";
                                string LHELEV_val = "";
                                string LHSKILTLYS_val = "";
                                string OPAREALAVGRTYPE_val = "";
                                string RWYMERK_val = "";
                                string TYPEVEG_val = "";
                                string KONNEKTERINGSLENKE_val = "";
                                string NVDB_KLASSELANDBRUKSVEG_val = "";
                                string VEGLENKEADRESSE_val = "";
                                string ADRESSEKODE_val = "";
                                string ADRESSENAVN_val = "";
                                string SIDEVEG_val = "";
                                string BARMARKSLØYPE_val = "";
                                string BELYSNING_val = "";
                                string RUTEMERKING_val = "";




                                int KOMM_ValCnt = 0;
                                //----------------------------------------------------------------------------------


                                for (int j = 1; j < splitedfline.Length; j++)
                                {
                                    //Line
                                    string firstpart = splitedfline[j].Split()[0].Trim();
                                    string aatname = splitedfline[j].Split()[0].Trim();
                                    if (aatname.Substring(0, 1) == ".")
                                    {
                                        if (Datagroup == "KURVE")
                                        {
                                            if (LineAtt.Contains(aatname) == false)
                                            {
                                                LineAtt.Add(aatname);
                                            }
                                        }
                                        if (Datagroup == "PUNKT")
                                        {
                                            if (PointAtt.Contains(aatname) == false)
                                            {
                                                PointAtt.Add(aatname);
                                            }
                                        }
                                        if (Datagroup == "FLATE")
                                        {
                                            if (polyAtt.Contains(aatname) == false)
                                            {
                                                polyAtt.Add(aatname);
                                            }
                                        }
                                    }

                                    string Attval = "";
                                    if (splitedfline[j].Length > firstpart.Length) { Attval = splitedfline[j].Substring(firstpart.Length + 1); Attval = Attval.Trim(); }
                                    //---------------------------------------------------------------------
                                    string AttributeNAME = "";
                                    string attributeName1 = splitedfline[j].Split()[0];
                                    string[] attributeName2 = attributeName1.Split('.');
                                    if (attributeName2.Length > 0) { AttributeNAME = attributeName2[attributeName2.Length - 1]; }
                                    if (AttributeNAME == "VANNBR") { VANNBR_val = Attval; }
                                    if (AttributeNAME == "LHLYSRETN") { LHLYSRETN_val = Attval; }
                                    if (AttributeNAME == "LHLYSFARGE") { LHLYSFARGE_val = Attval; }
                                    if (AttributeNAME == "LHLYS_OPPHØYD_NEDFELT") { LHLYS_OPPHØYD_NEDFELT_val = Attval; }
                                    if (AttributeNAME == "LHLYSTYPE") { LHLYSTYPE_val = Attval; }
                                    if (AttributeNAME == "HØYDE") { HØYDE_val = Attval; }
                                    if (AttributeNAME == "KYSTREF") { KYSTREF_val = Attval; }
                                    if (AttributeNAME == "KYSTKONSTRUKSJONSTYPE") { KYSTKONSTRUKSJONSTYPE_val = Attval; }
                                    if (AttributeNAME == "TRE_TYP") { TRE_TYP_val = Attval; }
                                    if (AttributeNAME == "BRUTRAFIKKTYPE") { BRUTRAFIKKTYPE_val = Attval; }
                                    if (AttributeNAME == "SKJERMINGFUNK") { SKJERMINGFUNK_val = Attval; }
                                    if (AttributeNAME == "HOB") { HOB_val = Attval; }
                                    if (AttributeNAME == "IDENTIFIKASJON") { IDENTIFIKASJON_val = Attval; }
                                    if (AttributeNAME == "LOKALID") { LOKALID_val = Attval; }
                                    if (AttributeNAME == "NAVNEROM") { NAVNEROM_val = Attval; }
                                    if (AttributeNAME == "VERSJONID") { VERSJONID_val = Attval; }
                                    if (AttributeNAME == "LEDN_EIER") { LEDN_EIER_val = Attval; }
                                    if (AttributeNAME == "LEDN_EIERTYPE") { LEDN_EIERTYPE_val = Attval; }
                                    if (AttributeNAME == "LEDN_EIERNAVN") { LEDN_EIERNAVN_val = Attval; }
                                    if (AttributeNAME == "LEDN_EIERANDEL") { LEDN_EIERANDEL_val = Attval; }
                                    if (AttributeNAME == "DRIFTSMERKING") { DRIFTSMERKING_val = Attval; }
                                    if (AttributeNAME == "LEDN_HØYDEREFERANSE") { LEDN_HØYDEREFERANSE_val = Attval; }
                                    if (AttributeNAME == "EL_STASJONSTYPE") { EL_STASJONSTYPE_val = Attval; }
                                    if (AttributeNAME == "MASTEFUNKSJON") { MASTEFUNKSJON_val = Attval; }
                                    if (AttributeNAME == "VNR") { VNR_val = Attval; }
                                    if (AttributeNAME == "MAST_LUFTFARTSHINDERMERKING") { MAST_LUFTFARTSHINDERMERKING_val = Attval; }
                                    if (AttributeNAME == "LEDNINGNETTVERKSTYPE") { LEDNINGNETTVERKSTYPE_val = Attval; }
                                    if (AttributeNAME == "FELLESFØRING") { FELLESFØRING_val = Attval; }
                                    if (AttributeNAME == "LEDNINGSNETTVERKSTYPE") { LEDNINGSNETTVERKSTYPE_val = Attval; }
                                    if (AttributeNAME == "LEDN_LEIETAKER") { LEDN_LEIETAKER_val = Attval; }
                                    if (AttributeNAME == "LINJEBREDDE") { LINJEBREDDE_val = Attval; }
                                    if (AttributeNAME == "VEGREKKVERKTYPE") { VEGREKKVERKTYPE_val = Attval; }
                                    if (AttributeNAME == "NEDSENKETKANTSTEIN") { NEDSENKETKANTSTEIN_val = Attval; }
                                    if (AttributeNAME == "VEGKATEGORI") { VEGKATEGORI_val = Attval; }
                                    if (AttributeNAME == "VEGSTATUS") { VEGSTATUS_val = Attval; }
                                    if (AttributeNAME == "VEGNUMMER") { VEGNUMMER_val = Attval; }
                                    if (AttributeNAME == "VEGOVERVEG") { VEGOVERVEG_val = Attval; }
                                    if (AttributeNAME == "JERNBANETYPE") { JERNBANETYPE_val = Attval; }
                                    if (AttributeNAME == "JERNBANEEIER") { JERNBANEEIER_val = Attval; }
                                    if (AttributeNAME == "LHINST_TYPE") { LHINST_TYPE_val = Attval; }
                                    if (AttributeNAME == "RESTR_OMR") { RESTR_OMR_val = Attval; }
                                    if (AttributeNAME == "RETN") { RETN_val = Attval; }
                                    if (AttributeNAME == "TWYMERK") { TWYMERK_val = Attval; }
                                    if (AttributeNAME == "LHAREAL") { LHAREAL_val = Attval; }
                                    if (AttributeNAME == "ANNENLUFTHAVN") { ANNENLUFTHAVN_val = Attval; }
                                    if (AttributeNAME == "PLFMERK") { PLFMERK_val = Attval; }
                                    if (AttributeNAME == "LHSKILTTYPE") { LHSKILTTYPE_val = Attval; }
                                    if (AttributeNAME == "LHSKILTKATEGORI") { LHSKILTKATEGORI_val = Attval; }
                                    if (AttributeNAME == "BYGGNR") { BYGGNR_val = Attval; }
                                    if (AttributeNAME == "BYGGTYP_NBR") { BYGGTYP_NBR_val = Attval; }
                                    if (AttributeNAME == "BYGGSTAT") { BYGGSTAT_val = Attval; }
                                    if (AttributeNAME == "SEFRAK_ID") { SEFRAK_ID_val = Attval; }
                                    if (AttributeNAME == "SEFRAKKOMMUNE") { SEFRAKKOMMUNE_val = Attval; }
                                    if (AttributeNAME == "HUSLØPENR") { HUSLØPENR_val = Attval; }
                                    if (AttributeNAME == "KOMM")
                                    {
                                        if (KOMM_ValCnt == 0) { KOMM_val = Attval; KOMM_ValCnt++; }
                                        else { KOMM_2_val = Attval; }
                                    }
                                    if (AttributeNAME == "STED_VERIF") { STED_VERIF_val = Attval; }
                                    if (AttributeNAME == "TAKSKJEGG") { TAKSKJEGG_val = Attval; }

                                    if (AttributeNAME == "VEGSPERRINGTYPE") { VEGSPERRINGTYPE_val = Attval; }
                                    if (AttributeNAME == "VPA") { VPA_val = Attval; }
                                    if (AttributeNAME == "VKJORFLT") { VKJORFLT_val = Attval; }
                                    if (AttributeNAME == "VFRADATO") { VFRADATO_val = Attval; }
                                    if (AttributeNAME == "LBVKLASSE") { LBVKLASSE_val = Attval; }
                                    //if (AttributeNAME == "") { _val = Attval; }
                                    //if (AttributeNAME == "") { _val = Attval; }
                                    //if (AttributeNAME == "") { _val = Attval; }
                                    //if (AttributeNAME == "") { _val = Attval; }


                                    //----------------------------------------------------------------------------------
                                    //----------DEc23 dec--------------------------------------
                                    if (AttributeNAME == "SLUSETYP") { SLUSETYP_val = Attval; }
                                    if (AttributeNAME == "ENDRINGSFLAGG") { ENDRINGSFLAGG_val = Attval; }
                                    if (AttributeNAME == "ENDRET_TYPE") { ENDRET_TYPE_val = Attval; }
                                    if (AttributeNAME == "ENDRET_TID") { ENDRET_TID_val = Attval; }
                                    if (AttributeNAME == "NETTVERKSTASJONTYPE") { NETTVERKSTASJONTYPE_val = Attval; }
                                    if (AttributeNAME == "KONSTRUKSJONSHØYDE") { KONSTRUKSJONSHØYDE_val = Attval; }
                                    if (AttributeNAME == "BELYSNINGSBRUK") { BELYSNINGSBRUK_val = Attval; }
                                    if (AttributeNAME == "NEDSENKET") { NEDSENKET_val = Attval; }
                                    if (AttributeNAME == "KANTSTEIN") { KANTSTEIN_val = Attval; }
                                    if (AttributeNAME == "VTILDATO") { VTILDATO_val = Attval; }
                                    if (AttributeNAME == "METER-TIL") { METER_TIL_val = Attval; }
                                    if (AttributeNAME == "METER-FRA") { METER_FRA_val = Attval; }
                                    if (AttributeNAME == "HOVEDPARSELL") { HOVEDPARSELL_val = Attval; }
                                    if (AttributeNAME == "VLENKEID") { VLENKEID_val = Attval; }
                                    if (AttributeNAME == "GATENR") { GATENR_val = Attval; }
                                    if (AttributeNAME == "GATENAVN") { GATENAVN_val = Attval; }
                                    if (AttributeNAME == "BRUKSKLASSE") { BRUKSKLASSE_val = Attval; }
                                    if (AttributeNAME == "BRUKSKLASSEHELÅR") { BRUKSKLASSEHELÅR_val = Attval; }
                                    if (AttributeNAME == "BRUKSKLASSEVINTER") { BRUKSKLASSEVINTER_val = Attval; }
                                    if (AttributeNAME == "BRUKSKLASSETELE") { BRUKSKLASSETELE_val = Attval; }
                                    if (AttributeNAME == "MAKSVOGNTOGLENGDE") { MAKSVOGNTOGLENGDE_val = Attval; }
                                    if (AttributeNAME == "MAKSTOTALVEKT") { MAKSTOTALVEKT_val = Attval; }
                                    if (AttributeNAME == "MAKSTOTALVEKTSKILTET") { MAKSTOTALVEKTSKILTET_val = Attval; }
                                    if (AttributeNAME == "LINEÆRREFERANSE") { LINEÆRREFERANSE_val = Attval; }
                                    if (AttributeNAME == "LINEÆRREFERANSETYPE") { LINEÆRREFERANSETYPE_val = Attval; }
                                    if (AttributeNAME == "REFERANSEFRA") { REFERANSEFRA_val = Attval; }
                                    if (AttributeNAME == "REFERANSETIL") { REFERANSETIL_val = Attval; }
                                    if (AttributeNAME == "KJØREFELT") { KJØREFELT_val = Attval; }
                                    if (AttributeNAME == "FARTSGRENSE") { FARTSGRENSE_val = Attval; }
                                    if (AttributeNAME == "FARTSGRENSEVERDI") { FARTSGRENSEVERDI_val = Attval; }
                                    if (AttributeNAME == "SVINGEFORBUDREFID") { SVINGEFORBUDREFID_val = Attval; }
                                    if (AttributeNAME == "FORBUDRETNING") { FORBUDRETNING_val = Attval; }
                                    if (AttributeNAME == "SKILTAHØYDE") { SKILTAHØYDE_val = Attval; }
                                    if (AttributeNAME == "LHFDET") { LHFDET_val = Attval; }
                                    if (AttributeNAME == "LHELEV") { LHELEV_val = Attval; }
                                    if (AttributeNAME == "LHSKILTLYS") { LHSKILTLYS_val = Attval; }
                                    if (AttributeNAME == "OPAREALAVGRTYPE") { OPAREALAVGRTYPE_val = Attval; }
                                    if (AttributeNAME == "RWYMERK") { RWYMERK_val = Attval; }
                                    if (AttributeNAME == "TYPEVEG") { TYPEVEG_val = Attval; }
                                    if (AttributeNAME == "KONNEKTERINGSLENKE") { KONNEKTERINGSLENKE_val = Attval; }
                                    if (AttributeNAME == "NVDB_KLASSELANDBRUKSVEG") { NVDB_KLASSELANDBRUKSVEG_val = Attval; }
                                    if (AttributeNAME == "VEGLENKEADRESSE") { VEGLENKEADRESSE_val = Attval; }
                                    if (AttributeNAME == "ADRESSEKODE") { ADRESSEKODE_val = Attval; }
                                    if (AttributeNAME == "ADRESSENAVN") { ADRESSENAVN_val = Attval; }
                                    if (AttributeNAME == "SIDEVEG") { SIDEVEG_val = Attval; }
                                    if (AttributeNAME == "BARMARKSLØYPE") { BARMARKSLØYPE_val = Attval; }
                                    if (AttributeNAME == "BELYSNING") { BELYSNING_val = Attval; }
                                    if (AttributeNAME == "RUTEMERKING") { RUTEMERKING_val = Attval; }







                                    if (splitedfline[j].Split()[0] == "..OBJTYPE") { Objtype = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..TRE_D_NIVÅ") { Val_TRE_D_NIVA = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..KVALITET") { Val_KVALITET = Attval; }

                                    else if (splitedfline[j].Split()[0] == "..DATAFANGSTDATO") { Val_DATAFANGSTDATO = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..REGISTRERINGSVERSJON") { Val_REGISTRERINGSVERSJON = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..MEDIUM") { MEDIUM = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..HREF") { HREF = Attval; }

                                    else if (splitedfline[j].Split()[0] == "..STATUS") { STATUS_val = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..KOPIDATA") { KOPIDATA_avl = Attval; }
                                    else if (splitedfline[j].Split()[0] == "...OMRÅDEID") { OMRADEID_val = Attval; }
                                    else if (splitedfline[j].Split()[0] == "...ORIGINALDATAVERT") { ORIGINALDATAVERT_val = Attval; }
                                    else if (splitedfline[j].Split()[0] == "...KOPIDATO") { KOPIDATO_val = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..OPPDATERINGSDATO") { OPPDATERINGSDATO_val = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..VERIFISERINGSDATO") { VERIFISERINGSDATO_val = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..INFORMASJON") { ppINFORMASJON = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..PROSESS_HISTORIE") { PROSESS_HISTORIE_val = Attval; }



                                    else if (splitedfline[j].Split()[0] == "..KOMM") { ppKOMM = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..BYGGNR") { ppBYGGNR = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..BYGGSTAT") { ppBYGGSTAT = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..INFORMASJON") { ppINFORMASJON = Attval; }


                                    //-----------------FLAT--------------------------

                                    else if (splitedfline[j].Split()[0] == "...MÅLEMETODE") { MALEMETODE = Attval; }
                                    else if (splitedfline[j].Split()[0] == "...NØYAKTIGHET") { NOYAKTIGHET = Attval; }
                                    else if (splitedfline[j].Split()[0] == "...SYNBARHET") { SYNBARHET = Attval; }
                                    else if (splitedfline[j].Split()[0] == "...H-MÅLEMETODE") { HMALEMETODE = Attval; }
                                    else if (splitedfline[j].Split()[0] == "...H-NØYAKTIGHET") { HNOYAKTIGHET = Attval; }

                                    else if (splitedfline[j].Split()[0] == "..SEFRAK_ID") { SEFRAK_ID = Attval; }
                                    else if (splitedfline[j].Split()[0] == "...SEFRAKKOMMUNE") { SEFRAKKOMMUNE = Attval; }
                                    else if (splitedfline[j].Split()[0] == "...REGISTRERINGKRETSNR") { REGISTRERINGKRETSNR = Attval; }
                                    else if (splitedfline[j].Split()[0] == "...HUSLØPENR") { HUSLØPENR = Attval; }

                                    else if (splitedfline[j].Split()[0] == "..BYGGTYP_NBR") { BYGGTYP_NBR = Attval; }
                                    else if (splitedfline[j].Split()[0] == "..REF") { ppREF = Attval; }
                                    //---------------------------------------[KOPIDATA],[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO]
                                }
                                //------------------------------Get Layer------------------------------------------------------------
                                string DGNlay = Objtype;
                                DataRow[] oRow1 = DataTableLayerList.Select("fname_no='" + Objtype.Trim() + "'");
                                if (oRow1.Length == 1)
                                {
                                    DGNlay = oRow1[0]["fname"].ToString();
                                    //Colorcoad = oRow1[0]["color"].ToString();
                                }
                                else if (oRow1.Length > 1)
                                {
                                    for (int w = 0; w < oRow1.Length; w++)
                                    {

                                        if (oRow1[w]["TRE_D_NIVÅ"].ToString() == Val_TRE_D_NIVA && oRow1[w]["MEDIUM"].ToString() == MEDIUM && oRow1[w]["HREF"].ToString() == HREF)
                                        {
                                            DGNlay = oRow1[w]["fname"].ToString();
                                        }

                                    }
                                }
                                //------------------------------------Get color-----------------------------------------------------------------
                                string Colorcoad = "1";
                                int MSTNC_COL = 1;
                                DataRow[] oRow22 = DataTableFeature.Select("fname='" + DGNlay + "'");
                                if (oRow22.Length == 1)
                                {
                                    Colorcoad = oRow22[0]["fcolor"].ToString();
                                }

                                if (Colorcoad != "") { MSTNC_COL = Convert.ToInt32(Colorcoad); }

                                //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                DGNlay = DGNlay + "_NEW";
                                if (mApp.ActiveDesignFile.Levels.Find(DGNlay, null) == null)
                                {
                                    mApp.ActiveDesignFile.AddNewLevel(DGNlay);
                                    mApp.ActiveDesignFile.Levels.Rewrite();
                                }
                                Level FLv = mApp.ActiveDesignFile.Levels[DGNlay];
                                string DGN_ELE_ID_Val = "";

                                if (Datagroup == "PUNKT" || Datagroup == "KURVE" || Datagroup == "FLATE") //"KURVE"
                                {
                                    DatabaseLink DLink = mApp.CreateDatabaseLink(Mlink, 4, MsdDatabaseLinkage.msdDatabaseLinkageOdbc, true, 1);
                                    Array VlistArray = Vlist as Array;
                                    LineElement L1 = mApp.CreateLineElement2(null, ref Vlist[0], ref Vlist[0]);
                                    if (Datagroup == "PUNKT" || Datagroup == "FLATE")
                                    {
                                        L1 = mApp.CreateLineElement2(null, ref Vlist[0], ref Vlist[0]);
                                    }
                                    else
                                    {
                                        L1 = mApp.CreateLineElement1(null, ref VlistArray);
                                    }
                                    //LineElement L1 = mApp.CreateLineElement2(null, ref Vlist[0], ref Vlist[0]);
                                    L1.AddDatabaseLink(DLink);
                                    L1.LineWeight = 5;
                                    L1.Level = FLv;
                                    L1.Color = MSTNC_COL;
                                    mApp.ActiveModelReference.AddElement((_Element)L1);
                                    L1.Redraw();
                                    Centroid_pointcnt++;
                                    DGN_ELE_ID_Val = L1.ID.Low.ToString();


                                    //------------jan 07-------------------------------------------------------------
                                    Array aDataBlocks;
                                    aDataBlocks = L1.GetUserAttributeData(SOSIFileNameID);
                                    if (aDataBlocks.Length > 0) { L1.DeleteUserAttributeData(SOSIFileNameID, 0); }
                                    MicroStationDGN.DataBlock oDataBlock;
                                    oDataBlock = new MicroStationDGN.DataBlock();

                                    oDataBlock.CopyString(ref AttSOSIfilename, true);
                                    L1.AddUserAttributeData(SOSIFileNameID, oDataBlock);
                                    L1.Rewrite();


                                    OleDbCommand cmd = conn.CreateCommand();
                                    string commtext1 = @"INSERT INTO Centroid_Attributes([SOSI_FILE_NAME],[MSLINK],[DGN_ELE_ID], [OBJID],[OBJTYPE],";
                                    string commtextval1 = AttSOSIfilename + Mlink + "','" + DGN_ELE_ID_Val + "','" + DataId + "','" + Objtype + "','";
                                    string commtext2 = "[TRE_D_NIVÅ],[KVALITET],[DATAFANGSTDATO],[REGISTRERINGSVERSJON],";
                                    string commtextval2 = Val_TRE_D_NIVA + "','" + Val_KVALITET + "','" + Val_DATAFANGSTDATO + "','" + Val_REGISTRERINGSVERSJON + "','";
                                    string commtext3 = "[MEDIUM],[HREF],[STATUS],[KOPIDATA],";
                                    string commtextval3 = MEDIUM + "','" + HREF + "','" + STATUS_val + "','" + KOPIDATA_avl + "','";
                                    string commtext4 = "[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO],[OPPDATERINGSDATO],";
                                    string commtextval4 = OMRADEID_val + "','" + ORIGINALDATAVERT_val + "','" + KOPIDATO_val + "','" + OPPDATERINGSDATO_val + "','";
                                    string commtext5 = "[VERIFISERINGSDATO],[INFORMASJON],[PROSESS_HISTORIE],";
                                    string commtextval5 = VERIFISERINGSDATO_val + "','" + ppINFORMASJON + "','" + PROSESS_HISTORIE_val + "','";

                                    string commtext6 = "[VANNBR],[LHLYSRETN],[LHLYSFARGE],[LHLYS_OPPHØYD_NEDFELT],";
                                    string commtextval6 = VANNBR_val + "','" + LHLYSRETN_val + "','" + LHLYSRETN_val + "','" + LHLYS_OPPHØYD_NEDFELT_val + "','";
                                    string commtext7 = "[LHLYSTYPE],[HØYDE],[KYSTREF],[KYSTKONSTRUKSJONSTYPE],";
                                    string commtextval7 = LHLYSTYPE_val + "','" + HØYDE_val + "','" + KYSTREF_val + "','" + KYSTKONSTRUKSJONSTYPE_val + "','";
                                    string commtext8 = "[TRE_TYP],[BRUTRAFIKKTYPE],[BRUOVERBRU],[SKJERMINGFUNK],";
                                    string commtextval8 = TRE_TYP_val + "','" + BRUTRAFIKKTYPE_val + "','" + BRUOVERBRU_val + "','" + SKJERMINGFUNK_val + "','";
                                    string commtext9 = "[HOB],[IDENTIFIKASJON],[LOKALID],[NAVNEROM],";
                                    string commtextval9 = HOB_val + "','" + IDENTIFIKASJON_val + "','" + LOKALID_val + "','" + NAVNEROM_val + "','";
                                    string commtext10 = "[VERSJONID],[LEDN_EIER],[LEDN_EIERTYPE],[LEDN_EIERNAVN],";
                                    string commtextval10 = VERSJONID_val + "','" + LEDN_EIER_val + "','" + LEDN_EIERTYPE_val + "','" + LEDN_EIERNAVN_val + "','";
                                    string commtext11 = "[LEDN_EIERANDEL],[DRIFTSMERKING],[LEDN_HØYDEREFERANSE],[EL_STASJONSTYPE],";
                                    string commtextval11 = LEDN_EIERANDEL_val + "','" + DRIFTSMERKING_val + "','" + LEDN_HØYDEREFERANSE_val + "','" + EL_STASJONSTYPE_val + "','";
                                    string commtext12 = "[MASTEFUNKSJON],[MASTEKONSTRUKSJON],[VNR],[MAST_LUFTFARTSHINDERMERKING],";
                                    string commtextval12 = MASTEFUNKSJON_val + "','" + MASTEKONSTRUKSJON_val + "','" + VNR_val + "','" + MAST_LUFTFARTSHINDERMERKING_val + "','";
                                    string commtext13 = "[LEDNINGNETTVERKSTYPE],[FELLESFØRING],[LEDNINGSNETTVERKSTYPE],[LEDN_LEIETAKER],";
                                    string commtextval13 = LEDNINGNETTVERKSTYPE_val + "','" + FELLESFØRING_val + "','" + LEDNINGSNETTVERKSTYPE_val + "','" + LEDN_LEIETAKER_val + "','";
                                    string commtext14 = "[LINJEBREDDE],[VEGREKKVERKTYPE],[NEDSENKETKANTSTEIN],[VEGKATEGORI],";
                                    string commtextval14 = LINJEBREDDE_val + "','" + VEGREKKVERKTYPE_val + "','" + NEDSENKETKANTSTEIN_val + "','" + VEGKATEGORI_val + "','";
                                    string commtext15 = "[VEGSTATUS],[VEGNUMMER],[VEGOVERVEG],[JERNBANETYPE],";
                                    string commtextval15 = VEGSTATUS_val + "','" + VEGNUMMER_val + "','" + VEGOVERVEG_val + "','" + JERNBANETYPE_val + "','";
                                    string commtext16 = "[JERNBANEEIER],[LHINST_TYPE],[RESTR_OMR],[RETN],";
                                    string commtextval16 = JERNBANEEIER_val + "','" + LHINST_TYPE_val + "','" + RESTR_OMR_val + "','" + RETN_val + "','";
                                    string commtext17 = "[TWYMERK], [LHAREAL],[ANNENLUFTHAVN],[PLFMERK],";
                                    string commtextval17 = TWYMERK_val + "','" + LHAREAL_val + "','" + ANNENLUFTHAVN_val + "','" + PLFMERK_val + "','";
                                    string commtext18 = "[LHSKILTTYPE],[LHSKILTKATEGORI],[BYGGNR],[BYGGTYP_NBR],";
                                    string commtextval18 = LHSKILTTYPE_val + "','" + LHSKILTKATEGORI_val + "','" + BYGGNR_val + "','" + BYGGTYP_NBR_val + "','";
                                    string commtext19 = "[BYGGSTAT],[SEFRAK_ID],[SEFRAKKOMMUNE],[HUSLØPENR],";
                                    string commtextval19 = BYGGSTAT_val + "','" + SEFRAK_ID_val + "','" + SEFRAKKOMMUNE_val + "','" + HUSLØPENR_val + "','";

                                    string commtext199 = "[VEGSPERRINGTYPE],[VPA],[VKJORFLT],[VFRADATO],[LBVKLASSE],[KOMM_2],";
                                    string commtextval199 = VEGSPERRINGTYPE_val + "','" + VPA_val + "','" + VKJORFLT_val + "','" + VFRADATO_val + "','" + LBVKLASSE_val + "','" + KOMM_2_val + "','";


                                    string commtext20 = "[KOMM],[STED_VERIF],[TAKSKJEGG])VALUES('";
                                    string commtextval20 = KOMM_val + "','" + STED_VERIF_val + "','" + TAKSKJEGG_val + "')";

                                    //--------------dec 23---------------
                                    string commtext21 = "[SLUSETYP],[ENDRINGSFLAGG],[ENDRET_TYPE],[ENDRET_TID],";
                                    string commtextval21 = SLUSETYP_val + "','" + ENDRINGSFLAGG_val + "','" + ENDRET_TYPE_val + "','" + ENDRET_TID_val + "','";

                                    string commtext22 = "[NETTVERKSTASJONTYPE],[KONSTRUKSJONSHØYDE],[BELYSNINGSBRUK],[NEDSENKET],";
                                    string commtextval22 = NETTVERKSTASJONTYPE_val + "','" + KONSTRUKSJONSHØYDE_val + "','" + BELYSNINGSBRUK_val + "','" + NEDSENKET_val + "','";

                                    string commtext23 = "[KANTSTEIN],[VTILDATO],[METER-TIL],[METER-FRA],";
                                    string commtextval23 = KANTSTEIN_val + "','" + VTILDATO_val + "','" + METER_TIL_val + "','" + METER_FRA_val + "','";

                                    string commtext24 = "[HOVEDPARSELL],[VLENKEID],[GATENR],[GATENAVN],";
                                    string commtextval24 = HOVEDPARSELL_val + "','" + VLENKEID_val + "','" + GATENR_val + "','" + GATENAVN_val + "','";

                                    string commtext25 = "[BRUKSKLASSE],[BRUKSKLASSEHELÅR],[BRUKSKLASSEVINTER],[BRUKSKLASSETELE],";
                                    string commtextval25 = BRUKSKLASSE_val + "','" + BRUKSKLASSEHELÅR_val + "','" + BRUKSKLASSEVINTER_val + "','" + BRUKSKLASSETELE_val + "','";

                                    string commtext26 = "[MAKSVOGNTOGLENGDE],[MAKSTOTALVEKT],[MAKSTOTALVEKTSKILTET],[LINEÆRREFERANSE],";
                                    string commtextval26 = MAKSVOGNTOGLENGDE_val + "','" + MAKSTOTALVEKT_val + "','" + MAKSTOTALVEKTSKILTET_val + "','" + LINEÆRREFERANSE_val + "','";

                                    string commtext27 = "[LINEÆRREFERANSETYPE],[REFERANSEFRA],[REFERANSETIL],[KJØREFELT],";
                                    string commtextval27 = LINEÆRREFERANSETYPE_val + "','" + REFERANSEFRA_val + "','" + REFERANSETIL_val + "','" + KJØREFELT_val + "','";

                                    string commtext28 = "[FARTSGRENSE],[FARTSGRENSEVERDI],[SVINGEFORBUDREFID],[FORBUDRETNING],";
                                    string commtextval28 = FARTSGRENSE_val + "','" + FARTSGRENSEVERDI_val + "','" + SVINGEFORBUDREFID_val + "','" + FORBUDRETNING_val + "','";

                                    string commtext29 = "[SKILTAHØYDE],[LHFDET],[LHELEV],[LHSKILTLYS],";
                                    string commtextval29 = SKILTAHØYDE_val + "','" + LHFDET_val + "','" + LHELEV_val + "','" + LHSKILTLYS_val + "','";

                                    string commtext30 = "[OPAREALAVGRTYPE],[RWYMERK],[TYPEVEG],[KONNEKTERINGSLENKE],";
                                    string commtextval30 = OPAREALAVGRTYPE_val + "','" + RWYMERK_val + "','" + TYPEVEG_val + "','" + KONNEKTERINGSLENKE_val + "','";

                                    string commtext31 = "[NVDB_KLASSELANDBRUKSVEG],[VEGLENKEADRESSE],[ADRESSEKODE],[ADRESSENAVN],";
                                    string commtextval31 = NVDB_KLASSELANDBRUKSVEG_val + "','" + VEGLENKEADRESSE_val + "','" + ADRESSEKODE_val + "','" + ADRESSENAVN_val + "','";

                                    string commtext32 = "[SIDEVEG],[BARMARKSLØYPE],[BELYSNING],[RUTEMERKING])VALUES('";
                                    string commtextval32 = SIDEVEG_val + "','" + BARMARKSLØYPE_val + "','" + BELYSNING_val + "','" + RUTEMERKING_val + "')";
                                    //string sss = commtext21 + commtext22 + commtext23 + commtext24 + commtext25 + commtext26 + commtext27 + commtext28 + commtext29 + commtext30 + commtext31 + commtext32;
                                    //string sss1 = commtextval21 + commtextval22 + commtextval23 + commtextval24 + commtextval25 + commtextval26 + commtextval27 + commtextval28 + commtextval29 + commtextval30 + commtextval31 + commtextval32;
                                    //-------------



                                    string finalcommand = commtext1 + commtext2 + commtext3 + commtext4 + commtext5 + commtext6 + commtext7 + commtext8 + commtext9 + commtext10 + commtext11 + commtext12 + commtext13 + commtext14 + commtext15 + commtext16 + commtext17 + commtext18 + commtext19 + commtext199 + commtext20 + commtext21 + commtext22 + commtext23 + commtext24 + commtext25 + commtext26 + commtext27 + commtext28 + commtext29 + commtext30 + commtext31 + commtext32;
                                    string finalcommandval = commtextval1 + commtextval2 + commtextval3 + commtextval4 + commtextval5 + commtextval6 + commtextval7 + commtextval8 + commtextval9 + commtextval10 + commtextval11 + commtextval12 + commtextval13 + commtextval14 + commtextval15 + commtextval16 + commtextval17 + commtextval18 + commtextval19 + commtextval199 + commtextval20 + commtextval21 + commtextval22 + commtextval23 + commtextval24 + commtextval25 + commtextval26 + commtextval27 + commtextval28 + commtextval29 + commtextval30 + commtextval31 + commtextval32;
                                    string Quearystr = finalcommand + finalcommandval;
                                    cmd.CommandText = Quearystr;
                                    cmd.ExecuteNonQuery();


                                    //OleDbCommand cmd = conn.CreateCommand();
                                    //cmd.CommandText = @"INSERT INTO Centroid_Attributes ([MSLINK],[DGN_ELE_ID], [OBJID],[OBJTYPE],[KOMM],[BYGGNR],[BYGGTYP_NBR],[BYGGSTAT],[INFORMASJON],[KOPIDATA],[OMRÅDEID],[ORIGINALDATAVERT],[KOPIDATO])VALUES('" + Mlink + "','" + DGN_ELE_ID_Val + "','" + DataId + "','" + Objtype + "','" + ppKOMM + "','" + ppBYGGNR + "','" + BYGGTYP_NBR + "','" + ppBYGGSTAT + "','" + ppINFORMASJON +"','"+ KOPIDATA_avl + "','" + OMRADEID_val + "','" + ORIGINALDATAVERT_val + "','" + KOPIDATO_val + "')";
                                    //cmd.ExecuteNonQuery();

                                    OleDbCommand cmd11 = conn.CreateCommand();
                                    cmd11.CommandText = @"INSERT INTO SOSI_ObjectID([SOSI_FILE_NAME],[ObjectID],[DGN_ELE_ID])VALUES('" + SOSIfilename +"',"+ Convert.ToInt32(Mlink) + "," + Convert.ToInt32(DGN_ELE_ID_Val) + ")";
                                    cmd11.ExecuteNonQuery();

                                }

                                
                                //------------------------------------------------------
                                this.Refresh();
                                progressBar1.Refresh();
                                progressBar1.Value = i + 1;
                            }
                            label4.Text = filestaus+"  Writing from attribute SOSI: DONE";
                            //string[] Headerline = Featurelist[0].Split(';');
                            //for (int k = 0; k < Headerline.Length; k++)
                            //{
                            //    OleDbCommand cmd = conn.CreateCommand();
                            //    cmd.CommandText = @"INSERT INTO SOSI_Header([Header])VALUES('" + Headerline[k].Trim() + "')";
                            //    cmd.ExecuteNonQuery();

                            //}

                            conn.Close();
                            //-----------------------------------
                        }//if (Featurelist.Count > 1)
                    }// if (textBox3.Text != "")
                    progressBar2.Refresh();
                    progressBar2.Value = filecount;
                    this.Refresh();

                }//foreach (string Fname in SelectedFilenames)
                //=======================================================================================================================================
                label4.Text =  "Writing to DGN from SOSI: DONE";
                richTextBox1.Text = richTextBox1.Text + "KURVE count=" + linecnt.ToString() + "\n";
                richTextBox1.Text = richTextBox1.Text + "PUNKT count=" + pointcnt.ToString() + "\n";
                richTextBox1.Text = richTextBox1.Text + "FLATE count=" + polycnt.ToString() + "\n";
                richTextBox1.Text = richTextBox1.Text + "Attribute feature count=" + Centroid_pointcnt.ToString() + "\n";
                string msg = "KURVE count=" + linecnt.ToString() + "\nPUNKT count=" + pointcnt.ToString() + "\nFLATE count=" + polycnt.ToString() + "\nAttribute feature count= " + Centroid_pointcnt.ToString();

                
                MessageBox.Show("DONE.......");
                MessageBox.Show(msg); //Centroid_pointcnt
            }
            //catch (Exception Ex)
            //{
            //    MessageBox.Show(Ex.Message);
            //}
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox2.Text == "") { return; }
            string FileN = textBox2.Text;
            string[] Str = FileN.Split('\\');
            string DSN = Str[Str.Length - 1].Substring(0, Str[Str.Length - 1].Length - 4);
            string pStr = MdbPath(FileN, DSN + ".mdb");
            if (pStr == "") { MessageBox.Show("MDB file not found.", "Message"); return; }
            CreateLink(pStr);
            CreatePCF((string)Str[Str.Length - 3]);
            OpenMicrostation(DSN + ".mdb", pStr, FileN);        
        }
        //-----------------------------
        private void CreatePCF(string Pname)
        {
            if (File.Exists(@"C:\Program Files\Bentley\Workspace\Projects\Main.pcf") == false) { MessageBox.Show("Main.pcf File was not Found.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); return; }
            StreamReader oSt = File.OpenText(@"C:\Program Files\Bentley\Workspace\Projects\Main.pcf");
            StreamWriter oWt = File.CreateText(@"C:\Program Files\Bentley\Workspace\Projects\KIL_ODBC.pcf");
            int i = 0;
            while (oSt.Peek() != -1)
            {
                string line = oSt.ReadLine();
                if (i == 34)
                { oWt.WriteLine(line + " " + "KIL_ODBC"); }
                else if (i == 30)
                { oWt.WriteLine(line + " " + Pname); }
                else if (i == 29)
                { oWt.WriteLine(line + " " + ProjectPath); }
                else
                { oWt.WriteLine(line); }
                i = i + 1;
            }
            oWt.Close(); oSt.Close();
        }
        private void OpenMicrostation(string DBName, string DBFile, string DesignFile)
        {
            MicroStationDGN.Application micro = new MicroStationDGN.Application();
            micro.Visible = true;
            try
            {
                micro.OpenDesignFile(DesignFile, false, MsdV7Action.msdV7ActionUpgradeToV8);
                this.BringToFront();
                DialogResult Response = MessageBox.Show("DataBase File: " + DBFile + "\nConnected Successfully. \n\n\n Are you Sure to be Continue ?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (Response == DialogResult.Yes)
                { micro.ShowStatus(DBName + " Connected successfully"); this.Close(); }
                else { micro.Quit(); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                micro.Quit();
            }
        }

        private string MdbPath(string FullPath, string Filename)
        {
            string[] pStr = System.Text.RegularExpressions.Regex.Split(FullPath, @"DGN\\");
            if (File.Exists(pStr[0] + @"DB\" + Filename) == true) { ProjectPath = pStr[0]; return pStr[0] + @"DB\" + Filename; }
            string[] str = FullPath.Split('\\');
            for (int i = str.Length - 2; i >= str.Length - 5; i--)
            {
                string tmp = string.Empty;
                for (int j = 0; j <= i; j++)
                {
                    tmp = tmp + str[j] + "\\";
                }
                if (File.Exists(tmp + Filename) == true) { ProjectPath = tmp; return tmp + Filename; }
            }
            return "";
        }

        private void CreateLink(string FileName)
        {
            ProcessStartInfo sInfo;
            string path = System.Environment.SystemDirectory;
            if (Directory.Exists(path.Substring(0, path.Length - 8) + "SysWOW64") == true)
                path = path.Substring(0, path.Length - 8) + "SysWOW64";



            sInfo = new ProcessStartInfo("CMD",
                                        "/C " + path + @"\ODBCCONF.exe CONFIGSYSDSN ""Microsoft Access Driver (*.mdb)""" +
                                        @" ""DSN=KIL_ODBC | Description=KIL_ODBC | DBQ= " + FileName + @"""");
            sInfo.CreateNoWindow = true;
            sInfo.RedirectStandardOutput = false;
            sInfo.UseShellExecute = false;
            Process p = new Process();
            p.StartInfo = sInfo;
            p.Start();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            OleDbConnection conn = new OleDbConnection("Provider = Microsoft.Jet.OLEDB.4.0; Data Source = C:\\AKHILESH_PROJECT_DATA\\SOSI_\\testing\\Norway_temp.mdb");
            conn.Open();
            OleDbCommand cmd = conn.CreateCommand();
           // cmd.CommandText = @"INSERT INTO point([MSLINK], [OBJTYPE],  [KOMM], [BYGGNR], [BYGGTYP_NBR],[BYGGSTAT],[INFORMASJON])VALUES('" + val1 + "', '" + val2 + "','" + val3 + "','" + val4 + "', '" + val5 + "')";
            string val5 = "Akhilesh ";
            cmd.CommandText = @"INSERT INTO point( [OBJTYPE])VALUES('"  + val5 + "')";
            cmd.ExecuteNonQuery();
            conn.Close();
           
        }

        //private void toolStripButton1_Click(object sender, EventArgs e)
        //{
        //    MicroStationDGN.Application mApp = (MicroStationDGN.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("MicroStationDGN.Application");
        //    string filepath = mApp.ActiveDesignFile.Path;
        //    string Filename = mApp.ActiveDesignFile.FullName;
        //    string DBpath=mApp.ActiveDesignFile.FullName.Replace(".dgn",".mdb");
        //    if (File.Exists(DBpath) == false) { MessageBox.Show("No DB Found..."); return; }

        //    ElementEnumerator oScanEnumerator = null;
        //    //ElementScanCriteria Sc = new ElementScanCriteria();
        //    _Element Ele = null;
        //    oScanEnumerator = mApp.ActiveModelReference.GetSelectedElements();
        //   // oScanEnumerator = mApp.ActiveModelReference.Scan(Sc);
        //    richTextBox1.Text = "";
        //    while (oScanEnumerator.MoveNext())
        //    {
        //        Ele = oScanEnumerator.Current;
        //        if (Ele.IsLineElement)
        //        {
        //            MicroStationDGN.DataBlock oDataBlock1;
        //            Array aDataBlocks1 = Ele.GetDatabaseLinks(MicroStationDGN.MsdDatabaseLinkage.msdDatabaseLinkageOdbc);
        //            if (aDataBlocks1.Length  > 0)
        //            {
        //                DatabaseLink DLink = (MicroStationDGN.DatabaseLink)aDataBlocks1.GetValue(0);
        //                int Mlink=DLink.Mslink;
        //                int Enumber = DLink.EntityNumber;
        //                OleDbConnection conn = new OleDbConnection("Provider = Microsoft.Jet.OLEDB.4.0; Data Source = " + DBpath);//C:\\AKHILESH_PROJECT_DATA\\SOSI_\\testing\\Norway_temp.mdb
        //                conn.Open();

        //                var query = "Select * From polygon where mslink=" +"'"+ Mlink+"'";
        //                if(Enumber==1)
        //                {
        //                    query = "Select * From point where mslink=" + "'" + Mlink + "'";
        //                }
        //                else if(Enumber==2)
        //                {
        //                    query = "Select * From line where mslink=" + "'" + Mlink + "'";
        //                }
        //                else if (Enumber == 3)
        //                {
        //                    query = "Select * From polygon where mslink=" + "'" + Mlink + "'";
        //                }

        //                var command = new OleDbCommand(query, conn);
        //                var reader = command.ExecuteReader();
        //                richTextBox1.Text = richTextBox1.Text+"\n----------------------------\n";
        //                while (reader.Read())
        //                {
        //                    for (int i = 0; i < reader.FieldCount; i++)
        //                    {
        //                        if (reader.GetValue(i).ToString().Trim().Length > 0)
        //                        richTextBox1.Text = richTextBox1.Text + reader.GetName(i).ToString() + "\t" + reader.GetValue(i).ToString() + "\n";
        //                    }
        //                }
                            
        //                conn.Close();
        //            }
                     
        //        }
        //    }
        //}

        private void toolStripButton2_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
 
            string[] sosifilename1 = textBox1.Text.Split('\\');
            string sosifilename = sosifilename1[sosifilename1.Length - 1];
            sosifilename = sosifilename.Substring(0, sosifilename.Length - 4);
            string app_path = System.Windows.Forms.Application.StartupPath;
           
            string GDNseed = System.Windows.Forms.Application.StartupPath + "\\Norway_SEED.dgn";
            if (File.Exists(GDNseed) == false) { MessageBox.Show("Norway_SEED.dgn not found"); return; }
            string OpDGN = textBox2.Text + "\\"+sosifilename+".dgn";
            File.Copy(GDNseed, OpDGN, true);
            string DBfile = System.Windows.Forms.Application.StartupPath + "\\Norway_SOSI_DB.mdb";
            if (File.Exists(DBfile) == false) { MessageBox.Show("Norway_SOSI_DB.mdb not found"); return; }
            string OpDB = textBox2.Text + "\\" + sosifilename + ".mdb";
            File.Copy(DBfile, OpDB, true);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                MicroStationDGN.Application mApp = (MicroStationDGN.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("MicroStationDGN.Application");
                string filepath = mApp.ActiveDesignFile.Path;
                string Filename = mApp.ActiveDesignFile.FullName;
                string DBpath = mApp.ActiveDesignFile.FullName.Replace(".dgn", ".mdb");
                if (File.Exists(DBpath) == false) { MessageBox.Show("No DB Found..."); return; }

                ElementEnumerator oScanEnumerator = null;
                //ElementScanCriteria Sc = new ElementScanCriteria();
                _Element Ele = null;
                oScanEnumerator = mApp.ActiveModelReference.GetSelectedElements();
                // oScanEnumerator = mApp.ActiveModelReference.Scan(Sc);
                richTextBox1.Text = "";
                while (oScanEnumerator.MoveNext())
                {
                    Ele = oScanEnumerator.Current;
                    if (Ele.IsLineElement || Ele.IsCellElement || Ele.IsShapeElement )
                    {

                        string Layer_Name = "";
                        if (Ele.IsCellElement)
                        {
                            ElementEnumerator tempE = Ele.AsCellElement.GetSubElements();
                            while (tempE.MoveNext())
                            {
                                _Element tempele = tempE.Current;
                                if (tempele.Type != MsdElementType.msdElementTypeCellHeader)
                                {
                                    Layer_Name = tempele.Level.Name.Trim();
                                }

                            }
                        }
                        else
                        {
                            Layer_Name = Ele.Level.Name;
                        }

                        string EleLevelName = Layer_Name;

                        string[] LayCode1 = Layer_Name.Trim().Split('_');
                        if (LayCode1 != null)
                        {
                            if (LayCode1[0].Length >= 5)
                            {
                                ////-----Mediun for DGN Layer start from 20***,30***,40**** -------------
                                if (LayCode1[0].Trim().Substring(0, 2) == "10") { EleLevelName = Layer_Name.Trim().Substring(2); }
                                if (LayCode1[0].Trim().Substring(0, 2) == "20") { EleLevelName = Layer_Name.Trim().Substring(2); }
                                if (LayCode1[0].Trim().Substring(0, 2) == "30") { EleLevelName = Layer_Name.Trim().Substring(2); }
                                if (LayCode1[0].Trim().Substring(0, 2) == "40") { EleLevelName = Layer_Name.Trim().Substring(2); }
                            }
                        }
                        Layer_Name = EleLevelName;



                        string objtypename = Layer_Name;
                        OleDbConnection conn1 = new OleDbConnection("Provider = Microsoft.Jet.OLEDB.4.0; Data Source = " + DBpath);//OpDB
                        conn1.Open();
                        var DataTableLayerList = new DataTable();
                        var query1 = "SELECT * from feature_Layer_Ref";
                        var adapter = new OleDbDataAdapter(query1, conn1);
                        OleDbCommandBuilder oleDbCommandBuilder = new OleDbCommandBuilder(adapter);
                        adapter.Fill(DataTableLayerList);
                        DataRow[] oRow1 = DataTableLayerList.Select("fname='" + Layer_Name.Trim() + "'");
                         if (oRow1.Length >= 1)
                         {
                             objtypename = oRow1[0]["fname_no"].ToString();
                         }

                         conn1.Close();
                        
                        
                        MicroStationDGN.DataBlock oDataBlock1;
                        Array aDataBlocks1 = Ele.GetDatabaseLinks(MicroStationDGN.MsdDatabaseLinkage.msdDatabaseLinkageOdbc);
                        if (aDataBlocks1.Length > 0)
                        {

                            DatabaseLink DLink = (MicroStationDGN.DatabaseLink)aDataBlocks1.GetValue(0);
                            int Mlink = DLink.Mslink;
                            int Enumber = DLink.EntityNumber;
                            OleDbConnection conn = new OleDbConnection("Provider = Microsoft.Jet.OLEDB.4.0; Data Source = " + DBpath);//C:\\AKHILESH_PROJECT_DATA\\SOSI_\\testing\\Norway_temp.mdb
                            conn.Open();

                            var query = "Select * From polygon where mslink=" + "'" + Mlink + "' and OBJTYPE= '" + objtypename + "'";
                            if (Enumber == 5)
                            {
                                query = "Select * From point where mslink=" + "'" + Mlink + "' and OBJTYPE= '" + objtypename + "'";
                            }
                            else if (Enumber == 6)
                            {
                                query = "Select * From line where mslink=" + "'" + Mlink + "' and OBJTYPE= '" + objtypename+"'";
                            }
                            else if (Enumber == 7)
                            {
                                string[] lname = Layer_Name.Trim().Split('_');
                                if (lname.Length > 1)
                                {
                                    Layer_Name = "";
                                    for (int q = 0; q < lname.Length-1; q++)
                                    {
                                        if (q == 0) { Layer_Name = lname[q]; }
                                        else { Layer_Name = Layer_Name + "_" + lname[q]; } 
                                    }
                                }
                                DataRow[] oRow2 = DataTableLayerList.Select("fname='" + Layer_Name.Trim() + "'");
                                if (oRow2.Length == 1)
                                {
                                    objtypename = oRow2[0]["fname_no"].ToString();
                                }
                                
                                
                                
                                query = "Select * From polygon where mslink=" + "'" + Mlink + "' and OBJTYPE= '" + objtypename + "'";
                            }
                            else if (Enumber == 4)
                            {
                                string[] lname = Layer_Name.Trim().Split('_');
                                if (lname.Length > 1)
                                {
                                    Layer_Name = "";
                                    for (int q = 0; q < lname.Length - 1; q++)
                                    {
                                        if (q == 0) { Layer_Name = lname[q]; }
                                        else { Layer_Name = Layer_Name + "_" + lname[q]; }
                                    }
                                }
                                DataRow[] oRow2 = DataTableLayerList.Select("fname='" + Layer_Name.Trim() + "'");
                                if (oRow2.Length == 1)
                                {
                                    objtypename = oRow2[0]["fname_no"].ToString();
                                }
                                query = "Select * From Centroid_Attributes where mslink=" + "'" + Mlink + "' and OBJTYPE= '" + objtypename + "'";
                            }
                            var command = new OleDbCommand(query, conn);
                            var reader = command.ExecuteReader();
                            //----------------------------------------------------------------------------------------------------------
                            if (Enumber == 1)
                            {
                                string[] lname = Layer_Name.Trim().Split('_');
                                if (lname.Length > 1)
                                {
                                    Layer_Name = "";
                                    for (int q = 0; q < lname.Length - 1; q++)
                                    {
                                        if (q == 0) { Layer_Name = lname[q]; }
                                        else { Layer_Name = Layer_Name + "_" + lname[q]; }
                                    }
                                }
                                objtypename = Layer_Name;
                                DataRow[] oRow2 = DataTableLayerList.Select("fname='" + Layer_Name.Trim() + "'");
                                if (oRow2.Length == 1)
                                {
                                    objtypename = oRow2[0]["fname_no"].ToString();
                                }
                                query = "Select * From line where mslink=" + "'" + Mlink + "' and OBJTYPE= '" + objtypename + "'";
                                command = new OleDbCommand(query, conn);
                                reader = command.ExecuteReader();
                                if (reader.HasRows == false)
                                {
                                    query = "Select * From point where mslink=" + "'" + Mlink + "' and OBJTYPE= '" + objtypename + "'";
                                    command = new OleDbCommand(query, conn);
                                     reader = command.ExecuteReader();
                                     if (reader.HasRows == false)
                                     {
                                         query = "Select * From polygon where mslink=" + "'" + Mlink + "' and OBJTYPE= '" + objtypename + "'";
                                         command = new OleDbCommand(query, conn);
                                         reader = command.ExecuteReader();
                                         
                                     }
                                }
                            }
                            //--------------------------------------------------------------------------------------------------------------
                            while (reader.Read())
                            {
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    if (reader.GetValue(i).ToString().Trim().Length > 0)
                                        richTextBox1.Text = richTextBox1.Text + reader.GetName(i).ToString() + "\t" + reader.GetValue(i).ToString() + "\n";
                                }
                            }
                            richTextBox1.Text = richTextBox1.Text + "-----------------------------------------------------------------\n";
                            conn.Close();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        //--------------------------
        public _Element[] GetEdgeElement(Fence oFen)
        {
            ElementEnumerator oScanEnumerator = null;
            _Element Ele = null;
            _Element[] oEle = null;
            int i = 0;
            oScanEnumerator = oFen.GetContents(false, true);
            while (oScanEnumerator.MoveNext())
            {
                Ele = oScanEnumerator.Current;
                if (Ele.IsGraphical == true && (Ele.IsLineElement == true || Ele.IsShapeElement == true))
                {
                    Array.Resize(ref oEle, i + 1);
                    oEle[i] = Ele;
                    i = i + 1;
                }
            }
            return oEle;
        }

        private void button8_Click(object sender, EventArgs e)
        {

            openFileDialog1.Filter = "SOSI files (*.sos)|*.sos|All files (*.*)|*.*";
            DialogResult di = openFileDialog1.ShowDialog();
            if (di == DialogResult.OK)
            {
                textBox3.Text = openFileDialog1.FileName;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox4.Text != "")
                {
                    MicroStationDGN.Application mApp = (MicroStationDGN.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("MicroStationDGN.Application");
                    long eleid = Convert.ToInt64(textBox4.Text);

                    _Element Ele = mApp.ActiveModelReference.GetElementByID64(eleid);

                    Point3d Pnt = mApp.Point3dZero();

                    if (Ele.IsLineElement == true)
                    {
                        Pnt = Ele.AsLineElement.PointAtDistance(Ele.AsLineElement.Length / 2);
                    }
                    else if (Ele.IsShapeElement == true)
                    {
                        Pnt = Ele.AsShapeElement.Centroid();
                    }
                    else if (Ele.IsTextElement == true)
                    {
                        Pnt = Ele.AsTextElement.get_Origin();
                    }
                    else if (Ele.IsCellElement == true)
                    {
                        Pnt = Ele.AsCellElement.Origin;
                    }
                    else if (Ele.IsComplexStringElement == true)
                    {
                        Pnt = Ele.AsComplexStringElement.PointAtDistance(Ele.AsComplexStringElement.Length / 2);
                    }
                    else if (Ele.IsComplexShapeElement == true)
                    {
                        Pnt = Ele.AsComplexShapeElement.Centroid();
                    }
                    else if (Ele.IsEllipseElement == true)
                    {
                        Pnt = Ele.AsEllipseElement.Centroid();
                    }
                    MicroStationDGN.View V1 = mApp.CommandState.LastView();
                    V1.ZoomAboutPoint(ref Pnt, 1);
                    V1.Redraw();
                    Ele.Redraw(MsdDrawingMode.msdDrawingModeHilite);

                    mApp.ActiveModelReference.SelectElement(Ele, true);
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {

            try
            {
                MicroStationDGN.Application mApp = (MicroStationDGN.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("MicroStationDGN.Application");
                ElementEnumerator oScanEnumerator = null;
                //ElementScanCriteria Sc = new ElementScanCriteria();
                _Element Ele = null;
                oScanEnumerator = mApp.ActiveModelReference.GetSelectedElements();
                // oScanEnumerator = mApp.ActiveModelReference.Scan(Sc);
                richTextBox1.Text = "";
                int cUserAttrId = 1911;
                int cUserAttrId_Poly_REF = 1922;
                int NewSOSI_ID_REF = 1933;
                int SOSIFileNameID = 1969;
                while (oScanEnumerator.MoveNext())
                {
                    Ele = oScanEnumerator.Current;
                    if (Ele.IsLineElement || Ele.IsCellElement || Ele.IsShapeElement)
                    {
                        string RefVals = "";
                        string Sfilename = "";

                        MicroStationDGN.DataBlock oDataBlock3;
                        Array aDataBlocks3 = Ele.GetDatabaseLinks(MicroStationDGN.MsdDatabaseLinkage.msdDatabaseLinkageOdbc);
                        if (aDataBlocks3.Length > 0)
                        {
                            DatabaseLink DLink = (MicroStationDGN.DatabaseLink)aDataBlocks3.GetValue(0);
                            int Mlink = DLink.Mslink;
                            int Enumber = DLink.EntityNumber;
                            richTextBox1.Text = richTextBox1.Text + "\nMSLINK: " + Mlink.ToString() + "\nEnumber=" + Enumber.ToString();
                        }

                        MicroStationDGN.DataBlock oDataBlock1;
                        Array aDataBlocks1;
                        aDataBlocks1 = Ele.GetUserAttributeData(cUserAttrId_Poly_REF); //for surface ref
                        if (aDataBlocks1.Length > 0)
                        {
                            oDataBlock1 = (MicroStationDGN.DataBlock)aDataBlocks1.GetValue(0);
                            oDataBlock1.CopyString(ref RefVals, false);
                            richTextBox1.Text = richTextBox1.Text + "\nSurface Referance: " + RefVals;
                        }


                        MicroStationDGN.DataBlock oDataBlock2;
                        Array aDataBlocks2;
                        aDataBlocks2 = Ele.GetUserAttributeData(SOSIFileNameID);
                        if (aDataBlocks2.Length > 0)
                        {
                            oDataBlock2 = (MicroStationDGN.DataBlock)aDataBlocks2.GetValue(0);
                            oDataBlock2.CopyString(ref Sfilename, false);
                            richTextBox1.Text = richTextBox1.Text + "\nSosi file name: " + Sfilename;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {

                MicroStationDGN.Application mApp = (MicroStationDGN.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("MicroStationDGN.Application");
                richTextBox1.Text = "";
                foreach (Level CurLv in mApp.ActiveDesignFile.Levels)
                {
                    ElementEnumerator oScanEnumerator = null;
                    ElementScanCriteria Sc = new ElementScanCriteria();
                    Sc.ExcludeNonGraphical();

                    Sc.ExcludeAllLevels();
                    Sc.IncludeLevel(CurLv);
                    //oScanEnumerator = mApp.ActiveModelReference.GetSelectedElements();
                    oScanEnumerator = mApp.ActiveModelReference.Scan(Sc);
                    _Element[] EleArray = oScanEnumerator.BuildArrayFromContents() as _Element[];
                    richTextBox1.Text = richTextBox1.Text + "\n" + CurLv.Name + "    =   " + EleArray.Length.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
           
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            textBox5.Text = dateTimePicker1.Value.Date.ToString("yyyyMMdd");
            //textBox5.Text = dateTimePicker1.Value.ToString();

            //string DOF = "";

            //DOF = dateTimePicker1.Value.Year.ToString();
            //DOF = DOF+dateTimePicker1.Value.Month.ToString();
             //DOF = dateTimePicker1.Value.Date.ToString();
            //textBox5.Text = dateTimePicker1.Value.Year.ToString()+dateTimePicker1.Value.Month.ToString()+dateTimePicker1.Value.Date.ToString();
            //string  DOGval = dateTimePicker1.Value.Date.ToString("dd-MM-yyyy");
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}



